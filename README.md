# mac-async

This repository contains the Agda mechanization of the paper _Securing Asynchronous Exceptions_[CSF'20]

[CSF'20] Carlos Tomé Cortiñas, Marco Vassena and Alejandro Russo. 2020 IEEE 33rd Computer Security Foundations Symposium.

### About

Language-based information-flow control (IFC) techniques often rely on special
purpose, ad-hoc primitives to address different covert channels that originate
in the runtime system, beyond the scope of language constructs. Since these
piecemeal solutions may not compose securely, there is a need for a unified
mechanism to control covert channels. As a first step towards this goal, we
argue for the design of a general interface that allows programs to safely
interact with the runtime system and the available computing resources. To
coordinate the communication between programs and the runtime system, we
propose the use of asynchronous exceptions (interrupts), which, to the best of
our knowledge, have not been considered before in the context of IFC languages.
Since asynchronous exceptions can be raised at any point during execution—often
due to the occurrence of an external event—threads must temporarily mask them
out when manipulating locks and shared data structures to avoid deadlocks and,
therefore, breaking program invariants. Crucially, the naive combination of
asynchronous exceptions with existing features of IFC languages (e.g.,
concurrency and synchronization variables) may open up new possibilities of
information leakage.  In this paper, we present MACasync, a concurrent,
statically enforced IFC language that, as a novelty, features asynchronous
exceptions. We show how asynchronous exceptions easily enable (out of the box)
useful programming patterns like speculative execution and some degree of
resource management. We prove that programs in MACasync satisfy
progress-sensitive non-interference and mechanize our formal claims in the Agda
proof assistant.


### Directory structure src/

* PSNI: *Main file* with the proof of progress-sensitive non-interference.

* Lattice: Abstract definition of the security lattice

* Exception: Abstract type of exceptions

* Types: Syntax of MACAsync types

* Calculus 
    + Base: Core calculus, renaming and substitution.

* Scheduler: Abstract interface for deterministic scheduler and non-interferent scheduler[1].

* Sequential
    - Calculus: Calculus for the store.
    - Semantics: Small-step sequential semantics.
    - Deterministic: Proof that the semantics are deterministic.
    - Progress: Proof of progress for the sequential part.
    - Valid: Validity of terms wrt the store and tpool

* Concurrent
    - Calculus: Calculus for the concurrent configurations.
    - Semantics: Small-step concurrent semantics.
    - Deterministic: Proof that the semantics are deterministic.
    - Progress: Proof of progress for the concurrent semantics.
    - Valid: Preservation of validity of terms wrt the store and thread pool.

* Security
    - Calculus: 
        + Base: Calculus for erased terms.
        + Erasure: Erasure for terms.

    - Sequential:
        + Calculus
        + Erasure: Erasure for the store, mask, and other constructs in the seq. semantics.
        + Semantics: Semantics for double-step erasure.
        + Deterministic: Proof of determinism for the erased semantics.
        + LowEq: Definition of low-equivalence for the sequential calculus.

    - Concurrent:
        + Calculus
        + Erasure: Erasure for the thread pool and parts of the global configuration.
        + Semantics: Concurrent semantics for double-step erasure.
        + Deterministic: Proof of determinism for the erased concurrent semantics.
        + LowEq: Definition of low-equivalence for the concurrent calculus.
        + Progress: Progress for the concurrent small step semantics, low steps can be reconstructed.


#### Auxiliary definitions for lists and relations

+ Data.List.Extra
+ Data.Product.Extra
+ Function.Extra

[1] As an example implementation of RoundRobin https://bitbucket.org/MarcoVassena/mac-model/src/master/Scheduler/RoundRobin/ 

### Agda version
This code type checks using Agda version 2.6.1 and agda standard library version 1.3
