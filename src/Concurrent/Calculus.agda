module Concurrent.Calculus where

  open import Calculus.Base
  open import Sequential.Calculus

  open import Data.List using (List; length; [])
  open import Data.List.Extra
  open import Data.Nat  using (ℕ)
  open import Data.Product using (_×_; Σ)
  open import Data.Unit using (⊤; tt)

  open import Relation.Binary.PropositionalEquality using (_≡_; refl)
  open import Relation.Nullary

  -------------------------------------------------------------------------------- 
  --                               Thread
  --------------------------------------------------------------------------------

  -- A thread of control
  record Thread (l : Label) : Set where 
    constructor ⟨_,_,_⟩
    field
      term       : CTerm (MAC l （）)    -- the computation
      masked     : ξ-Mask                -- the mask
      exceptions : ξ-List l              -- the list of pending exceptions

  open Thread

  -------------------------------------------------------------------------------- 
  --                            Thread pool
  --------------------------------------------------------------------------------

  -- A labeled thread pool
  Pool : (l : Label) → Set
  Pool l = List (Thread l)
  
  TPool : Set
  TPool = (l : Label) → Pool l

  _[_↦_]ᵀ : TPool -> (l : Label) -> List (Thread l) -> TPool
  _[_↦_]ᵀ Φ l P l' with Label-≡-dec l l'
  ... | yes refl = P
  ... | no ¬p    = Φ l'

  infixl 15 _[_↦_]ᵀ

  sizeᵀ : TPool → Label → ℕ
  sizeᵀ Φ l = length (Φ l)

  -- add exception to the thread
  Th-add-ξ : ∀ {l} → ξ → (l' : Label) →  (l'⊑l : l' ⊑ l) → Thread l → Thread l
  Th-add-ξ ex l' l'⊑l ⟨ term , ξ-mask , exs ⟩ = ⟨ term , ξ-mask , ξ-L-add exs ex l' l'⊑l ⟩

  -- Sytactic sugar for Lookupℕ in a Pool
  _↦_∈ᴾ_ : ∀ {l} -> ℕ -> Thread l -> Pool l -> Set
  n ↦ t ∈ᴾ ts = Lookupℕ t n ts

  -- Syntatic sugar for updating a Pool.
  _≔_[_↦_]ᴾ : ∀ {l} -> Pool l -> Pool l -> ℕ -> Thread l -> Set
  P' ≔ P [ n ↦ t ]ᴾ = Updateℕ t n P P'

  mkIdˢ : TPool → Idˢ
  mkIdˢ Φ l = length (Φ l)

  -------------------------------------------------------------------------------- 
  --                            Global configuration
  --------------------------------------------------------------------------------

  open import Scheduler.Base

  module _ (Sch : Scheduler) where

    open Scheduler Sch

    record Global : Set where
      constructor ⟨_,_,_⟩
      field
        tpool : TPool
        store : Store
        state : State
