module Concurrent.Deterministic where

  open import Calculus.Base

  open import Sequential.Calculus
  open import Sequential.Semantics
  open import Sequential.Deterministic

  open import Concurrent.Calculus
  open import Concurrent.Semantics
  open import Scheduler.Base

  open import Data.Empty                            using (⊥-elim)
  open import Data.Nat                              using (ℕ)
  open import Data.Nat.Properties                   using (≤-irrelevant)
  open import Data.Product                          using (Σ; _×_; _,_; -,_)
  open import Data.Product.Extra
  open import Data.List.Extra

  open import Relation.Binary.PropositionalEquality using (_≡_; refl; trans; sym) renaming (subst to ≡-subst)

  -- Small-step thread semantics is deterministic
  ⟼t-det : ∀ {l} {ϕ} {Σ} {th : Thread l}
              {ev₁ : CEvent l} {th₁ : Thread l} {Σ₁}
              {ev₂ : CEvent l} {th₂ : Thread l} {Σ₂}
          → [ ϕ ] Σ , th ⟼t[ ev₁ ] Σ₁ , th₁
          → [ ϕ ] Σ , th ⟼t[ ev₂ ] Σ₂ , th₂
          → ev₁ ≡ ev₂ × th₁ ≡ th₂ × Σ₁ ≡ Σ₂
  ⟼t-det (Lift s) (Lift s₁) with ⟼e-det s s₁
  ⟼t-det (Lift s) (Lift s₁) | refl , refl , refl , refl = refl , refl , refl
  
  module _ (Sch : Scheduler) where

    open Scheduler Sch
    open Global 

    ↪-determinism : ∀ {l n} {c₁ c₂ c₃ : Global Sch}
                    → l , n ⊢ c₁ ↪ c₂ → l , n ⊢ c₁ ↪ c₃ → c₂ ≡ c₃
    ↪-determinism (Step p₁ p₂ d sch-step) (Step p₃ p₄ d₁ sch-step₁)
      with Lookupℕ-inj p₁ p₃
    ... | refl
      with ⟼t-det d d₁
    ... | refl , refl , refl
      with ↑[ Sch ]⟶-det sch-step sch-step₁
    ... | refl
      with Updateℕ-inj p₂ p₄
    ... | refl = refl
    ↪-determinism (Step p₁ p₂ d sch-step) (Fork p₃ p₄ d₁ sch-step₁)
      with Lookupℕ-inj p₁ p₃
    ... | refl
      with ⟼t-det d d₁
    ... | ()
    ↪-determinism (Step p₁ p₂ d sch-step) (Throw p₃ p₄ p₅ p₆ d₁ sch-step₁)
      with Lookupℕ-inj p₁ p₃
    ... | refl
      with ⟼t-det d d₁
    ... | ()
    ↪-determinism (Step p₁ p₂ d sch-step) (Done isV-th₁ p₃ sch-step₁)
      with Lookupℕ-inj p₁ p₃
    ... | refl = ⊥-elim (⟼t-Values-do-not-step _ (isV-th₁ , (-,₃ d)))

    ↪-determinism (Fork p₁ p₂ d sch-step) (Step p₃ p₄ d₁ sch-step₁)
      with Lookupℕ-inj p₁ p₃
    ... | refl
      with ⟼t-det d d₁
    ... | ()
    ↪-determinism (Fork p₁ p₂ d sch-step) (Fork p₃ p₄ d₁ sch-step₁)
      with Lookupℕ-inj p₁ p₃
    ... | refl
      with ⟼t-det d d₁
    ... | refl , refl , refl
      with ↑[ Sch ]⟶-det sch-step sch-step₁
    ... | refl
      with Updateℕ-inj p₂ p₄
    ... | refl = refl
    ↪-determinism (Fork p₁ p₂ d sch-step) (Throw p₃ p₄ p₅ p₆ d₁ sch-step₁)
      with Lookupℕ-inj p₁ p₃
    ... | refl
      with ⟼t-det d d₁
    ... | ()
    ↪-determinism (Fork p₁ p₂ d sch-step) (Done isV-th₁ p₃ sch-step₁)
      with Lookupℕ-inj p₁ p₃
    ... | refl = ⊥-elim (⟼t-Values-do-not-step _ (isV-th₁ , (-,₃ d)))
    ↪-determinism (Throw p₁ p₂ p₃ p₄ d sch-step) (Step p₅ p₆ d₁ sch-step₁)
      with Lookupℕ-inj p₁ p₅
    ... | refl
      with ⟼t-det d d₁
    ... | ()
    ↪-determinism (Throw p₁ p₂ p₃ p₄ d sch-step) (Fork p₅ p₆ d₁ sch-step₁)
      with Lookupℕ-inj p₁ p₅
    ... | refl
      with ⟼t-det d d₁
    ... | ()
    ↪-determinism (Throw p₁ p₂ p₃ p₄ d sch-step) (Throw p₅ p₆ p₇ p₈ d₁ sch-step₁)
      with Lookupℕ-inj p₁ p₅
    ... | refl
      with ⟼t-det d d₁
    ... | refl , refl , refl
      with ↑[ Sch ]⟶-det sch-step sch-step₁
    ... | refl
      with Updateℕ-inj p₃ p₇
    ... | refl
      with Lookupℕ-inj p₂ p₆
    ... | refl
      with Updateℕ-inj p₄ p₈
    ... | refl = refl
    ↪-determinism (Throw p₁ p₂ p₃ p₄ d sch-step) (Done isV-th₁ p₅ sch-step₁)
      with Lookupℕ-inj p₁ p₅
    ... | refl = ⊥-elim (⟼t-Values-do-not-step _ (isV-th₁ , (-,₃ d)))
    ↪-determinism (Done isV-th₁ p sch-step) (Step p₁ p₂ d sch-step₁)
      with Lookupℕ-inj p p₁
    ... | refl = ⊥-elim (⟼t-Values-do-not-step _ (isV-th₁ , (-,₃ d)))
    ↪-determinism (Done isV-th₁ p sch-step) (Fork p₁ p₂ d sch-step₁)
      with Lookupℕ-inj p p₁
    ... | refl = ⊥-elim (⟼t-Values-do-not-step _ (isV-th₁ , (-,₃ d)))
    ↪-determinism (Done isV-th₁ p sch-step) (Throw p₁ p₂ p₃ p₄ d sch-step₁)
      with Lookupℕ-inj p p₁
    ... | refl = ⊥-elim (⟼t-Values-do-not-step _ (isV-th₁ , (-,₃ d)))
    ↪-determinism (Done isV-th₁ p sch-step) (Done isV-th₂ p₁ sch-step₁)
      with ↑[ Sch ]⟶-det sch-step sch-step₁
    ... | refl = refl
    ↪-determinism (Step p₁ p₂ d sch-step) (Stuck p d₁ sch-step₁)
      with Lookupℕ-inj p₁ p
    ... | refl
      with ⟼t-det d d₁
    ... | ()
    ↪-determinism (Fork p₁ p₂ d sch-step) (Stuck p d₁ sch-step₁)
      with Lookupℕ-inj p₁ p
    ... | refl
      with ⟼t-det d d₁
    ... | ()
    ↪-determinism (Throw p₁ p₂ p₃ p₄ d sch-step) (Stuck p d₁ sch-step₁)
      with Lookupℕ-inj p₁ p
    ... | refl
      with ⟼t-det d d₁
    ... | ()
    ↪-determinism (Stuck p d sch-step) (Step p₁ p₂ d₁ sch-step₁)
      with Lookupℕ-inj p₁ p
    ... | refl
      with ⟼t-det d d₁
    ... | ()
    ↪-determinism (Stuck p d sch-step) (Fork p₁ p₂ d₁ sch-step₁)
      with Lookupℕ-inj p₁ p
    ... | refl
      with ⟼t-det d d₁
    ... | ()
    ↪-determinism (Stuck p d sch-step) (Throw p₁ p₂ p₃ p₄ d₁ sch-step₁)
      with Lookupℕ-inj p₁ p
    ... | refl
      with ⟼t-det d d₁
    ... | ()
    ↪-determinism (Stuck p d sch-step) (Stuck p₁ d₁ sch-step₁)
      with Lookupℕ-inj p p₁
    ... | refl
      with ⟼t-det d d₁
    ... | refl , refl , refl
      with ↑[ Sch ]⟶-det sch-step sch-step₁
    ... | refl = refl
    ↪-determinism (Stuck p d sch-step) (Done isV-th₁ p₁ sch-step₁)
      with Lookupℕ-inj p p₁
    ... | refl = ⊥-elim (⟼t-Values-do-not-step _ (isV-th₁ , (-,₃ d)))
    ↪-determinism (Done isV-th₁ p sch-step) (Stuck p₁ d sch-step₁)
      with Lookupℕ-inj p p₁
    ... | refl = ⊥-elim (⟼t-Values-do-not-step _ (isV-th₁ , (-,₃ d)))

