open import Types

module Concurrent.Scheduler (Term : Ty → Set) (State : Set) where

  open import Lattice

  open import Data.Product
  open import Relation.Binary.PropositionalEquality
  open import Data.Nat

  -------------------------------------------------------------------------------- 
  --                            Concurrent events
  --------------------------------------------------------------------------------

  -- concurrent events to be pass to the scheduler
  data ConcEvent (l : Label) : Set where
    fork : ∀ {h} (l⊑h : l ⊑ h) (t : Term (MAC h （）)) (n : ℕ) → ConcEvent l
    step : ConcEvent l
    done : ConcEvent l

  data Message : Label -> Set where
    ⟪_,_,_⟫ : (l : Label) (n : ℕ) (e : ConcEvent l) -> Message l

  -------------------------------------------------------------------------------- 
  --                            Scheduler
  --------------------------------------------------------------------------------

  record Scheduler : Set₁ where
    field
        -- The scheduler semantics relation: ω ⟶ ω' ↑ m denotes that
        -- scheduler state ω gets updated to ω' and generates message m.
        _↑_⟶_ : ∀ {l} -> State → Message l → State → Set

        -- the scheduler is deterministic
        ↑⟶-det : ∀ {l n e} {ω₁ ω₂ ω₃ : State} -> ω₁ ↑ ⟪ l , n , e ⟫ ⟶ ω₂ → ω₁ ↑ ⟪ l , n , e ⟫ ⟶ ω₃ → ω₂ ≡ ω₃

        -- (l,n) ∈ˢ ω denotes that thread (l,n) is alive according to scheduler state ω.
        _∈ˢ_ : Label × ℕ → State -> Set

  open Scheduler public

  ↑⟶-syntax = Scheduler._↑_⟶_; syntax ↑⟶-syntax S w m w' = w ↑[ S ] m ⟶ w'
