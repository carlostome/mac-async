module Concurrent.Semantics  where

  open import Calculus.Base
  open import Concurrent.Calculus
  open import Scheduler.Base

  open import Sequential.Calculus
  open import Sequential.Semantics
  open import Sequential.Lemmas

  open import Data.Bool using (Bool; true; false)
  open import Data.Nat using (ℕ; _<_)
  open import Data.Fin using (fromℕ<)
  open import Relation.Binary.PropositionalEquality using (_≡_)
  open import Data.List using (List; []; _∷_; lookup; _[_]∷=_; _[_]%=_; _++_; [_])
  open import Data.Unit using (tt; ⊤)
  open import Relation.Nullary
  open import Data.Product
  open import Data.Product.Extra
  
  -- Small-step thread semantics
  data [_]_,_⟼t[_]_,_ {l : Label} (ϕ : Idˢ) (Σ : Store) : (t₁ : Thread l) (e : CEvent l) → Store → Thread l → Set where

    Lift : ∀ {t₁ : CTerm (MAC l （）)} {M : ξ-Mask} {exs : ξ-List l}
             {Σ' : Store} {ev : CEvent l} {exs' : ξ-List l} {t₂ : CTerm (MAC l （）)}

          →  (s : [ ϕ ] Σ , t₁ [ M , exs ]⟼e[ exs' , ev ] Σ' , t₂)
          -----------------------------------------
          → [ ϕ ] Σ , ⟨ t₁ , M , exs ⟩ ⟼t[ ev ] Σ' , ⟨ t₂ , M , exs' ⟩

  data ⟼t-Value {l : Label} : Thread l → Set where
    ⟨_,_,_⟩ : ∀ {t : CTerm (MAC l （）)} → ⟼-Value t → (M : ξ-Mask) → (exs : ξ-List l) → ⟼t-Value ⟨ t , M , exs ⟩

  ⟼t-Values-do-not-step : ∀ {l : Label} {ϕ} {Σ : Store} (th : Thread l)
                        → ¬ (⟼t-Value th × ∃₃ λ e Σ' th' → [ ϕ ] Σ , th ⟼t[ e ] Σ' , th')
  ⟼t-Values-do-not-step .(⟨ _ , M , exs ⟩) (⟨ x , M , exs ⟩ , _ , _ , _ , Lift s) = ⟼-Values-do-not-step (x , (-,₄ s))

  fork-child : ∀ {l} → CTerm (MAC l （）) → ξ-Mask → Thread l
  fork-child t m = ⟨ t , m , [] ⟩

  open Scheduler

  module _ {Sch : Scheduler} where
    data _,_⊢_↪_  (l : Label) (n : ℕ) : Global Sch → Global Sch -> Set where

        Step : ∀ {Φ} {Pᴸ} {Σ}
                {ω ω' : State Sch}
                {th₁ : Thread l}
                {Σ'  : Store}
                {th₂ : Thread l}

            → (p₁ : n ↦ th₁ ∈ᴾ Φ l)
            → (p₂ : Pᴸ ≔ (Φ l) [ n ↦ th₂ ]ᴾ)
            → (d : [ mkIdˢ Φ ] Σ , th₁ ⟼t[ ∅ ] Σ' , th₂)
            → (sch-step : ω ↑[ Sch ] ⟪ l , n , step ⟫ ⟶ ω')
            --------------------------------------------------------------------------
            → l , n ⊢ ⟨ Φ , Σ , ω ⟩ ↪ ⟨ Φ [ l ↦ Pᴸ ]ᵀ , Σ' , ω' ⟩

        Fork : ∀ {Φ} {Pᴸ} {Σ}
                {ω ω' : State Sch}
                {h} {l⊑h : l ⊑ h}
                {th₁ : Thread l}
                {th₂ : Thread l}
                {t : CTerm (MAC h （）)}
                {M : ξ-Mask}
            → let P = Φ l
                  Φ' = Φ [ l ↦ Pᴸ ]ᵀ
                  nᴴ = mkIdˢ Φ h
                  Pᴴ = Φ' h
            in
            (p₁ : n ↦ th₁ ∈ᴾ Φ l)
            → (p₂ : Pᴸ ≔ (Φ l) [ n ↦ th₂ ]ᴾ)
            → (d  : [ mkIdˢ Φ ] Σ , th₁ ⟼t[ fork h l⊑h t M ] Σ , th₂)
            → (sch-step : ω ↑[ Sch ] ⟪ l , n , fork h l⊑h nᴴ ⟫ ⟶ ω')
            ------------------------------------------------------------------------------------------------------------------------------------
            → l , n ⊢ ⟨ Φ , Σ , ω ⟩ ↪ ⟨ Φ' [ h ↦ Pᴴ ++ [ fork-child t M ] ]ᵀ , Σ , ω' ⟩

        Throw : ∀ {Φ} {Pᴸ : Pool l} {Σ}
                {ω ω' : State Sch}
                {h : Label}
                {l⊑h : l ⊑ h}
                {th₁ : Thread l}
                {e : ξ}
                {th₂ : Thread l}
                {Pᴴ : Pool h}
                {thξ : Thread h}
                {nᴴ : ℕ}
            → let Φ' = Φ [ l ↦ Pᴸ ]ᵀ
                in
                (p₁ : n  ↦ th₁ ∈ᴾ Φ  l)
            → (p₂ : nᴴ ↦ thξ ∈ᴾ Φ' h)
            → (p₃ : Pᴸ ≔ (Φ l)  [ n   ↦ th₂ ]ᴾ)
            → (p₄ : Pᴴ ≔ (Φ' h) [ nᴴ  ↦ Th-add-ξ e l l⊑h thξ ]ᴾ)
            → (d : [ mkIdˢ Φ ] Σ , th₁ ⟼t[ throw e h l⊑h nᴴ ] Σ , th₂)
            → (sch-step : ω ↑[ Sch ] ⟪ l , n , step ⟫ ⟶ ω')
            -------------------------------------------------------------------------------------------------------------------------------
            → l , n ⊢ ⟨ Φ , Σ , ω ⟩ ↪ ⟨ Φ' [ h ↦ Pᴴ ]ᵀ , Σ , ω' ⟩

        Stuck : ∀ {Φ} {Σ}
                {ω ω' : State Sch} 
                {th   : Thread l}

                → n ↦ th ∈ᴾ Φ l 
                → (d : [ mkIdˢ Φ ] Σ , th ⟼t[ stuck ] Σ , th)
                → (sch-step : ω ↑[ Sch ] ⟪ l , n , stuck ⟫ ⟶ ω')
                ------------------------------------
                → l , n ⊢ ⟨ Φ , Σ , ω ⟩ ↪ ⟨ Φ , Σ ,  ω' ⟩

        Done : ∀ {Φ} {Σ}
                {ω ω' : State Sch} 
                {th₁ : Thread l}
                (isV-th₁ : ⟼t-Value th₁)

            → n ↦ th₁ ∈ᴾ Φ l 
            → (sch-step : ω ↑[ Sch ] ⟪ l , n , done ⟫ ⟶ ω')
            ------------------------------------
            → l , n ⊢ ⟨ Φ , Σ , ω ⟩ ↪ ⟨ Φ , Σ ,  ω' ⟩

    getEvent : ∀ {l} {n} {c₁ c₂ : Global Sch} -> l , n ⊢ c₁ ↪ c₂ -> SchedEvent l
    getEvent (Step p₁ p₂ d sch-step) = step
    getEvent {l} (Fork {Φ = Φ} {Pᴸ = Pᴸ} {h = h} {l⊑h = l⊑h} p₁ p₂ d sch-step) = fork h l⊑h (sizeᵀ Φ h)
    getEvent (Throw p₁ p₂ p₃ p₄ d sch-step) = step
    getEvent (Stuck p d sch-step) = stuck
    getEvent (Done isV-th₁ x sch-step) = done

    open Global

    getSchStep : ∀ {l n} {c₁ c₂ : Global Sch} -> (s : l , n ⊢ c₁ ↪ c₂) -> state c₁ ↑[ Sch ] ⟪ l , n , getEvent s ⟫ ⟶ state c₂ 
    getSchStep (Step p₁ p₂ d sch-step) = sch-step
    getSchStep (Fork p₁ p₂  d sch-step) = sch-step
    getSchStep (Throw p₁ p₂ p₃ p₄ d sch-step) = sch-step
    getSchStep (Stuck x d sch-step) = sch-step
    getSchStep (Done isV-th₁ x sch-step) = sch-step

  _,_⊢[_]_↪_ : Label → ℕ → (Sch : Scheduler) → Global Sch → Global Sch → Set
  _,_⊢[_]_↪_ l n S w w' = _,_⊢_↪_ {S} l n w w'
    
  -- transitive reflexive closure of the ↪ relation
  data _↪⋆_ {Sch} : Global Sch → Global Sch → Set where

    [] : ∀ {g} -> g ↪⋆ g

    _∷_ : ∀ {l n} {g₁ g₂ g₃} -> l , n ⊢ g₁ ↪ g₂ -> g₂ ↪⋆ g₃ -> g₁ ↪⋆ g₃
