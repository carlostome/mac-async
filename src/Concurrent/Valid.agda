module Concurrent.Valid where

  open import Calculus.Base
  open import Sequential.Calculus
  open import Concurrent.Calculus

  open import Concurrent.Semantics

  open import Data.List using (List; _∷_; length; [])
  open import Data.Bool using (true; false)
  open import Data.List.Extra
  open import Data.Nat  using (ℕ; _<_)
  open import Data.Product using (_×_; Σ; ∃; _,_)
  open import Data.Unit using (⊤; tt)

  open import Relation.Binary.PropositionalEquality using (_≡_; refl)
  open import Relation.Nullary

  open import Scheduler.Base
  open Global

  open import Calculus.Base
  open import Sequential.Calculus
  open import Sequential.Semantics
  import Sequential.Valid as WS

  open import Data.List
  open import Data.List.Extra
  open import Data.Unit using (⊤; tt)
  open import Data.Product renaming (proj₁ to fst)
  open import Data.Product.Extra
  open import Data.Nat
  open import Data.Nat.Properties using (≤-refl)
  open import Relation.Binary.PropositionalEquality renaming ([_] to [_]≡)
  open import Data.Maybe using (Maybe; just; nothing)
  open import Function 

  mutual
    well-scoped : ∀ {Γ τ} (ϕ : TPool) (Σ : Store) → Term Γ τ → Set
    well-scoped ϕ Σ (var τ∈Γ) = ⊤
    well-scoped ϕ Σ (λ′ t) = well-scoped ϕ Σ t
    well-scoped ϕ Σ (t ∙ t₁) = well-scoped ϕ Σ t × well-scoped ϕ Σ t₁
    well-scoped ϕ Σ （） = ⊤
    well-scoped ϕ Σ True = ⊤
    well-scoped ϕ Σ False = ⊤
    well-scoped ϕ Σ (if t then t₁ else t₂) = well-scoped ϕ Σ t × well-scoped ϕ Σ t₁ × well-scoped ϕ Σ t₂
    well-scoped ϕ Σ (return l t) = well-scoped ϕ Σ t
    well-scoped ϕ Σ (t >>= u) = well-scoped ϕ Σ t × well-scoped ϕ Σ u
    well-scoped ϕ Σ (Labeled l t) = well-scoped ϕ Σ t
    well-scoped ϕ Σ (label l⊑h t) = well-scoped ϕ Σ t
    well-scoped ϕ Σ (unlabel l⊑h t) = well-scoped ϕ Σ t
    well-scoped ϕ Σ (fork l⊑h t) = well-scoped ϕ Σ t
    well-scoped ϕ Σ (χ l x) = ⊤
    well-scoped ϕ Σ (throwTo e l⊑h t) = well-scoped ϕ Σ t
    well-scoped ϕ Σ (catch hs t) = well-scoped-list ϕ Σ hs × well-scoped ϕ Σ t
    well-scoped ϕ Σ (mask e t) = well-scoped ϕ Σ t
    well-scoped ϕ Σ (unmask e t) = well-scoped ϕ Σ t
    well-scoped ϕ Σ (#Thread l n) = ∃ λ th → Lookupℕ th n (ϕ l)
    well-scoped ϕ Σ (newMVar l⊑h) = ⊤
    well-scoped ϕ Σ (takeMVar t) = well-scoped ϕ Σ t
    well-scoped ϕ Σ (putMVar t t₁) = well-scoped ϕ Σ t × well-scoped ϕ Σ t₁
    well-scoped ϕ Σ (#MVar {τ = τ} l n) = ∃ λ c → Lookupℕ (τ , c) n (Σ l)

    well-scoped-list : ∀ {Γ τ} (ϕ : TPool) (Σ : Store) → List (ξ × Term Γ τ) → Set
    well-scoped-list ϕ Σ [] = ⊤
    well-scoped-list ϕ Σ ((_ , t) ∷ xs) = well-scoped ϕ Σ t × well-scoped-list ϕ Σ xs

  well-scopedᶜ : (ϕ : TPool) → (Σ : Store) → ∃ Cell → Set
  well-scopedᶜ ϕ Σ (τ , ⊗)     = ⊤
  well-scopedᶜ ϕ Σ (τ , ⟦ t ⟧) = well-scoped ϕ Σ t

  well-scopedₘ : (ϕ : TPool) → (Σ : Store) → Mem → Set
  well-scopedₘ ϕ Σ []      = ⊤
  well-scopedₘ ϕ Σ (c ∷ m) = well-scopedᶜ ϕ Σ c

  well-scopedₛ : (ϕ : TPool) → Store → Set
  well-scopedₛ ϕ Σ = (l : Label) → well-scopedₘ ϕ Σ (Σ l)

  well-scoped-CEvent : (ϕ : TPool) (Σ : Store) → {l : Label} → CEvent l → Set
  well-scoped-CEvent ϕ Σ ∅ = ⊤
  well-scoped-CEvent ϕ Σ (fork h l⊑h t M) = well-scoped ϕ Σ t
  well-scoped-CEvent ϕ Σ (throw e h l⊑h n) = ∃ λ th → Lookupℕ th n (ϕ h)
  well-scoped-CEvent ϕ Σ stuck = ⊤

  mutual
    well-scoped→well-scoped : ∀ {Γ τ} {ϕ : TPool} {Σ } (t : Term Γ τ)
                            → well-scoped ϕ Σ t
                            → WS.well-scoped Σ t
    well-scoped→well-scoped (var τ∈Γ) x = tt
    well-scoped→well-scoped (λ′ t) x = well-scoped→well-scoped t x
    well-scoped→well-scoped (t ∙ t₁) (v , v₁) = well-scoped→well-scoped t v , well-scoped→well-scoped t₁ v₁
    well-scoped→well-scoped （） x = tt
    well-scoped→well-scoped True x = tt
    well-scoped→well-scoped False x = tt
    well-scoped→well-scoped (if t then t₁ else t₂) (v , v₁ , v₂)
        = well-scoped→well-scoped t v , well-scoped→well-scoped t₁ v₁ , well-scoped→well-scoped t₂ v₂
    well-scoped→well-scoped (return l t) x = well-scoped→well-scoped t x
    well-scoped→well-scoped (t >>= t₁) (v , v₁) = well-scoped→well-scoped t v , well-scoped→well-scoped t₁ v₁
    well-scoped→well-scoped (Labeled l t) x = well-scoped→well-scoped t x
    well-scoped→well-scoped (label l⊑h t) x = well-scoped→well-scoped t x
    well-scoped→well-scoped (unlabel l⊑h t) x = well-scoped→well-scoped t x
    well-scoped→well-scoped (fork l⊑h t) x = well-scoped→well-scoped t x
    well-scoped→well-scoped (χ l x₁) x = tt
    well-scoped→well-scoped (throwTo e l⊑h t) x = well-scoped→well-scoped t x
    well-scoped→well-scoped (catch hs t) (vₛ , vₜ) = well-scopedx2-list hs vₛ , (well-scoped→well-scoped t vₜ)
    well-scoped→well-scoped (mask e t) x = well-scoped→well-scoped t x
    well-scoped→well-scoped (unmask e t) x = well-scoped→well-scoped t x
    well-scoped→well-scoped (#Thread l n) x = tt
    well-scoped→well-scoped (newMVar l⊑h) x = tt
    well-scoped→well-scoped (takeMVar t) x = well-scoped→well-scoped t x
    well-scoped→well-scoped (putMVar t₁ t₂) (v₁ , v₂) = well-scoped→well-scoped t₁ v₁ , well-scoped→well-scoped t₂ v₂
    well-scoped→well-scoped (#MVar l n) x = x

    well-scopedx2-list : ∀ {Γ τ} {ϕ : TPool} {Σ : Store} → (xs : List (ξ × Term Γ τ))
                            → well-scoped-list ϕ Σ xs
                            → WS.well-scoped-list Σ xs
    well-scopedx2-list [] vs = tt
    well-scopedx2-list ((_ , x) ∷ xs) (v , vs) = well-scoped→well-scoped x v , well-scopedx2-list xs vs

  throw-≡ : ∀ {l} {ϕ} {τ} {Σ} {t₁ t₂ : CTerm (MAC l τ)} {M} {exs exs'} {e} {h} {l⊑h} {n}
              {Σ'}
            → well-scoped ϕ Σ t₁
            → [ mkIdˢ ϕ ] Σ , t₁ [ M , exs ]⟼e[ exs' , throw {l = l} e h l⊑h n ] Σ' , t₂
            → exs' ≡ exs × Σ' ≡ Σ × well-scoped-CEvent ϕ Σ (throw {l = l} e h l⊑h n)
  throw-≡ v (Mask s) = throw-≡ v s
  throw-≡ v (UnMask s) = throw-≡ v s
  throw-≡ (v , _) (Bind s) = throw-≡ v s
  throw-≡ v (Catch s) = throw-≡ (proj₂ v) s
  throw-≡ v (LiftC◍ x s) = refl , refl , aux v s
    where aux : ∀ {l} {ϕ} {τ} {Σ} {t₁ t₂ : CTerm (MAC l τ)} {M} {e} {h} {l⊑h} {n}
                    → well-scoped ϕ Σ t₁
                    → [ mkIdˢ ϕ ] t₁ [ M ]⟼c[ throw {l = l} e h l⊑h n ] t₂
                    → well-scoped-CEvent ϕ Σ (throw {l = l} e h l⊑h n)
          aux v (ThrowTo₂ e l⊑h n) = v

  fork-≡ : ∀ {l} {ϕ} {τ} {Σ} {t₁ t₂ : CTerm (MAC l τ)} {M} {M'} {exs exs'} {h} {l⊑h} {t'} 
             {Σ'}
            → well-scoped ϕ Σ t₁
            → [ mkIdˢ ϕ ] Σ , t₁ [ M , exs ]⟼e[ exs' , fork h l⊑h t' M' ] Σ' , t₂
            → exs' ≡ exs × Σ' ≡ Σ × well-scoped-CEvent ϕ Σ (fork h l⊑h t' M' )
  fork-≡ v (Mask s) = fork-≡ v s
  fork-≡ v (UnMask s) = fork-≡ v s
  fork-≡ v (Bind s) = fork-≡ (fst v) s
  fork-≡ v (Catch s) = fork-≡ (proj₂ v) s
  fork-≡ v (LiftC◍ x s) with aux v s
    where aux : ∀ {l} {ϕ} {τ} {Σ} {t₁ t₂ : CTerm (MAC l τ)} {M} {M'} {h} {l⊑h} {t}
                    → well-scoped ϕ Σ t₁
                    → [ mkIdˢ ϕ ] t₁ [ M ]⟼c[ fork {l = l} h l⊑h t M' ] t₂
                    → M ≡ M' × well-scoped-CEvent ϕ Σ (fork {l = l} h l⊑h t M')
          aux v (Fork l⊑h) = refl , v
  ... | refl , v' = refl , refl , v' 

  stuck-≡ : ∀ {l} {ϕ} {τ} {Σ} {t₁ t₂ : CTerm (MAC l τ)} {M} {exs exs'}
            {Σ'}
            → well-scoped ϕ Σ t₁
            → [ mkIdˢ ϕ ] Σ , t₁ [ M , exs ]⟼e[ exs' , stuck ] Σ' , t₂
            → exs' ≡ exs × Σ' ≡ Σ × t₁ ≡ t₂
  stuck-≡ v (Mask s) with stuck-≡ v s
  ... | refl , refl , refl = refl , refl , refl
  stuck-≡ v (UnMask s) with stuck-≡ v s
  ... | refl , refl , refl = refl , refl , refl
  stuck-≡ (v , _) (Bind s) with stuck-≡ v s
  ... | refl , refl , refl = refl , refl , refl
  stuck-≡ (vhs , vt) (Catch s) with stuck-≡ vt s
  ... | refl , refl , refl = refl , refl , refl
  stuck-≡ v (LiftSₛ s) = refl , refl , refl

  well-scoped-Th : ∀ {l} → (ϕ : TPool) → (Σ : Store) → Thread l → Set
  well-scoped-Th ϕ Σ ⟨ term , masked , exceptions ⟩ = well-scoped ϕ Σ term

  -- A well-scoped pool contains only well-scoped threads.
  well-scopedᴾ : ∀ {l} → (ϕ : TPool) → (Σ : Store) → Pool l → Set
  well-scopedᴾ ϕ Σ []       = ⊤
  well-scopedᴾ ϕ Σ (th ∷ P) = well-scoped-Th ϕ Σ th × well-scopedᴾ ϕ Σ P

  well-scopedᴾ-lookupℕ : {ϕ : TPool} {Σ : Store} {l : Label} {P : Pool l}
                 → well-scopedᴾ ϕ Σ P
                 → ∀ {n} {th} → Lookupℕ th n P  
                 → well-scoped-Th ϕ Σ th
  well-scopedᴾ-lookupℕ (v , _) here = v
  well-scopedᴾ-lookupℕ (_ , v) (there l) = well-scopedᴾ-lookupℕ v l

  well-scopedᴾ-updateℕ : {ϕ : TPool} {Σ : Store} {l : Label} {P P' : Pool l} {th : Thread l}
                 → well-scopedᴾ ϕ Σ P
                 → well-scoped-Th ϕ Σ th
                 → ∀ {n} → Updateℕ th n P P'
                 → well-scopedᴾ ϕ Σ P'
  well-scopedᴾ-updateℕ (_ , vᴾ) vₜₕ here = vₜₕ , vᴾ
  well-scopedᴾ-updateℕ (v , vᴾ) vₜₕ (there upd) = v , well-scopedᴾ-updateℕ vᴾ vₜₕ upd

  -- A well-scoped pool map contains only well-scoped pools
  well-scopedᵀ : (Σ : Store) → TPool → Set
  well-scopedᵀ Σ Φ = (l : Label) → well-scopedᴾ Φ Σ (Φ l) 

  open import Function.Extensionality
  open import Data.Empty
  
  -- Lemmas
  updateᵀ-≡ : ∀ {l} {Φ : TPool} {P : Pool l} → Φ l ≡ P → Φ ≡ (Φ [ l ↦ P ]ᵀ)
  updateᵀ-≡ {l} {Φ} {P} p = fun-ext aux
    where
      aux : ∀ (l' : Label) → Φ l' ≡ (Φ [ l ↦ P ]ᵀ) l'
      aux l' with Label-≡-dec l l'
      ... | .true because ofʸ refl = p
      ... | .false because ofⁿ ¬p = refl

  updateᵀ-index : ∀ {l} {Φ : TPool} {P : Pool l} → (Φ [ l ↦ P ]ᵀ) l ≡ P
  updateᵀ-index {l} {Φ} {P} with Label-≡-dec l l
  ... | .true because ofʸ refl = refl
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p refl)

  -- The scheduler state is well-scoped with respect to the thread pool map if
  -- any alive thread according to the scheduler is present in the pool map.
  module _ (Sch : Scheduler) where
    open Scheduler Sch

    -- well-scoped scheduler state
    well-scopedˢ : State → TPool → Set
    well-scopedˢ ω Φ = (∀ l n → (l , n) ∈ˢ ω → ∃ (λ t → n ↦ t ∈ᴾ (Φ l)))

    -- well-scoped global configuration
    well-scopedᴳ : Global Sch -> Set
    well-scopedᴳ ⟨ Φ , Σ , ω ⟩ = well-scopedᵀ Σ Φ × well-scopedₛ Φ Σ × well-scopedˢ ω Φ

    -- A well-scoped scheduler preserves the well-scoped scheduler property
    record Well-scopedˢ : Set where
        field well-scopedˢ↪ : ∀ {l n c₁ c₂} → l , n ⊢ c₁ ↪ c₂ → well-scopedᴳ c₁ → well-scopedˢ (state c₂) (tpool c₂)

    module Well-scoped↪ (𝓥 : Well-scopedˢ) where

        open Well-scopedˢ 𝓥 public

        -- The concurrent semantics preserves the well-scoped property.  We
        -- do not prove it here yet but we refer the reader to the
        -- proof by Vassena
        -- (https://bitbucket.org/MarcoVassena/mac-model/src/master/Concurrent/Well-scoped.agda)
        -- which follows the same pattern.

        -- The main idea of the lemma is to prove that given a well-scoped
        -- configuration (i.e. programs refer only to variables
        -- withing the store and threads in the system.  Since we
        -- control the semantics, it is clear that (1) operations on
        -- synchronization variables do not introduce bogus references
        -- as the only that actually modifies the size of the store is
        -- newMVar, and the new size always increases.

        -- The other tricky case that introduces identifiers to
        -- threads is the fork operation that creates new identifiers for the
        -- newly spawned threads.
        -- Since to do so, it uses as a reference the parameter ϕ which carries
        -- the fresher identification number per security level. This maintains
        -- the correctness invariant.

        postulate
          well-scoped↪ : ∀ {l n} {c₁ c₂ : Global Sch} -> l , n ⊢ c₁ ↪ c₂ -> well-scopedᴳ c₁ -> well-scopedᴳ c₂
