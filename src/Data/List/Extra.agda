module Data.List.Extra where

  open import Data.List               using (List; _∷_; []; length; lookup; map)
  open import Data.List.Properties
  open import Data.Maybe   using (Maybe; just; nothing; maybe)
  open import Data.Product using (_×_; _,_; ∃)
  open import Data.Bool    using (Bool; true; false)
  open import Data.Nat
  open import Data.Fin     using (fromℕ<; Fin) renaming (zero to fzero; suc to fsuc)

  first : ∀ {A : Set} → (A → Bool) → List A → Maybe (A × List A)
  first p []       = nothing
  first p (x ∷ xs) with p x
  first p (x ∷ xs) | false
    with first p xs
  first p (x ∷ xs) | false | nothing       = nothing
  first p (x ∷ xs) | false | just (y , ys) = just (y , x ∷ ys)
  first p (x ∷ xs) | true                  = just (x , xs)

  open import Relation.Binary.PropositionalEquality

  data Lookupℕ {A : Set} (a : A) : ℕ → List A → Set where
    here  : ∀ {xs : List A}             → Lookupℕ a 0 (a ∷ xs)
    there : ∀ {n} {x : A} {xs : List A} → Lookupℕ a n xs -> Lookupℕ a (1 + n) (x ∷ xs)

  data Updateℕ {A : Set} (a : A) : ℕ → List A → List A → Set where
    here  : ∀ {x : A} {xs : List A}          → Updateℕ a 0 (x ∷ xs) (a ∷ xs)
    there : ∀ {n} {x : A} {xs₁ xs₂ : List A} → Updateℕ a n xs₁ xs₂ -> Updateℕ a (1 + n) (x ∷ xs₁) (x ∷ xs₂)

  Lookupℕ-inj : ∀ {A : Set} {a b : A} {n : ℕ} {xs : List A}
              → Lookupℕ a n xs
              → Lookupℕ b n xs
              → a ≡ b
  Lookupℕ-inj here here = refl
  Lookupℕ-inj (there p) (there q) = Lookupℕ-inj p q

  Updateℕ-inj : ∀ {A : Set} {a : A} {n : ℕ} {xs : List A} {ys₁ ys₂ : List A}
              → Updateℕ a n xs ys₁
              → Updateℕ a n xs ys₂
              → ys₁ ≡ ys₂
  Updateℕ-inj here here = refl
  Updateℕ-inj (there p) (there q) = cong (_ ∷_) (Updateℕ-inj p q)

  Lookupℕ-map : ∀ {A B : Set} {a : A} {f : A → B} {n : ℕ} {xs : List A}
              → Lookupℕ a n xs
              → Lookupℕ (f a) n (map f xs)
  Lookupℕ-map here = here
  Lookupℕ-map (there x) = there (Lookupℕ-map x)

  Updateℕ-map : ∀ {A B : Set} {a : A} {f : A → B} {n : ℕ} {xs : List A} {ys : List A}
              → Updateℕ a n xs ys
              → Updateℕ (f a) n (map f xs) (map f ys)
  Updateℕ-map here = here
  Updateℕ-map (there x) = there (Updateℕ-map x)

  Updateℕ-length : ∀ {A : Set} {a : A} {n : ℕ} {xs : List A} {ys : List A}
                 → Updateℕ a n xs ys
                 → length xs ≡ length ys
  Updateℕ-length here = refl
  Updateℕ-length (there x) = cong suc (Updateℕ-length x)

  data _∈_ {A : Set} (x : A) : (xs : List A) → Set where
    here  : ∀ {xs}     → x ∈ (x ∷ xs)
    there : ∀ {y} {xs} → x ∈ xs → x ∈ (y ∷ xs)

  Lookupℕ⇒Updateℕ : ∀ {A : Set} {x : A} {n : ℕ} {xs : List A}
                  → Lookupℕ x n xs
                  → (y : A)
                  → ∃ λ ys → Updateℕ y n xs ys
  Lookupℕ⇒Updateℕ here y = y ∷ _ , here
  Lookupℕ⇒Updateℕ (there x) y with Lookupℕ⇒Updateℕ x y
  Lookupℕ⇒Updateℕ (there x) y | fst , snd = _ ∷ fst , there snd

  Lookupℕ⇒length : ∀ {A : Set} {n : ℕ} {x : A} {xs : List A}
                 → Lookupℕ x n xs → n < length xs
  Lookupℕ⇒length here = s≤s z≤n
  Lookupℕ⇒length (there p) = s≤s (Lookupℕ⇒length p)

  Lookupℕ-length : ∀ {A : Set} (n : ℕ) (xs : List A) → (n < length xs)
                 → ∃ λ x → Lookupℕ x n xs
  Lookupℕ-length zero (x ∷ xs) p = x , here
  Lookupℕ-length (suc n) (x ∷ xs) (s≤s p)
    with Lookupℕ-length n xs p
  ... | (x' , p') = x' , there p'
