module Data.Product.Extra where

  open import Level
  open import Data.Product

  ∃₃ : ∀ {a b c d} {A : Set a} {B : A → Set b} {C : (a : A) → B a → Set c}
         (D : (a : A) → (b : B a) → (c : C a b) → Set d) → Set (a ⊔ b ⊔ c ⊔ d)
  ∃₃ D = ∃ λ a → ∃ λ b → ∃ λ c → D a b c

  -,₃_ : ∀ {a b c d} {A : Set a} {B : A → Set b} {C : (a : A) → B a → Set c}
         {D : (a : A) → (b : B a) → (c : C a b) → Set d} {a : A} → {b : B a} → {c : C a b} → D a b c → ∃₃ D
  -,₃ y = -, (-, (-, y))

  ∃₄ : ∀ {a b c d e} {A : Set a} {B : A → Set b} {C : (a : A) → B a → Set c} 
         {D : (a : A) → (b : B a) → (c : C a b) → Set d} (E : (a : A) → (b : B a) → (c : C a b) → (d : D a b c) → Set e) → Set (a ⊔ b ⊔ c ⊔ d ⊔ e)
  ∃₄ E = ∃ λ a → ∃ λ b → ∃ λ c → ∃ λ d → E a b c d

  -,₄_ : ∀ {a b c d e} {A : Set a} {B : A → Set b} {C : (a : A) → B a → Set c}
         {D : (a : A) → (b : B a) → (c : C a b) → Set d}
         {E : (a : A) → (b : B a) → (c : C a b) → (d : D a b c) → Set e} 
         {a : A} → {b : B a} → {c : C a b} → {d : D a b c} → E a b c d → ∃₄ E
  -,₄ y = -, -, (-, (-, y))
