module Exception where

  open import Relation.Binary.PropositionalEquality hiding ([_])
  open import Relation.Nullary

  postulate
    ξ       : Set
    ξ-≡-dec : ∀ (x y : ξ) → Dec (x ≡ y)
