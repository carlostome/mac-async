module Function.Extensionality where

  open import Level using (0ℓ)
  open import Axiom.Extensionality.Propositional

  postulate
    fun-ext : Extensionality 0ℓ 0ℓ
    fun-ext-implicit : ExtensionalityImplicit 0ℓ 0ℓ
