module Function.Extra where

  open import Relation.Binary.PropositionalEquality

  cong₃ : ∀ {A B C D : Set} → (f : A → B → C → D)
        → ∀ {a₁ a₂} {b₁ b₂} {c₁ c₂} → a₁ ≡ a₂ → b₁ ≡ b₂ → c₁ ≡ c₂
                                    → f a₁ b₁ c₁ ≡ f a₂ b₂ c₂
  cong₃ f refl refl refl = refl
