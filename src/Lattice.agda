module Lattice where

  open import Relation.Binary
  open import Level            using (0ℓ)
  open import Relation.Nullary
  open import Relation.Binary.PropositionalEquality

  postulate
    Label   : Set
    _⊑_     : Label → Label → Set
    ⊑-refl  : Reflexive _⊑_
    ⊑-trans : Transitive _⊑_
    Label-≡-dec   : ∀ (x y : Label) → Dec (x ≡ y)
    Label-⊑-dec   : ∀ (x y : Label) → Dec (x ⊑ y)

    ⋤-trans  : ∀ {a b c} -> a ⊑ b -> ¬ (a ⊑ c) -> ¬ (b ⊑ c)
