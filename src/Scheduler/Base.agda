module Scheduler.Base where

  open import Lattice

  open import Data.Product
  open import Data.Nat

  open import Relation.Binary.PropositionalEquality

  -------------------------------------------------------------------------------- 
  --                            Concurrent events
  --------------------------------------------------------------------------------

  -- scheduler events
  data SchedEvent (l : Label) : Set where
    fork  : (h : Label) (l⊑h : l ⊑ h) (n : ℕ) → SchedEvent l
    step  : SchedEvent l
    stuck : SchedEvent l
    done  : SchedEvent l

  data Message : Label -> Set where
    ⟪_,_,_⟫ : (l : Label) (n : ℕ) (e : SchedEvent l) -> Message l

  -------------------------------------------------------------------------------- 
  --                            Scheduler
  --------------------------------------------------------------------------------

  record Scheduler : Set₁ where
    field
        State : Set

        -- The scheduler semantics relation: ω ⟶ ω' ↑ m denotes that
        -- scheduler state ω gets updated to ω' and generates message m.
        _↑_⟶_ : ∀ {l} -> State → Message l → State → Set

        -- the scheduler is deterministic
        ↑⟶-det : ∀ {l n e} {ω₁ ω₂ ω₃ : State} -> ω₁ ↑ ⟪ l , n , e ⟫ ⟶ ω₂ → ω₁ ↑ ⟪ l , n , e ⟫ ⟶ ω₃ → ω₂ ≡ ω₃

        -- (l,n) ∈ˢ ω denotes that thread (l,n) is alive according to scheduler state ω.
        _∈ˢ_ : Label × ℕ → State -> Set

    Next : State → Label × ℕ → Set
    Next ω (l , n) = ∀ (e : SchedEvent l) → ∃ (λ ω' → ω ↑ ⟪ l , n , e ⟫ ⟶ ω' )

  ↑⟶-syntax = Scheduler._↑_⟶_; syntax ↑⟶-syntax S w m w' = w ↑[ S ] m ⟶ w'
  ↑[_]⟶-det  = Scheduler.↑⟶-det
