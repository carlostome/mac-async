-- This module defines the basic features of a non-interfering
-- scheduler.
open import Lattice using (Label)
module Scheduler.Security (A : Label) where

  open import Lattice
  import Scheduler.Base as S

  open import Scheduler.Base

  open import Data.Nat
  open import Data.Product
  open import Relation.Nullary
  open import Relation.Binary.PropositionalEquality

  -- Erasure of labeled events
  εᴱ : ∀ {l} -> SchedEvent l -> SchedEvent l
  εᴱ (fork h n p) with Label-⊑-dec h A
  εᴱ (fork h n p) | yes _ = fork h n p
  εᴱ (fork h n p) | no ¬p = step
  εᴱ step  = step
  εᴱ stuck = stuck
  εᴱ done  = done

  -- Erasure of labeled messages
  εᴹ : ∀ {l} -> Message l -> Message l
  εᴹ ⟪ l , n , e ⟫ = ⟪ l , n , εᴱ e ⟫

  -- The interface of a non-interfering scheduler.
  record NIˢ (𝓢 : S.Scheduler) : Set₁ where
    open Scheduler 𝓢 public
    field
      -- State specific erasure function
      εˢ  : State -> State

      -- State structural low-equivalence
      _≈ˢ_ : State -> State -> Set

      -- Structural low-equivalence must be equivalent to the kernel
      -- of the scheduler specific erasure function
      ⌞_⌟ˢ : ∀ {ω₁ ω₂} -> ω₁ ≈ˢ ω₂ -> εˢ ω₁ ≡ εˢ ω₂
      ⌜_⌝ˢ : ∀ {ω₁ ω₂} -> εˢ ω₁ ≡ εˢ ω₂ -> ω₁ ≈ˢ ω₂

      -- Restricted Simulation:
      -- If the scheduler performs a low step, i.e., it schedules a
      -- low thread, then that step can be simulated by the erased
      -- scheduler.
      εˢ-simᴸ : ∀ {ω₁ ω₂ : State} {l} {m : Message l}
                -> (l⊑A : l ⊑ A) ->  ω₁ ↑ m ⟶ ω₂ -> (εˢ ω₁) ↑ (εᴹ m)  ⟶ (εˢ ω₂) 

      -- No Observable Effect:
      -- Low-equivalence preservation for scheduler high steps.
      εˢ-simᴴ : ∀ {ω₁ ω₂ l} {m : Message l} -> ¬ (l ⊑ A) ->  ω₁ ↑ m ⟶ ω₂ → ω₁ ≈ˢ ω₂

      -- Annotated structural low-equivalence
      _≈ˢ⟨_,_⟩_ : State -> ℕ -> ℕ -> State -> Set

      -- Helper functions to lift and remove annotations.
      offset₁ : {ω₁ ω₂ : State} -> ω₁ ≈ˢ ω₂ -> ℕ
      offset₂ : {ω₁ ω₂ : State} -> ω₁ ≈ˢ ω₂ -> ℕ
      align : ∀ {ω₁ ω₂} -> (eq : ω₁ ≈ˢ ω₂) -> ω₁ ≈ˢ⟨ offset₁ eq , offset₂ eq ⟩ ω₂
      forget : ∀ {ω₁ ω₂ n m} -> ω₁ ≈ˢ⟨ n , m ⟩ ω₂ -> ω₁ ≈ˢ ω₂


      -- Starvation-free: If a scheduler runs a low thread, then a
      -- (i,1 + j)-low-equivalent scheduler cannot indefinitely let the
      -- same thread wait and starve: the low thread will *eventually*
      -- be scheduled in j + 1 steps.
      triangleˢ : ∀ {L H n m i j e e' ω₁ ω₂ ω₁' ω₂'}  -> L ⊑ A -> ω₁ ≈ˢ⟨ i , suc j ⟩ ω₂ ->
                    ω₁ ↑ ⟪ L , n , e ⟫  ⟶ ω₁' → ω₂ ↑ ⟪ H , m , e' ⟫ ⟶ ω₂' -> ¬ (H ⊑ A) × (ω₁ ≈ˢ⟨ i , j ⟩ ω₂')

      -- Progress: If a scheduler runs a low thread then any low-equivalent scheduler should run a thread as well.
      -- If the two schedulers are aligned (ω₁ ≈⟨ i , 0 ⟩ ω₂) then the same thread is scheduled.
      -- Otherwise (ω₁ ≈⟨ i , 1 + j ⟩ ω₂) some other thread gets scheduled.
      progressᴸ : ∀ {L i n e₁ ω₁ ω₂ ω₁'} -> (L⊑A : L ⊑ A) -> ω₁ ↑ ⟪ L , n , e₁ ⟫ ⟶ ω₁'  -> ω₁ ≈ˢ⟨ i , 0 ⟩ ω₂
                      → ((L , n) ∈ˢ ω₂) × (Next ω₂ (L , n))
      progressᴴ : ∀ {ω₁ ω₁' ω₂ L e n n₁ n₂} -> L ⊑ A -> ω₁ ≈ˢ⟨ n₁ , suc n₂ ⟩ ω₂ -> ω₁ ↑ ⟪ L , n , e ⟫ ⟶ ω₁'  -> ∃ (λ x → x ∈ˢ ω₂ × Next ω₂ x)

      -- Progress: If a scheduler runs a low thread then any low-equivalent scheduler should run a thread as well.

    refl-≈ˢ : ∀ {ω} -> ω ≈ˢ ω
    refl-≈ˢ = ⌜ refl ⌝ˢ

    sym-≈ˢ : ∀ {ω₁ ω₂} -> ω₁ ≈ˢ ω₂ -> ω₂ ≈ˢ ω₁
    sym-≈ˢ x = ⌜ sym (⌞ x ⌟ˢ) ⌝ˢ

    trans-≈ˢ : ∀ {ω₁ ω₂ ω₃} -> ω₁ ≈ˢ ω₂ -> ω₂ ≈ˢ ω₃ -> ω₁ ≈ˢ ω₃
    trans-≈ˢ x y = ⌜ trans (⌞ x ⌟ˢ) (⌞ y ⌟ˢ) ⌝ˢ

  open NIˢ

  ≈ˢ⟨⟩-syntax = NIˢ._≈ˢ⟨_,_⟩_; syntax ≈ˢ⟨⟩-syntax NIS w n n' w' = w ≈ˢ[ NIS ]⟨ n , n' ⟩ w'
