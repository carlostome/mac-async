module Security.Calculus.Base where

  open import Lattice   public
  open import Exception public
  open import Types     public

  open import Data.Bool                          using (Bool; true; false)
  open import Data.List                          using (List; _∷_; []; _++_; [_]; length; map)
  open import Data.Nat using (ℕ)
  open import Data.Product using (∃; _,_; _×_; map₂)
  open import Data.Maybe using (Maybe; just; nothing)

  open import Relation.Nullary 
  open import Relation.Binary.PropositionalEquality using (refl)

  data Term (Γ : Ctx) : Ty -> Set where

    -- Basic λ-calculus
    var : ∀ {τ}     →  (τ∈Γ : τ ∈ Γ)   → Term Γ τ
    λ′  : ∀ {τ₁ τ₂} → Term (τ₁ ∷ Γ) τ₂ → Term Γ (τ₁ ⇒ τ₂)
    _∙_ : ∀ {τ₁ τ₂} → Term Γ (τ₁ ⇒ τ₂) → Term Γ τ₁ → Term Γ τ₂

    -- Unit
    （） : Term Γ （）

    -- Booleans
    True  : Term Γ Bool'
    False : Term Γ Bool'
    if_then_else_ : ∀ {τ} → Term Γ Bool' → Term Γ τ → Term Γ τ → Term Γ τ

    -- MAC monad
    return : ∀ {τ} → (l : Label) → Term Γ τ → Term Γ (MAC l τ)
    _>>=_  : ∀ {l} {τ₁ τ₂} → Term Γ (MAC l τ₁) → Term Γ (τ₁ ⇒ MAC l τ₂) → Term Γ (MAC l τ₂)

    -- Basic labeled values
    Labeled  : ∀ {τ} → (l : Label) → Term Γ τ → Term Γ (Labeled l τ)
    label    : ∀ {l h τ} → (l⊑h : l ⊑ h) → Term Γ τ → Term Γ (MAC l (Labeled h τ))
    unlabel  : ∀ {l h τ} → (l⊑h : l ⊑ h) → Term Γ (Labeled l τ) → Term Γ (MAC h τ)

    -- Concurrency
    fork     : ∀ {l h} → (l⊑h : l ⊑ h) → Term Γ (MAC h （）) → Term Γ (MAC l (ThreadId h))

    -- Async exceptions
    χ        : ∀ {τ} (l : Label) → ξ → Term Γ (MAC l τ)
    throwTo  : ∀ {l h} (e : ξ) → (l⊑h : l ⊑ h) → Term Γ (ThreadId h) → Term Γ (MAC l （）)
    catch    : ∀ {l τ} → (hs : List (ξ × Term Γ (MAC l τ))) → Term Γ (MAC l τ) → Term Γ (MAC l τ)
    mask     : ∀ {l τ} → (e : ξ) → Term Γ (MAC l τ) → Term Γ (MAC l τ)
    unmask   : ∀ {l τ} → (e : ξ) → Term Γ (MAC l τ) → Term Γ (MAC l τ)
    #Thread  : ∀ (l : Label) → Maybe ℕ → Term Γ (ThreadId l)

    -- Synchronization variables
    newMVar   : ∀ {l h τ} → (l⊑h : l ⊑ h)     → Term Γ (MAC l (MVar h τ))
    takeMVar  : ∀ {l τ}   → Term Γ (MVar l τ) → Term Γ (MAC l τ)
    putMVar   : ∀ {l τ}   → Term Γ (MVar l τ) → Term Γ τ → Term Γ (MAC l （）)
    #MVar     : ∀ {τ} (l : Label) → Maybe ℕ → Term Γ (MVar l τ)

    -- Erasure
    newMVar●  : ∀ {l h τ} → (l⊑h : l ⊑ h) → Term Γ (MAC l (MVar h τ))
    label●    : ∀ {l h τ} → (l⊑h : l ⊑ h) → Term Γ (MAC l (Labeled h τ))
    throwTo●  : ∀ {l h} (e : ξ) → (l⊑h : l ⊑ h) → Term Γ (ThreadId h) → Term Γ (MAC l （）)
    fork●     : ∀ {l h} → (l⊑h : l ⊑ h) → Term Γ (MAC h （）) → Term Γ (MAC l (ThreadId h))

    ●         : ∀ {τ} → Term Γ τ

  infixl 25 _∙_ 
  infixr 25 _>>=_

  -- Closed term
  CTerm : Ty → Set
  CTerm = Term [] 

  -- Is the term exceptional?
  data Is-↯ {Γ} {τ} : ∀ {l} → Term Γ (MAC l τ) → Set where
    is-↯ : (l : Label) → (e : ξ) → Is-↯ (χ l e)

  Is-↯-dec : ∀ {Γ} {τ} {l} → (t : Term Γ (MAC l τ)) → Dec (Is-↯ t)
  Is-↯-dec (var τ∈Γ) = no (λ ())
  Is-↯-dec (t ∙ u) = no (λ ())
  Is-↯-dec (if t then t₁ else t₂) = no (λ ())
  Is-↯-dec (return l t) = no (λ ())
  Is-↯-dec (t >>= t₁) = no (λ ())
  Is-↯-dec (label l⊑h t) = no (λ ())
  Is-↯-dec (unlabel l⊑h t) = no (λ ())
  Is-↯-dec (fork l⊑h t) = no (λ ())
  Is-↯-dec (throwTo e l⊑h t) = no (λ ())
  Is-↯-dec (χ l x) = yes (is-↯ l x)
  Is-↯-dec (catch t h) = no (λ ())
  Is-↯-dec (mask e t) = no (λ ())
  Is-↯-dec (unmask e t) = no (λ ())
  Is-↯-dec (newMVar l⊑h) = no (λ ())
  Is-↯-dec (takeMVar t) = no (λ ())
  Is-↯-dec (putMVar t t₁) = no (λ ())
  Is-↯-dec ● = no (λ ())
  Is-↯-dec (newMVar● l⊑h) = no (λ ())
  Is-↯-dec (throwTo● l⊑h e t) = no (λ ())
  Is-↯-dec (label● l⊑h ) = no (λ ())
  Is-↯-dec (fork● l⊑h t) = no (λ ())

  mutual
    wken : ∀ {τ} {Γ₁ Γ₂ : Ctx} -> Γ₁ ⊆ Γ₂ -> Term Γ₁ τ -> Term Γ₂ τ
    wken Γ₁⊆Γ₂ (var τ∈Γ) = var (wkenⱽ Γ₁⊆Γ₂ τ∈Γ)
    wken Γ₁⊆Γ₂ (λ′ t) = λ′ (wken (keep Γ₁⊆Γ₂) t)
    wken Γ₁⊆Γ₂ (t ∙ t₁) = wken Γ₁⊆Γ₂ t ∙ wken Γ₁⊆Γ₂ t₁
    wken Γ₁⊆Γ₂ （） = （）
    wken Γ₁⊆Γ₂ True  = True
    wken Γ₁⊆Γ₂ False = False
    wken Γ₁⊆Γ₂ (if t then t₁ else t₂) = if (wken Γ₁⊆Γ₂ t) then (wken Γ₁⊆Γ₂ t₁) else (wken Γ₁⊆Γ₂ t₂)
    wken Γ₁⊆Γ₂ (return l t) = return l (wken Γ₁⊆Γ₂ t)
    wken Γ₁⊆Γ₂ (t >>= t₁) = wken Γ₁⊆Γ₂ t >>= wken Γ₁⊆Γ₂ t₁
    wken Γ₁⊆Γ₂ (Labeled l t) = Labeled l (wken Γ₁⊆Γ₂ t)
    wken Γ₁⊆Γ₂ (label l⊑h t) = label l⊑h (wken Γ₁⊆Γ₂ t)
    wken Γ₁⊆Γ₂ (unlabel l⊑h t) = unlabel l⊑h (wken Γ₁⊆Γ₂ t)
    wken Γ₁⊆Γ₂ (fork l⊑h t) = fork l⊑h (wken Γ₁⊆Γ₂ t)
    wken Γ₁⊆Γ₂ (throwTo n l⊑h t) = throwTo n l⊑h (wken Γ₁⊆Γ₂ t)
    wken Γ₁⊆Γ₂ (χ l e) = χ l e
    wken Γ₁⊆Γ₂ (catch hs t) = catch (wken-list Γ₁⊆Γ₂ hs) (wken Γ₁⊆Γ₂ t) 
    wken Γ₁⊆Γ₂ (mask e t)  = mask  e  (wken Γ₁⊆Γ₂ t)
    wken Γ₁⊆Γ₂ (unmask e t) = unmask e (wken Γ₁⊆Γ₂ t)
    wken Γ₁⊆Γ₂ (#Thread l n) = #Thread l n
    wken Γ₁⊆Γ₂ (#MVar l n) = #MVar l n
    wken Γ₁⊆Γ₂ (newMVar l⊑h) = newMVar l⊑h
    wken Γ₁⊆Γ₂ (takeMVar t) = takeMVar (wken Γ₁⊆Γ₂ t)
    wken Γ₁⊆Γ₂ (putMVar t t₁) = putMVar (wken Γ₁⊆Γ₂ t) (wken Γ₁⊆Γ₂ t₁)
    wken Γ₁⊆Γ₂ (newMVar● l⊑h) = newMVar● l⊑h
    wken Γ₁⊆Γ₂ (label● l⊑h  ) = label● l⊑h
    wken Γ₁⊆Γ₂ (throwTo● e l⊑h t) = throwTo● e l⊑h (wken Γ₁⊆Γ₂ t)
    wken Γ₁⊆Γ₂ (fork● l⊑h t) = fork● l⊑h (wken Γ₁⊆Γ₂ t)
    wken Γ₁⊆Γ₂ ● = ●

    wken-list : ∀ {τ} {Γ₁ Γ₂} → Γ₁ ⊆ Γ₂ → List (ξ × Term Γ₁ τ) → List (ξ × Term Γ₂ τ)
    wken-list Γ₁⊆Γ₂ [] = []
    wken-list Γ₁⊆Γ₂ ((e , h) ∷ hs) = (e , (wken Γ₁⊆Γ₂ h)) ∷ (wken-list Γ₁⊆Γ₂ hs)

  data Sub (Δ : Ctx) : Ctx → Set where
    []  : Sub Δ []
    _∷_ : ∀ {τ} {Γ} → Term Δ τ → Sub Δ Γ → Sub Δ (τ ∷ Γ)

  wkenS : {Γ Δ₁ Δ₂ : Ctx} -> Δ₁ ⊆ Δ₂ -> Sub Δ₁ Γ -> Sub Δ₂ Γ
  wkenS Δ₁⊆Δ₂ []      = []
  wkenS Δ₁⊆Δ₂ (t ∷ σ) = wken Δ₁⊆Δ₂ t ∷ (wkenS Δ₁⊆Δ₂ σ)

  lookup : ∀ {τ} {Γ Δ} → τ ∈ Γ → Sub Δ Γ → Term Δ τ
  lookup here (t ∷ σ) = t
  lookup (there x) (t ∷ σ) = lookup x σ
  
  extend : ∀ {Δ Γ} Σ → Sub Δ Γ → Sub (Σ ++ Δ) (Σ ++ Γ)
  extend [] σ      = σ
  extend (τ ∷ Σ) σ = (var here) ∷ wkenS (drop ⊆-refl) (extend Σ σ)

  open import Relation.Binary.PropositionalEquality hiding (subst)

  lookup-wken : ∀ {Γ} {Δ₁ Δ₂} {τ} → (τ∈Γ : τ ∈ Γ) (Δ₁⊆Δ₂ : Δ₁ ⊆ Δ₂)
                → (σ : Sub Δ₁ Γ) → lookup τ∈Γ (wkenS Δ₁⊆Δ₂ σ) ≡ wken Δ₁⊆Δ₂ (lookup τ∈Γ σ)
  lookup-wken here Δ₁⊆Δ₂ (_ ∷ σ) = refl
  lookup-wken (there τ∈Γ) Δ₁⊆Δ₂ (_ ∷ σ) = lookup-wken τ∈Γ Δ₁⊆Δ₂ σ

  mutual
    subst : ∀ {τ} {Γ Δ} → Sub Δ Γ → Term Γ τ → Term Δ τ
    subst σ (var τ∈Γ) = lookup τ∈Γ σ
    subst σ (λ′ t) = λ′ (subst (extend _ σ) t)
    subst σ (t ∙ u) = subst σ t ∙ subst σ u
    subst σ （） = （）
    subst σ True = True
    subst σ False = False
    subst σ (if t then t₁ else t₂) = if subst σ t then subst σ t₁ else subst σ t₂
    subst σ (return l t) = return l (subst σ t)
    subst σ (t >>= t₁) = subst σ t >>= subst σ t₁
    subst σ (Labeled l t) = Labeled l (subst σ t)
    subst σ (label l⊑h t) = label l⊑h (subst σ t)
    subst σ (unlabel l⊑h t) = unlabel l⊑h (subst σ t)
    subst σ (fork l⊑h t) = fork l⊑h (subst σ t)
    subst σ (χ l x) = χ l x
    subst σ (throwTo e l⊑h t) = throwTo e l⊑h (subst σ t)
    subst σ (catch hs t) = catch (subst-list σ hs) (subst σ t)
    subst σ (mask e t) = mask e (subst σ t)
    subst σ (unmask e t) = unmask e (subst σ t)
    subst σ (#Thread l n) = #Thread l n
    subst σ (newMVar l⊑h) = newMVar l⊑h
    subst σ (takeMVar t) = takeMVar (subst σ t)
    subst σ (putMVar t t₁) = putMVar (subst σ t) (subst σ t₁)
    subst σ (#MVar l n) = #MVar l n
    subst σ (newMVar● l⊑h) = newMVar● l⊑h
    subst σ (label● l⊑h)   = label● l⊑h
    subst σ (throwTo● l⊑h e t) = throwTo● l⊑h e (subst σ t)
    subst σ (fork● l⊑h t) = fork● l⊑h (subst σ t)
    subst σ ● = ●

    subst-list : ∀ {τ} {Δ Γ} → Sub Δ Γ → List (ξ × Term Γ τ) → List (ξ × Term Δ τ)
    subst-list Γ₁⊆Γ₂ [] = []
    subst-list Γ₁⊆Γ₂ ((e , h) ∷ hs) = (e , (subst Γ₁⊆Γ₂ h)) ∷ (subst-list Γ₁⊆Γ₂ hs)

  [_/_]  : ∀ {τ₁ τ₂} → Term (τ₁ ∷ []) τ₂ → CTerm τ₁ → CTerm τ₂
  [_/_] {τ₁} {τ₂} t u = subst (u ∷ []) t
