module Security.Calculus.Erasure where

  open import Security.Calculus.Base as Cˢ
  open import Calculus.Base          as C

  open import Data.Bool                          using (Bool; true; false)
  open import Data.List                          using (List; _∷_; []; _++_; [_]; map)
  open import Data.Nat using (ℕ)
  open import Data.Product using (∃; _,_; map₂; _×_)
  open import Data.Maybe using (Maybe; just) renaming (nothing to ●)
  open import Function using (_∘′_)
  open import Function.Extra 

  open import Data.Empty using (⊥-elim)
  open import Relation.Nullary 
  open import Relation.Binary.PropositionalEquality using (_≡_; refl; cong; cong₂)

  mutual
    ε : (A : Label) → ∀ {Γ} {τ} → C.Term Γ τ → Cˢ.Term Γ τ
    ε l (var τ∈Γ) = var τ∈Γ
    ε l (λ′ t)    = λ′ (ε l t)
    ε l (t ∙ u)   = ε l t ∙ ε l u
    ε l （）   = （）
    ε l True  = True
    ε l False = False
    ε l (if t then t₁ else t₂) = if (ε l t) then (ε l t₁) else (ε l t₂)
    ε l (return l' t)  = return l' (ε l t)
    ε l (t >>= u)      = ε l t >>= ε l u
    ε l (Labeled l' t)
      with Label-⊑-dec l' l
    ... | .true because ofʸ p   = Labeled l' (ε l t)
    ... | .false because ofⁿ ¬p = Labeled l' ●
    ε l (label {h = h} l⊑h t)
      with Label-⊑-dec h l
    ... | .true because ofʸ p   = label l⊑h (ε l t)
    ... | .false because ofⁿ ¬p = label● l⊑h
    ε l (unlabel l⊑h t) = unlabel l⊑h (ε l t)
    ε l (fork {h = h} l⊑h t)
      with Label-⊑-dec h l
    ... | .true because ofʸ p   = fork l⊑h (ε l t)
    ... | .false because ofⁿ ¬p = fork● l⊑h (ε l t)
    ε l (χ l' x)        = χ l' x
    ε l (throwTo {h = h} e l⊑h t)
      with Label-⊑-dec h l
    ... | .true because ofʸ p   = throwTo e l⊑h (ε l t)
    ... | .false because ofⁿ ¬p = throwTo● e l⊑h (ε l t)
    ε l (catch hs t)  = catch (ε-list l hs) (ε l t)
    ε l (mask e t)    = mask e (ε l t)
    ε l (unmask e t)  = unmask e (ε l t)
    ε l (#Thread l' n)
      with Label-⊑-dec l' l
    ... | .true because ofʸ p   = #Thread  l' (just n)
    ... | .false because ofⁿ ¬p = #Thread l' ●
    ε l (newMVar {h = h} l⊑h)
      with Label-⊑-dec h l
    ... | .true because ofʸ p   = newMVar l⊑h
    ... | .false because ofⁿ ¬p = newMVar● l⊑h
    ε l (takeMVar t)   = takeMVar (ε l t)
    ε l (putMVar t u)  = putMVar (ε l t) (ε l u)
    ε l (#MVar l' n)
      with Label-⊑-dec l' l
    ... | .true because ofʸ p   = #MVar l' (just n)
    ... | .false because ofⁿ ¬p = #MVar l' ●

    ε-list : (l : Label) → ∀ {Γ} {τ} → List (ξ × C.Term Γ τ) → List (ξ × Cˢ.Term Γ τ)
    ε-list l [] = []
    ε-list l ((ex , h) ∷ hs) = (ex , ε l h) ∷ ε-list l hs 

  ε-Is-↯-⇒ : ∀ (A : Label) (l : Label) → (l⊑A : l ⊑ A)
               {τ : Ty} (t : C.CTerm (MAC l τ))
             → C.Is-↯ t → Cˢ.Is-↯ (ε A t)
  ε-Is-↯-⇒ A l l⊑A .(χ l e) (is-↯ .l e) = is-↯ l e

  ε-¬-Is-↯-⇒ : ∀ (A : Label) (l : Label) → (l⊑A : l ⊑ A)
               {τ : Ty} (t : C.CTerm (MAC l τ))
               → ¬ C.Is-↯ t → ¬ Cˢ.Is-↯ (ε A t)
  ε-¬-Is-↯-⇒ A l l⊑A (t ∙ t₁) = λ _ ()
  ε-¬-Is-↯-⇒ A l l⊑A (if t then t₁ else t₂) = λ _ ()
  ε-¬-Is-↯-⇒ A l l⊑A (return .l t) = λ _ ()
  ε-¬-Is-↯-⇒ A l l⊑A (t >>= t₁) = λ _ ()
  ε-¬-Is-↯-⇒ A l l⊑A (label {h = h} l⊑h t)
    with Label-⊑-dec h A
  ... | .true because ofʸ p = λ _ () 
  ... | .false because ofⁿ ¬p = λ _ ()
  ε-¬-Is-↯-⇒ A l l⊑A (unlabel l⊑h t) = λ _ ()
  ε-¬-Is-↯-⇒ A l l⊑A (fork {h = h} l⊑h t)
    with Label-⊑-dec h A
  ... | .true because ofʸ p = λ _ () 
  ... | .false because ofⁿ ¬p = λ _ ()
  ε-¬-Is-↯-⇒ A l l⊑A (χ .l x) = λ ¬p _ → ¬p (is-↯ l x)
  ε-¬-Is-↯-⇒ A l l⊑A (throwTo {h = h} e l⊑h t)
    with Label-⊑-dec h A
  ... | .true because ofʸ p = λ _ () 
  ... | .false because ofⁿ ¬p = λ _ ()
  ε-¬-Is-↯-⇒ A l l⊑A (catch hs t) = λ _ ()
  ε-¬-Is-↯-⇒ A l l⊑A (mask e t) = λ _ ()
  ε-¬-Is-↯-⇒ A l l⊑A (unmask e t) = λ _ ()
  ε-¬-Is-↯-⇒ A l l⊑A (newMVar {h = h} l⊑h)
    with Label-⊑-dec h A
  ... | .true because ofʸ p = λ _ () 
  ... | .false because ofⁿ ¬p = λ _ ()
  ε-¬-Is-↯-⇒ A l l⊑A (takeMVar t) = λ _ ()
  ε-¬-Is-↯-⇒ A l l⊑A (putMVar t t₁) = λ _ ()

  ε-Sub : (A : Label) → {Γ Δ : Ctx} → C.Sub Δ Γ → Cˢ.Sub Δ Γ
  ε-Sub A [] = []
  ε-Sub A (t ∷ σ) = ε A t ∷ ε-Sub A σ

  mutual
    wken-hom-ε : ∀ (A : Label) {τ} {Δ₁ Δ₂} (Δ₁⊆Δ₂ : Δ₁ ⊆ Δ₂) (t : C.Term Δ₁ τ)
                 → Cˢ.wken Δ₁⊆Δ₂ (ε A t) ≡ ε A (C.wken Δ₁⊆Δ₂ t)
    wken-hom-ε A Δ₁⊆Δ₂ (var τ∈Γ) = refl
    wken-hom-ε A Δ₁⊆Δ₂ (λ′ t) = cong λ′ (wken-hom-ε A (keep Δ₁⊆Δ₂) t)
    wken-hom-ε A Δ₁⊆Δ₂ (t ∙ u) = cong₂ _∙_ (wken-hom-ε A Δ₁⊆Δ₂ t) (wken-hom-ε A Δ₁⊆Δ₂ u)
    wken-hom-ε A Δ₁⊆Δ₂ （） = refl
    wken-hom-ε A Δ₁⊆Δ₂ True = refl
    wken-hom-ε A Δ₁⊆Δ₂ False = refl
    wken-hom-ε A Δ₁⊆Δ₂ (if t then t₁ else t₂)
      = cong₃ (if_then_else_ ) (wken-hom-ε A Δ₁⊆Δ₂ t) (wken-hom-ε A Δ₁⊆Δ₂ t₁) (wken-hom-ε A Δ₁⊆Δ₂ t₂)
    wken-hom-ε A Δ₁⊆Δ₂ (return l t) = cong (return l) (wken-hom-ε A Δ₁⊆Δ₂ t)
    wken-hom-ε A Δ₁⊆Δ₂ (t >>= u) = cong₂ _>>=_ (wken-hom-ε A Δ₁⊆Δ₂ t) (wken-hom-ε A Δ₁⊆Δ₂ u)
    wken-hom-ε A Δ₁⊆Δ₂ (Labeled l t)
      with Label-⊑-dec l A
    ... | .true because ofʸ p = cong (Labeled l) (wken-hom-ε A Δ₁⊆Δ₂ t)
    ... | .false because ofⁿ ¬p = refl
    wken-hom-ε A Δ₁⊆Δ₂ (label {h = h} l⊑h t)
      with Label-⊑-dec h A
    ... | .true because ofʸ p = cong (label l⊑h) (wken-hom-ε A Δ₁⊆Δ₂ t)
    ... | .false because ofⁿ ¬p = refl
    wken-hom-ε A Δ₁⊆Δ₂ (unlabel l⊑h t) = cong (unlabel l⊑h) (wken-hom-ε A Δ₁⊆Δ₂ t)
    wken-hom-ε A Δ₁⊆Δ₂ (fork {h = h} l⊑h t)
      with Label-⊑-dec h A
    ... | .true because ofʸ p = cong (fork l⊑h) (wken-hom-ε A Δ₁⊆Δ₂ t)
    ... | .false because ofⁿ ¬p =  cong (fork● l⊑h) (wken-hom-ε A Δ₁⊆Δ₂ t)
    wken-hom-ε A Δ₁⊆Δ₂ (χ l x) = refl
    wken-hom-ε A Δ₁⊆Δ₂ (throwTo {h = h} e l⊑h t)
      with Label-⊑-dec h A
    ... | .true because ofʸ p = cong (throwTo e l⊑h) (wken-hom-ε A Δ₁⊆Δ₂ t)
    ... | .false because ofⁿ ¬p =  cong (throwTo● e l⊑h) (wken-hom-ε A Δ₁⊆Δ₂ t)
    wken-hom-ε A Δ₁⊆Δ₂ (catch hs t) = cong₂ catch (wken-list-hom-ε A Δ₁⊆Δ₂ hs) (wken-hom-ε A Δ₁⊆Δ₂ t)
    wken-hom-ε A Δ₁⊆Δ₂ (mask e t) = cong (mask e) (wken-hom-ε A Δ₁⊆Δ₂ t)
    wken-hom-ε A Δ₁⊆Δ₂ (unmask e t) = cong (unmask e) (wken-hom-ε A Δ₁⊆Δ₂ t)
    wken-hom-ε A Δ₁⊆Δ₂ (#Thread l n)
      with Label-⊑-dec l A
    ... | .true because ofʸ p = refl
    ... | .false because ofⁿ ¬p = refl
    wken-hom-ε A Δ₁⊆Δ₂ (newMVar {h = h} l⊑h)
      with Label-⊑-dec h A
    ... | .true because ofʸ p = refl
    ... | .false because ofⁿ ¬p = refl
    wken-hom-ε A Δ₁⊆Δ₂ (takeMVar t) = cong takeMVar (wken-hom-ε A Δ₁⊆Δ₂ t)
    wken-hom-ε A Δ₁⊆Δ₂ (putMVar t u) = cong₂ putMVar (wken-hom-ε A Δ₁⊆Δ₂ t) (wken-hom-ε A Δ₁⊆Δ₂ u)
    wken-hom-ε A Δ₁⊆Δ₂ (#MVar l n)
      with Label-⊑-dec l A
    ... | .true because ofʸ p = refl
    ... | .false because ofⁿ ¬p = refl

    wken-list-hom-ε : ∀ (A : Label) {τ} {Δ₁ Δ₂} (Δ₁⊆Δ₂ : Δ₁ ⊆ Δ₂) (hs : List (ξ × C.Term Δ₁ τ))
                     → Cˢ.wken-list Δ₁⊆Δ₂ (ε-list A hs) ≡ ε-list A (C.wken-list Δ₁⊆Δ₂ hs)
    wken-list-hom-ε A Δ₁⊆Δ₂ [] = refl
    wken-list-hom-ε A Δ₁⊆Δ₂ ((e , t) ∷ hs) = cong₂ _∷_ (cong (e ,_) (wken-hom-ε A Δ₁⊆Δ₂ t)) (wken-list-hom-ε A Δ₁⊆Δ₂ hs)

  wken-hom-lookup-ε : ∀ (A : Label) {Γ} {Δ₁ Δ₂} {τ} (τ∈Γ : τ ∈ Γ) (Δ₁⊆Δ₂ : Δ₁ ⊆ Δ₂)
                → (σ : C.Sub Δ₁ Γ)
                → Cˢ.wken Δ₁⊆Δ₂ (ε A (C.lookup τ∈Γ σ)) ≡ ε A (C.lookup τ∈Γ (C.wkenS Δ₁⊆Δ₂ σ))
  wken-hom-lookup-ε A here Δ₁⊆Δ₂ (t ∷ σ) = wken-hom-ε A Δ₁⊆Δ₂ t
  wken-hom-lookup-ε A (there τ∈Γ) Δ₁⊆Δ₂ (t ∷ σ) = wken-hom-lookup-ε A τ∈Γ Δ₁⊆Δ₂ σ

  subst-var : ∀ (A : Label) {Γ Δ : Ctx} Σ {τ : Ty} (τ∈Γ : τ Cˢ.∈ (Σ ++ Γ)) (σ : C.Sub Δ Γ)
              → Cˢ.lookup τ∈Γ (Cˢ.extend Σ (ε-Sub A σ)) ≡ ε A (C.lookup τ∈Γ (C.extend Σ σ))
  subst-var A [] τ∈Γ σ = aux τ∈Γ σ
    where aux : ∀ {Γ} {Δ} {τ} (τ∈Γ : τ ∈ Γ) (σ : C.Sub Δ Γ) →
                Cˢ.lookup τ∈Γ (ε-Sub A σ) ≡ ε A (C.lookup τ∈Γ σ)
          aux here (x ∷ σ) = refl
          aux (there τ∈Γ) (x ∷ σ) = aux τ∈Γ σ
  subst-var A (x ∷ Σ) here σ = refl
  subst-var A (α ∷ Σ) {τ} (there τ∈Γ) σ
    rewrite Cˢ.lookup-wken τ∈Γ (drop {α} ⊆-refl) (Cˢ.extend Σ (ε-Sub A σ))
          | subst-var A Σ τ∈Γ σ
          = wken-hom-lookup-ε A τ∈Γ (drop ⊆-refl) (C.extend Σ σ)

  mutual
    subst-hom-ε : ∀ (A : Label) {Γ Δ} {Σ} {τ : Ty} (t : C.Term (Σ ++ Γ) τ) (σ : C.Sub Δ Γ)
                    → Cˢ.subst (Cˢ.extend Σ (ε-Sub A σ)) (ε A t) ≡ ε A (C.subst (C.extend Σ σ) t)
    subst-hom-ε A (var τ∈Γ) σ = subst-var A _ τ∈Γ σ
    subst-hom-ε A (λ′ t) σ = cong λ′ (subst-hom-ε A t σ)
    subst-hom-ε A (t ∙ u) σ = cong₂ _∙_ (subst-hom-ε A t σ) (subst-hom-ε A u σ)
    subst-hom-ε A （） σ = refl
    subst-hom-ε A True σ = refl
    subst-hom-ε A False σ = refl
    subst-hom-ε A (if t then t₁ else t₂) σ = cong₃ if_then_else_ (subst-hom-ε A t σ) (subst-hom-ε A t₁ σ) (subst-hom-ε A t₂ σ)
    subst-hom-ε A (return l t) σ = cong (return l) (subst-hom-ε A t σ)
    subst-hom-ε A (t >>= u) σ = cong₂ _>>=_ (subst-hom-ε A t σ) (subst-hom-ε A u σ)
    subst-hom-ε A (Labeled l t) σ
      with Label-⊑-dec l A
    ... | .true because ofʸ p = cong (Labeled l) (subst-hom-ε A t σ)
    ... | .false because ofⁿ ¬p = refl
    subst-hom-ε A (label {h = h} l⊑h t) σ
      with Label-⊑-dec h A
    ... | .true because ofʸ p = cong (label l⊑h) (subst-hom-ε A t σ)
    ... | .false because ofⁿ ¬p = refl
    subst-hom-ε A (unlabel l⊑h t) σ = cong (unlabel l⊑h) (subst-hom-ε A t σ)
    subst-hom-ε A (fork {h = h} l⊑h t) σ
      with Label-⊑-dec h A
    ... | .true because ofʸ p = cong (fork l⊑h) (subst-hom-ε A t σ)
    ... | .false because ofⁿ ¬p = cong (fork● l⊑h) (subst-hom-ε A t σ)
    subst-hom-ε A (χ l x) σ = refl
    subst-hom-ε A (throwTo {h = h} e l⊑h t) σ
      with Label-⊑-dec h A
    ... | .true because ofʸ p = cong (throwTo e l⊑h) (subst-hom-ε A t σ)
    ... | .false because ofⁿ ¬p = cong (throwTo● e l⊑h) (subst-hom-ε A t σ)
    subst-hom-ε A (catch hs t) σ = cong₂ catch (subst-list-hom-ε A hs σ) (subst-hom-ε A t σ)
    subst-hom-ε A (mask e t) σ = cong (mask e) (subst-hom-ε A t σ)
    subst-hom-ε A (unmask e t) σ = cong (unmask e) (subst-hom-ε A t σ)
    subst-hom-ε A (#Thread l n) σ
      with Label-⊑-dec l A
    ... | .true because ofʸ p = refl
    ... | .false because ofⁿ ¬p = refl
    subst-hom-ε A (newMVar {h = h} l⊑h) σ
      with Label-⊑-dec h A
    ... | .true because ofʸ p = refl
    ... | .false because ofⁿ ¬p = refl
    subst-hom-ε A (takeMVar t) σ = cong takeMVar (subst-hom-ε A t σ)
    subst-hom-ε A (putMVar t u) σ = cong₂ putMVar (subst-hom-ε A t σ) ((subst-hom-ε A u σ))
    subst-hom-ε A (#MVar l n) σ
      with Label-⊑-dec l A
    ... | .true because ofʸ p = refl
    ... | .false because ofⁿ ¬p = refl

    subst-list-hom-ε : ∀ (A : Label) {τ} {Γ Δ} {Σ} (hs : List (ξ × C.Term (Σ ++ Γ) τ)) (σ : C.Sub Δ Γ)
                      → Cˢ.subst-list (Cˢ.extend Σ (ε-Sub A σ)) (ε-list A hs) ≡ ε-list A (C.subst-list (C.extend Σ σ) hs)
    subst-list-hom-ε A [] σ = refl
    subst-list-hom-ε A ((e , h) ∷ hs) σ = cong₂ _∷_ (cong (e ,_) ((subst-hom-ε A h σ))) (subst-list-hom-ε A hs σ)
