module Security.Concurrent.Calculus where

  open import Security.Calculus.Base
  open import Calculus.Base as C using ()
  open import Security.Sequential.Calculus

  open import Data.List using (List; length; [])
  open import Data.List.Extra
  open import Data.Maybe using (Maybe; just; map) renaming (nothing to ●)
  open import Data.Nat  using (ℕ)
  open import Data.Product using (_×_; Σ)
  open import Data.Unit using (⊤; tt)

  open import Relation.Binary.PropositionalEquality using (_≡_; refl; cong)
  open import Relation.Nullary
  open import Data.Empty

  -------------------------------------------------------------------------------- 
  --                               Thread
  --------------------------------------------------------------------------------

  -- A thread of control
  record Thread' (l : Label) : Set where 
    constructor ⟨_,_,_⟩
    field
      term       : CTerm (MAC l （）)    
      masked     : ξ-Mask               
      exceptions : ξ-List l            

  open Thread'

  Thread : (l : Label) → Set
  Thread l = Maybe (Thread' l)

  -------------------------------------------------------------------------------- 
  --                            Thread pool
  --------------------------------------------------------------------------------

  -- A labeled thread pool
  Pool : (l : Label) → Set
  Pool l = Maybe (List (Thread l))
  
  TPool : Set
  TPool = (l : Label) → Pool l

  _[_↦_]ᵀ : TPool -> (l : Label) -> Pool l -> TPool
  _[_↦_]ᵀ Φ l P l' with Label-≡-dec l l'
  ... | yes refl = P
  ... | no ¬p    = Φ l'

  -- add exception to the thread
  Th-add-ξ : ∀ {l} → ξ → (l' : Label) →  (l'⊑l : l' ⊑ l) → Thread l → Thread l
  Th-add-ξ ex l' l'⊑l (just ⟨ term , ξ-mask , exs ⟩) = just ⟨ term , ξ-mask , ξ-L-add exs ex l' l'⊑l ⟩
  Th-add-ξ ex l' l'⊑l ●                              = ●

  -- Sytactic sugar for Lookupℕ in a Pool
  _↦_∈ᴾ_ : ∀ {l} -> ℕ -> Thread l -> Pool l -> Set
  n ↦ t ∈ᴾ ●      = ⊥
  n ↦ t ∈ᴾ just P = Lookupℕ t n P

  -- Syntatic sugar for updating a Pool.
  _≔_[_↦_]ᴾ : ∀ {l} -> Pool l -> Pool l -> ℕ -> Thread l -> Set
  P' ≔ ● [ n ↦ t ]ᴾ           = ⊥
  ● ≔ just P [ n ↦ t ]ᴾ       = ⊥
  just P' ≔ just P [ n ↦ t ]ᴾ = Updateℕ t n P P'

  sizeᵀ : TPool → Label → ℕ
  sizeᵀ Φ l with Φ l
  sizeᵀ Φ l | ●        = 0
  sizeᵀ Φ l | just ths = length ths

  mkIdˢ : TPool → Idˢ
  mkIdˢ Φ l = map length (Φ l)

  -------------------------------------------------------------------------------- 
  --                            Global configuration
  --------------------------------------------------------------------------------

  open import Scheduler.Base

  module _ (Sch : Scheduler) where

    open Scheduler Sch

    record Global : Set where
      constructor ⟨_,_,_⟩
      field
        tpool : TPool
        store : Store
        state : State

  -------------------------------------------------------------------------------- 
  -- properties

  ↦∈ᴾ-inj : ∀ {l} {P : Pool l} {n} {th₁ th₂ : Thread l}
            → n ↦ th₁ ∈ᴾ P
            → n ↦ th₂ ∈ᴾ P
            → th₁ ≡ th₂
  ↦∈ᴾ-inj {_} {just P} p₁ p₂ = Lookupℕ-inj p₁ p₂
  
  ≔[]ᴾ-inj : ∀ {l} {P P₁ P₂ : Pool l} {n} {th : Thread l}
            → P₁ ≔ P [ n ↦ th ]ᴾ
            → P₂ ≔ P [ n ↦ th ]ᴾ
            → P₁ ≡ P₂
  ≔[]ᴾ-inj {_} {just _} {just _} {just _} p₁ p₂ = cong just (Updateℕ-inj p₁ p₂)
