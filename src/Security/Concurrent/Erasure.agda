open import Lattice using (Label)

module Security.Concurrent.Erasure (A : Label) where

  open import Lattice
  
  open import Sequential.Calculus          as SC
  open import Concurrent.Calculus          as CC

  open import Calculus.Base                as C
  open import Security.Calculus.Base       as Cˢ
  open import Security.Sequential.Calculus as SCˢ
  open import Security.Concurrent.Calculus as CCˢ renaming (_[_↦_]ᵀ to _[_↦_]ᵀˢ)

  open import Security.Calculus.Erasure
  open import Security.Sequential.Erasure

  open import Data.Bool               using (Bool; true; false)
  open import Data.Empty              using (⊥-elim)
  open import Data.List               using (List; _∷_; []; map; length)
  open import Data.List.Extra
  open import Data.Maybe              using (just) renaming (nothing to ●) 
  open import Data.Nat                using (ℕ)
  open import Function.Extensionality using (fun-ext)

  open import Relation.Nullary 
  open import Relation.Binary.PropositionalEquality renaming (subst to ≡-subst)


  -------------------------------------------------------------------------------- 
  --                               Thread
  --------------------------------------------------------------------------------

  εₜ-Thread : (l : Label) → CC.Thread l → CCˢ.Thread l
  εₜ-Thread l ⟨ t , m , exs ⟩ = just ⟨ ε A t , m , εₜ-ξ-List A l exs ⟩

  εᵗ-Thread : (l : Label) → CC.Thread l → CCˢ.Thread l
  εᵗ-Thread l th with Label-⊑-dec l A
  εᵗ-Thread l th | .true because ofʸ p   = εₜ-Thread l th
  εᵗ-Thread l th | .false because ofⁿ ¬p = ●

  -------------------------------------------------------------------------------- 
  --                            Thread pool
  --------------------------------------------------------------------------------

  ε-Pool : (l : Label) → CC.Pool l → CCˢ.Pool l
  ε-Pool l P = just (map (εₜ-Thread l) P)

  ε-TPool : CC.TPool → CCˢ.TPool
  ε-TPool Φ l with Label-⊑-dec l A
  ε-TPool Φ l | .true because ofʸ p   = ε-Pool l (Φ l)
  ε-TPool Φ l | .false because ofⁿ ¬p = ●

  -------------------------------------------------------------------------------- 
  --                            Global configuration
  --------------------------------------------------------------------------------

  open import Scheduler.Base
  open import Scheduler.Security

  module _ {Sch : Scheduler} {Sch-NIˢ : NIˢ A Sch} where

    open Scheduler Sch
    open NIˢ Sch-NIˢ

    εᴳ : CC.Global Sch → CCˢ.Global Sch
    εᴳ ⟨ pool , store , state ⟩ = ⟨ ε-TPool pool , εₛ A store , εˢ state ⟩

  -------------------------------------------------------------------------------- 
  --                               Lemmas
  --------------------------------------------------------------------------------

  private
    ε-Thread-[↦]-com-high-aux :(l : Label) (l⋤A : ¬ (l ⊑ A))
                              → (Φ : CC.TPool) (P : CC.Pool l)
                              → ∀ l'
                              → ε-TPool Φ l' ≡ ε-TPool (Φ [ l ↦ P ]ᵀ) l'
    ε-Thread-[↦]-com-high-aux l l⋤A P pool l'
      with Label-⊑-dec l A
    ... | .true because ofʸ p   = ⊥-elim (l⋤A p)
    ... | .false because ofⁿ ¬p
      with Label-⊑-dec l' A
    ε-Thread-[↦]-com-high-aux l l⋤A P pool l' | .false because ofⁿ ¬p | .true because ofʸ p
      with Label-≡-dec l l'
    ... | .true because ofʸ refl = ⊥-elim (l⋤A p)
    ... | .false because ofⁿ ¬p₁ = refl
    ε-Thread-[↦]-com-high-aux l l⋤A P pool l' | .false because ofⁿ ¬p | .false because ofⁿ ¬p' = refl

  ε-Thread-[↦]-com-high : (l : Label) (l⋤A : ¬ (l ⊑ A))
                        → (P : CC.TPool) (pool : List (CC.Thread l))
                        → ε-TPool P ≡ ε-TPool (P [ l ↦ pool ]ᵀ)
  ε-Thread-[↦]-com-high l l⋤A P pool  = fun-ext (ε-Thread-[↦]-com-high-aux l l⋤A P pool)

  ε-Thread-[↦]-com-low-aux : (l : Label) → (l⊑A : l ⊑ A)
          → (Φ : CC.TPool) → (P : CC.Pool l)
          → ∀ l'
          → (ε-TPool (Φ CC.[ l ↦ P ]ᵀ)) l' ≡ (ε-TPool Φ CCˢ.[ l ↦ ε-Pool l P ]ᵀ) l'
  ε-Thread-[↦]-com-low-aux l l⊑A Φ P l'
    with Label-≡-dec l l'
  ... | .true because ofʸ refl
      with Label-⊑-dec l A
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ... | .true because ofʸ p
      with Label-≡-dec l l
  ... | .true because ofʸ refl = refl
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p refl)
  ε-Thread-[↦]-com-low-aux l l⊑A Φ P l' | .false because ofⁿ ¬p 
      with Label-⊑-dec l A
  ... | .false because ofⁿ ¬p' = ⊥-elim (¬p' l⊑A)
  ... | .true because ofʸ p
      with Label-⊑-dec l' A
  ... | .false because ofⁿ ¬p₁ = refl
  ... | .true because ofʸ p'
      with Label-≡-dec l l'
  ... | .true because ofʸ p₁ = ⊥-elim (¬p p₁)
  ... | .false because ofⁿ ¬p₁ = refl

  ε-Thread-[↦]-com-low : (l : Label) → (l⊑A : l ⊑ A)
                       → (Φ : CC.TPool) (P : CC.Pool l)
                       → ε-TPool (Φ CC.[ l ↦ P ]ᵀ) ≡ ε-TPool Φ CCˢ.[ l ↦ ε-Pool l P ]ᵀ
  ε-Thread-[↦]-com-low l l⊑A Φ P = fun-ext (ε-Thread-[↦]-com-low-aux l l⊑A Φ P)

  ↦∈ᴾ-com : ∀ (l : Label) → (l⊑A : l ⊑ A)
              (Φ : CC.TPool) (n : ℕ) (th : CC.Thread l)
            → n CC.↦  th ∈ᴾ (Φ l)
            → n CCˢ.↦ (εₜ-Thread l th) ∈ᴾ (ε-TPool Φ l)
  ↦∈ᴾ-com l l⊑A Φ n th P with Label-⊑-dec l A
  ↦∈ᴾ-com l l⊑A Φ n th P | .true because ofʸ p = aux n th (Φ l) P
    where
        aux : ∀ (n : ℕ) (th : CC.Thread l) (P : CC.Pool l)
              → n CC.↦  th ∈ᴾ P
              → n CCˢ.↦ (εₜ-Thread l th) ∈ᴾ (ε-Pool l P)
        aux _ th .(th ∷ _) here = here
        aux .(_) th .(_ ∷ _) (there x) = there (Lookupℕ-map x)
  ↦∈ᴾ-com l l⊑A Φ n th P | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
              
  ↦∈ᴾ-com₂ : ∀ (l : Label) → (l⊑A : l ⊑ A)
               (Φ : CC.TPool) (n : ℕ) (h : Label) (h⊑A : h ⊑ A) (th : CC.Thread h) (P : CC.Pool l) 
             → n CC.↦  th ∈ᴾ ((Φ CC.[ l ↦ P ]ᵀ) h)
             → n CCˢ.↦ (εₜ-Thread h th) ∈ᴾ (((ε-TPool Φ) CCˢ.[ l ↦ ε-Pool l P ]ᵀ) h)
  ↦∈ᴾ-com₂ l l⊑A Φ n h h⊑A th P x
    with Label-≡-dec l h
  ... | .true because ofʸ refl = Lookupℕ-map x
  ... | .false because ofⁿ ¬p
    with Label-⊑-dec h A
  ... | .true because ofʸ p    = Lookupℕ-map x
  ... | .false because ofⁿ ¬p₁ = ¬p₁ h⊑A

  ≔[↦]ᴾ-com : ∀ (l : Label) → (l⊑A : l ⊑ A)
                (Φ : CC.TPool) (n : ℕ) (th : CC.Thread l) (P : CC.Pool l)
              → P CC.≔ (Φ l) [ n ↦ th ]ᴾ
              → (ε-Pool l P) CCˢ.≔ (ε-TPool Φ l) [ n ↦ εₜ-Thread l th ]ᴾ
  ≔[↦]ᴾ-com l l⊑A Φ n th P p
    with Label-⊑-dec l A
  ≔[↦]ᴾ-com l l⊑A Φ n th P p | .true because ofʸ p'  = aux n th P (Φ l) p
    where  aux : ∀ (n : ℕ) (th : CC.Thread l) (P : CC.Pool l) (P' : CC.Pool l)
                 → P CC.≔ P' [ n ↦ th ]ᴾ
                 → (ε-Pool l P) CCˢ.≔ (ε-Pool l P') [ n ↦ εₜ-Thread l th ]ᴾ
           aux .0 th .(th ∷ _) .(_ ∷ _) here = here
           aux .(_) th .(_ ∷ _) .(_ ∷ _) (there x) = there (Updateℕ-map x)
  ≔[↦]ᴾ-com l l⊑A Φ n th P p | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
