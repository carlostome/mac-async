open import Lattice using (Label)
open import Scheduler.Base
import Scheduler.Security as Schˢ
module Security.Concurrent.LowEq (A : Label) (Sch : Scheduler) (Sch-NIˢ : Schˢ.NIˢ A Sch) where

  open import Calculus.Base as C using ()

  open import Security.Calculus.Base
  open import Security.Concurrent.Erasure
  open import Security.Sequential.Erasure

  open import Concurrent.Calculus as CC
  open import Security.Concurrent.Calculus as CCˢ

  open import Sequential.Calculus as SC
  open import Security.Sequential.Calculus as SCˢ
  open import Security.Sequential.LowEq A

  open import Data.List using (List; length; [])
  open import Data.List.Extra
  open import Data.Maybe using (Maybe; just; map) renaming (nothing to ●)
  open import Data.Nat  using (ℕ)
  open import Data.Product using (_×_; Σ; _,_)
  open import Data.Unit using (⊤; tt)

  open import Relation.Binary.PropositionalEquality using (_≡_; refl; cong)
  open import Relation.Nullary
  open import Data.Empty

  open import Scheduler.Security A

  open CC.Global
  open CCˢ.Global
  open NIˢ Sch-NIˢ

  _≈ᵀ_ : (Φ Φ' : CC.TPool) → Set
  Φ ≈ᵀ Φ' = ε-TPool A Φ ≡ ε-TPool A Φ'

  _≈ᴳ_ : (c₁ c₂ : CC.Global Sch) → Set
  c₁ ≈ᴳ c₂ = εᴳ A {Sch} {Sch-NIˢ} c₁ ≡ εᴳ A {Sch} {Sch-NIˢ} c₂
  
  ≈ᴳ⇒≡ : ∀ {c₁ c₂ : CC.Global Sch}
       → c₁ ≈ᴳ c₂
       → ε-TPool A (tpool c₁) ≡ ε-TPool A (tpool c₂) × εₛ A (store c₁) ≡ εₛ A (store c₂) × εˢ (state c₁) ≡ εˢ (state c₂)
  ≈ᴳ⇒≡ {⟨ tpool₁ , store₁ , state₁ ⟩} {⟨ tpool₂ , store₂ , state₂ ⟩} eq = aux eq
    where aux : ∀ {tp₁ tp₂ : CCˢ.TPool} {st₁ st₂ : SCˢ.Store} {s₁ s₂ : State}
              →  _≡_ {A = CCˢ.Global Sch} CCˢ.⟨ tp₁ , st₁ , s₁ ⟩ CCˢ.⟨ tp₂ , st₂ , s₂ ⟩
              →  tp₁ ≡ tp₂ × st₁ ≡ st₂ × s₁ ≡ s₂
          aux refl = refl , refl , refl

  ≈-state : ∀ {c₁ c₂ : CC.Global Sch}
            → c₁ ≈ᴳ c₂
            → state c₁ ≈ˢ state c₂
  ≈-state c₁≈ᴳc₂ with ≈ᴳ⇒≡ c₁≈ᴳc₂
  ... | a , b , c = ⌜ c ⌝ˢ
 
  ≈-store : ∀ {c₁ c₂ : CC.Global Sch}
            → c₁ ≈ᴳ c₂
            → store c₁ ≈ₛ store c₂
  ≈-store c₁≈ᴳc₂ with ≈ᴳ⇒≡ c₁≈ᴳc₂
  ... | a , b , c = b

  ≈-tpool : ∀ {c₁ c₂ : CC.Global Sch}
            → c₁ ≈ᴳ c₂
            → tpool c₁ ≈ᵀ tpool c₂
  ≈-tpool c₁≈ᴳc₂ with ≈ᴳ⇒≡ c₁≈ᴳc₂
  ... | a , b , c = a

  -- Lifts annotations in the scheduler to configurations
  record _≈ᴳ⟨_,_⟩_ (c₁ : CC.Global Sch) (n₁ : ℕ) (n₂ : ℕ) (c₂ : CC.Global Sch) : Set where
    constructor ⟨_,_,_⟩
    field
      tpool-≈ : tpool c₁   ≈ᵀ tpool c₂
      store-≈ : store c₁   ≈ₛ store c₂
      state-≈ : (state c₁) ≈ˢ[ Sch-NIˢ ]⟨ n₁ , n₂ ⟩ (state c₂)
  open _≈ᴳ⟨_,_⟩_ public

  ≈ᴳ⟨⟩⇒≈ᴳ : ∀ {c₁ c₂ : CC.Global Sch} {n₁ n₂ : ℕ}
            → c₁ ≈ᴳ⟨ n₁ , n₂ ⟩ c₂
            → c₁ ≈ᴳ c₂
  ≈ᴳ⟨⟩⇒≈ᴳ ⟨ tpool-≈' , store-≈' , state-≈' ⟩ = aux tpool-≈' store-≈' ⌞ forget  state-≈' ⌟ˢ
    where aux : ∀ {tp₁ tp₂ : CCˢ.TPool} {st₁ st₂ : SCˢ.Store} {s₁ s₂ : State}
              →  tp₁ ≡ tp₂ → st₁ ≡ st₂ → s₁ ≡ s₂
              →  _≡_ {A = CCˢ.Global Sch} CCˢ.⟨ tp₁ , st₁ , s₁ ⟩ CCˢ.⟨ tp₂ , st₂ , s₂ ⟩
          aux refl refl refl = refl

  alignᴳ : ∀ {c₁ c₂ : CC.Global Sch} -> (c₁≈c₂ : c₁ ≈ᴳ c₂) ->
          let ω₁≈ω₂ = ≈-state c₁≈c₂ in
          c₁ ≈ᴳ⟨ offset₁ ω₁≈ω₂ , offset₂ ω₁≈ω₂ ⟩ c₂
  alignᴳ c₁≈ᴳc₂ = ⟨ ≈-tpool c₁≈ᴳc₂ , ≈-store c₁≈ᴳc₂ , (align (≈-state c₁≈ᴳc₂)) ⟩
