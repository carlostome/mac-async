open import Lattice using (Label)
open import Scheduler.Base
import Scheduler.Security as Schˢ
module Security.Concurrent.Progress (A : Label) (Sch : Scheduler) (Sch-NIˢ : Schˢ.NIˢ A Sch) where

  open import Calculus.Base as C using ()

  open import Security.Calculus.Base
  open import Security.Concurrent.Erasure
  open import Security.Sequential.Erasure

  open import Concurrent.Calculus as CC
  open import Security.Concurrent.Calculus as CCˢ
  open import Concurrent.Semantics
  open import Concurrent.Valid as WS
  open import Sequential.Valid as WSˢ
  open import Sequential.Progress

  open import Sequential.Calculus as SC
  open import Sequential.Semantics as SS
  open import Security.Sequential.Calculus as SCˢ
  open import Security.Sequential.LowEq A

  open import Data.List using (List; length; [])
  open import Data.List.Extra
  open import Data.Maybe using (Maybe; just; map) renaming (nothing to ●)
  open import Data.Nat  using (ℕ; suc; zero)
  open import Data.Nat.Properties using (≤-reflexive; ≤-trans)
  open import Data.Product using (_×_; Σ; _,_; ∃)
  open import Data.Unit using (⊤; tt)
  open import Data.Product.Extra
  open import Data.Bool using (Bool; true; false)
  open import Data.Sum

  open import Relation.Binary.PropositionalEquality using (_≡_; refl; sym; cong; inspect) renaming ([_] to [_]≡; subst to subst≡)
  open import Relation.Nullary
  open import Data.Empty

  open import Scheduler.Security A
  open import Security.Concurrent.LowEq A Sch Sch-NIˢ

  open CC.Global
  open CC.Thread

  private
    -- useful auxiliary lemma to discharge the case of throw
    lemma : (l : Label) (tp : CC.TPool) (P : CC.Pool l) (p : length P ≡ length (tp l))
          → ∀ (h : Label) → length (tp h) ≡ length ((tp CC.[ l ↦ P ]ᵀ) h)  
    lemma l tp P p h with Label-≡-dec l h
    ... | .true because ofʸ refl = sym p
    ... | .false because ofⁿ ¬p = refl

  -- If we know that a global configuration is valid and the scheduler
  -- schedules an existing thread then, we can reconstruct the step of
  -- that configuration, where that thread is run.  To do that, we
  -- inspect the status of the program (the memory and the scheduled
  -- thread) and choose the appropriate concurrent step.
  mkStep : ∀ {l n} (th : CC.Thread l) (c : CC.Global Sch)
           → n CC.↦ th ∈ᴾ (tpool c l)
           → WS.well-scoped (tpool c) (store c) (term th)
           → ⟼-Status (CC.mkIdˢ (tpool c)) (store c) (term th) (masked th) (exceptions th)
           → NIˢ.Next Sch-NIˢ (state c) (l , n)
           → ∃ λ c' → l , n ⊢ c ↪ c'
  mkStep th c p v (inj₁ x) next
    with next done 
  ... | ω' , s' = _ , Done ⟨ x , (masked th) , (exceptions th) ⟩ p s'
  mkStep th c p v (inj₂ (inj₁ (exs' , ∅ , Σ' , t' , s , e))) next
    with next step | Lookupℕ⇒Updateℕ p ⟨ _ , _ , exs' ⟩
  ... | ω' , s' | _ , p' = _ , (Step p p' (Lift s) s')
  mkStep th c p v (inj₂ (inj₁ (exs' , fork h l⊑h t x , Σ' , t' , s , _))) next
    with next (fork h l⊑h (CC.sizeᵀ (tpool c) h))| Lookupℕ⇒Updateℕ p ⟨ _ , _ , exs' ⟩ | fork-≡ v s
  ... | ω' , s' | _ , p' | refl , refl , v' = _ , Fork p p' (Lift s) s'
  mkStep {l} ⟨ t , m , exs ⟩ ⟨ tp , st₁ , se₁ ⟩ p v (inj₂ (inj₁ (exs' , throw e h l⊑h n , Σ' , t' , s , _))) next
    with next step | Lookupℕ⇒Updateℕ p ⟨ t' , m , exs' ⟩ | throw-≡ v s
  ... | ω' , s' | Pᴸ , p' | refl , refl , (th , pr)
    with Lookupℕ-length n ((tp CC.[ l ↦ Pᴸ ]ᵀ) h) (≤-trans (Lookupℕ⇒length pr) (≤-reflexive (lemma l tp Pᴸ (sym (Updateℕ-length p')) h)))
  ... | (thξ , pr') with Lookupℕ⇒Updateℕ pr' (CC.Th-add-ξ e l l⊑h thξ)
  ... | (Pᴴ , p'') = _ , (Throw p pr' p' p'' (Lift s) s')
  mkStep th c p v (inj₂ (inj₁ (exs' , stuck , Σ' , t' , s , _))) next
    with next stuck | stuck-≡ v s
  ... | ω' , s' | refl , refl , refl = _ , (Stuck p (Lift s) s')
  mkStep ⟨ .(C.mask _ _) , m , exs ⟩ c p _ (inj₂ (inj₂ (Mask s))) next
    with next stuck
  ... | ω' , s' = _ , (Stuck p (Lift (Mask s)) s')
  mkStep ⟨ .(C.unmask _ _) , m , exs ⟩ c p _ (inj₂ (inj₂ (UnMask s))) next
    with next stuck
  ... | ω' , s' = _ , (Stuck p (Lift (UnMask s)) s')
  mkStep ⟨ .(_ C.>>= _) , m , exs ⟩ c p _ (inj₂ (inj₂ (Bind s))) next
    with next stuck
  ... | ω' , s' = _ , (Stuck p (Lift (Bind s)) s')
  mkStep ⟨ .(C.catch _ _) , m , exs ⟩ c p _ (inj₂ (inj₂ (Catch s))) next
    with next stuck
  ... | ω' , s' = _ , (Stuck p (Lift (Catch s)) s')
  mkStep ⟨ .(C.takeMVar (C.#MVar _ _)) , m , .[] ⟩ ⟨ tpool₁ , store₁ , state₁ ⟩ p _ (inj₂ (inj₂ (LiftSₛ (TakeMVarₛ p₁)))) next
    with next stuck 
  ... | ω' , s' = _ , (Stuck p (Lift (LiftSₛ (TakeMVarₛ p₁))) s')
  mkStep ⟨ .(C.putMVar (C.#MVar _ _) _) , m , .[] ⟩ ⟨ tpool₁ , store₁ , state₁ ⟩ p _ (inj₂ (inj₂ (LiftSₛ (PutMVarₛ p₁)))) next
    with next stuck 
  ... | ω' , s' = _ , (Stuck p (Lift (LiftSₛ (PutMVarₛ p₁))) s')

  -- There are two variants of 1-step progress progressᴸ and progressᴴ.
  -- The main difference lies in the fact that when the first
  -- configuration c₁ is aligned with the second, i.e., c₁ ≈⟨ i , 0 ⟩
  -- c₂, then we know that the exact same thread is scheduled
  -- (progressᴸ).  When they are not aligned, i.e., c₁ ≈⟨ i , 1 + j ⟩
  -- c₂, then there is "some" other thread scheduled before (progressᴴ).
    
  -- 1-step progress (low-step)
  progressᴸ : ∀ {L n i} {c₁ c₂ c₁' : CC.Global Sch} {v₁ : well-scopedᴳ Sch c₁} {v₂ : well-scopedᴳ Sch c₂}
            -> L ⊑ A -> c₁ ≈ᴳ⟨ i , 0 ⟩ c₂ ->  L , n ⊢ c₁ ↪ c₁' -> ∃ λ c₂' → L , n ⊢ c₂ ↪ c₂'
  progressᴸ {L} {n} {c₁ = ⟨ ω₁ , Σ₁ , Φ₁ ⟩} {c₂ = ⟨ Φ₂ , Σ₂ , ω₂ ⟩} {v₂ = vᵀ , vₛ , vˢ} L⊑A ⟨ Φ₁≈Φ₂ , Σ₁≈Σ₂ ,  ω₁≈ω₂ ⟩ s
    with NIˢ.progressᴸ Sch-NIˢ L⊑A (getSchStep s) ω₁≈ω₂
  ... | L,n∈ˢω₂ , nextˢ with vˢ L n L,n∈ˢω₂
  ... | th , r = mkStep th ⟨ Φ₂ , Σ₂ , ω₂ ⟩ r (well-scopedᴾ-lookupℕ (vᵀ L) r) (⟼-progress (CC.mkIdˢ Φ₂) Σ₂ (term th)
                        (well-scoped→well-scoped (term th) (well-scopedᴾ-lookupℕ (vᵀ L) r)) (masked th) (exceptions th)) nextˢ

  -- 1-step progress (high-step)
  progressᴴ : ∀ {L i j n} {c₁ c₂ c₁' : CC.Global Sch} → {v₁ : well-scopedᴳ Sch c₁} {v₂ : well-scopedᴳ Sch c₂}
              → L ⊑ A -> c₁ ≈ᴳ⟨ i , suc j ⟩ c₂ -> L , n ⊢ c₁ ↪ c₁' -> ∃₃ (λ l' n' c₂' → l' , n' ⊢ c₂ ↪ c₂')
  progressᴴ {c₁ = ⟨ ω₁ , Σ₁ , Φ₁ ⟩} {c₂ = ⟨ Φ₂ , Σ₂ , ω₂ ⟩} {v₂ = vᵀ , vₛ , vˢ} L⊑A ⟨ Φ₁≈Φ₂ , Σ₁≈Σ₂ , ω₁≈ω₂ ⟩ step₁
    with NIˢ.progressᴴ Sch-NIˢ L⊑A ω₁≈ω₂ (getSchStep step₁)
  ... | (H , m) , Ln∈ω₁ , nextˢ with vˢ H  m Ln∈ω₁
  ... | th , r = H , m , mkStep th ⟨ Φ₂ , Σ₂ , ω₂ ⟩ r (well-scopedᴾ-lookupℕ (vᵀ H) r) (⟼-progress (CC.mkIdˢ Φ₂) Σ₂ (term th)
                                ((well-scoped→well-scoped (term th) (well-scopedᴾ-lookupℕ (vᵀ H) r))) (masked th) (exceptions th)) nextˢ
