open import Lattice using (Label)
open import Scheduler.Base
open import Scheduler.Security
import Concurrent.Valid as V
module Security.PSNI (A : Label) (Sch : Scheduler) (Sch-V : V.Well-scopedˢ Sch) (Sch-NIˢ : NIˢ A Sch) where

  open import Lattice
  open import Concurrent.Calculus          as CC
  open import Security.Concurrent.Erasure 
  open import Concurrent.Semantics         as CS
  open import Security.Concurrent.Deterministic  as CDˢ
  open import Security.Concurrent.Calculus as CCˢ
  open import Security.Concurrent.Progress
  open import Concurrent.Valid
  
  open import Security.Simulation.Concurrent
  open import Relation.Binary.PropositionalEquality
  open import Relation.Nullary
  open import Data.Product
  open import Data.Bool

  open import Data.Nat
  open import Security.Concurrent.LowEq A Sch Sch-NIˢ

  open Scheduler Sch
  open NIˢ Sch-NIˢ hiding (progressᴸ; progressᴴ)
  open V.Well-scoped↪ Sch Sch-V

  -- Low-equivalence preservation (low steps).  If two low-equivalent
  -- configuration step with the same low-thread, then the resulting
  -- configurations are low-equivalent.
  squareᴸ : ∀ (l : Label) {n} {c₁ c₁' c₂ c₂' : CC.Global Sch}
            → (l ⊑ A) -> εᴳ A {Sch} {Sch-NIˢ} c₁ ≡ εᴳ A {Sch} {Sch-NIˢ} c₂
            → l , n ⊢ c₁ ↪ c₁' → l , n  ⊢ c₂ ↪ c₂' → εᴳ A {Sch} {Sch-NIˢ} c₁' ≡ εᴳ A {Sch} {Sch-NIˢ} c₂'
  squareᴸ l l⊑A c₁≈c₂ step₁ step₂
    with ε-↪-simᴸ A {Sch-NIˢ = Sch-NIˢ} l l⊑A step₁ | ε-↪-simᴸ A {Sch-NIˢ = Sch-NIˢ} l l⊑A step₂
  ... | ε-step₁ | ε-step₂ with ≈ᴳ⇒≡ c₁≈c₂
  ... | eq₁ , eq₂ , eq₃ rewrite eq₁ | eq₂ | eq₃
    = CDˢ.↪-determinism Sch ε-step₁ ε-step₂

  -- Low-equivalence preservation (high step).
  -- If c₂ is 1 + j step behind c₁, then a high thread is scheduled
  -- first in (H, m) ⊢ c₂ ↪ c₂' By using the scheduler requirements
  -- (no-starvation and no-observable effect), we prove annotated
  -- low-equivalence preservation, i.e., c₁ ≈ c₂' and c₂' is j step
  -- behind c₁.
  triangleᴴ : ∀ {L H i j n m} {c₁ c₁' c₂ c₂' : CC.Global Sch} →
              L ⊑ A -> c₁ ≈ᴳ⟨ i , suc j ⟩ c₂ ->  L , n  ⊢ c₁ ↪ c₁' ->  H , m  ⊢ c₂ ↪ c₂' -> c₁ ≈ᴳ⟨ i , j ⟩ c₂'
  triangleᴴ {H = H} L⊑A ⟨ Φ₁≈Φ₂ , Σ₁≈Σ₂ , ω₁≈ω₂ ⟩ step₁ step₂ with NIˢ.triangleˢ Sch-NIˢ L⊑A ω₁≈ω₂ (CS.getSchStep step₁) (CS.getSchStep step₂)
  ... | H⋤A , ω₁≈ω₂' with ≈ᴳ⇒≡ (ε-↪-simᴴ A {Sch} {Sch-NIˢ} H H⋤A step₂)
  ... | Φ₂≈Φ₂' ,  Σ₂≈Σ₂' , ω₂≈ω₂' = ⟨ trans Φ₁≈Φ₂ Φ₂≈Φ₂' , trans Σ₁≈Σ₂ Σ₂≈Σ₂' , ω₁≈ω₂' ⟩

  -- If configuration c₁ runs with a low thread ((l , n) ⊢ c₁ ↪ c₁') and
  -- c₂ is low-equivalent and n₂ steps behind (i.e., c₁ ≈ᴳ⟨ n₁ , n₂ ⟩
  -- c₂), then in at most n₂ steps c₂ catches up (c₂ ↪⋆ c₂') to a
  -- low-equivalent configuration c₁' ≈ c₂'.
  εᴳ-simᴸ⋆ : ∀ (L : Label) {n n₁} {c₁ c₁' c₂ : CC.Global Sch} → (n₂ : ℕ) → {v₁ : well-scopedᴳ Sch c₁} {v₂ : well-scopedᴳ Sch c₂} →
              L ⊑ A -> L , n  ⊢ c₁ ↪ c₁' -> c₁ ≈ᴳ⟨ n₁ , n₂ ⟩ c₂ -> ∃ (λ c₂' → c₁' ≈ᴳ c₂' × c₂ ↪⋆ c₂')
  εᴳ-simᴸ⋆ L zero {v₁} {v₂} L⊑A step₁ c₁≈c₂ with progressᴸ A Sch Sch-NIˢ {v₁ = v₁} {v₂ = v₂} L⊑A  c₁≈c₂ step₁
  εᴳ-simᴸ⋆ L zero L⊑A step₁ c₁≈c₂ | c₂' , step₂ = c₂' , squareᴸ L L⊑A (≈ᴳ⟨⟩⇒≈ᴳ c₁≈c₂) step₁ step₂ , (step₂ ∷ [])
  εᴳ-simᴸ⋆ L (suc n₂) {v₁ = v₁} {v₂ = v₂} L⊑A step₁ c₁≈c₂ with progressᴴ A Sch Sch-NIˢ {v₁ = v₁} {v₂ = v₂} L⊑A c₁≈c₂ step₁
  ... | H , m , c₂' , step₂ with triangleᴴ L⊑A c₁≈c₂ step₁ step₂
  ... | c₁≈c₂' with εᴳ-simᴸ⋆ _ n₂ {v₁} {well-scoped↪ step₂ v₂} L⊑A step₁ c₁≈c₂'
  ... | c₂'' , c₁≈c₂'' , ss = c₂'' , c₁≈c₂'' , (step₂ ∷ ss)

  -- The actual proof depends on whether we are matching a low or a high step.
  -- This helper lemma choose the correct one.
  εᴳ-sim⋆ : ∀ (l : Label) {n} {c₁ c₁' c₂ : CC.Global Sch} → {v₁ : well-scopedᴳ Sch c₁} {v₂ : well-scopedᴳ Sch c₂}
            → l , n ⊢ c₁ ↪ c₁' → c₁ ≈ᴳ c₂ → ∃ (λ c₂' → c₁' ≈ᴳ c₂' × c₂ ↪⋆ c₂')
  εᴳ-sim⋆ l {c₁ = c₁} {c₁' = c₁'} {c₂ = c₂} {v₁} {v₂} s c₁≈c₂ with Label-⊑-dec l A
  ... | .true because ofʸ l⊑A  = εᴳ-simᴸ⋆ _ _ {v₁} {v₂} l⊑A s (alignᴳ c₁≈c₂)
  ... | .false because ofⁿ l⋤A = c₂ , trans (sym (ε-↪-simᴴ A {Sch-NIˢ = Sch-NIˢ} l l⋤A s)) c₁≈c₂ , []

  -- Progress-sensitive noninterference (PSNI)
  psni : ∀ (l : Label) {n} {c₁ c₁' c₂ : CC.Global Sch} → {v₁ : well-scopedᴳ Sch c₁} {v₂ : well-scopedᴳ Sch c₂}
         →  l , n ⊢ c₁ ↪ c₁' → c₁ ≈ᴳ c₂ → (∃ λ c₂' → c₂ ↪⋆ c₂' × c₁' ≈ᴳ c₂')
  psni l {v₁ = v₁} {v₂ = v₂} s eq with εᴳ-sim⋆ l {v₁ = v₁} {v₂ = v₂} s eq
  ... | c₂' , c₁'≈ᴳc₂' ,  c₂↪⋆c₂' = c₂' , c₂↪⋆c₂' , c₁'≈ᴳc₂'
