module Security.Sequential.Calculus where

  open import Security.Calculus.Base
  open import Calculus.Base as C using ()

  open import Data.Bool       using (Bool; true; false; not)
  open import Data.List       using (List; _∷_; []; _++_; [_]; length)
  open import Data.List.Extra
  open import Data.Maybe      using (Maybe; just; nothing; maybe′)
  open import Data.Maybe renaming (nothing to ●)
  open import Data.Nat        using (ℕ)
  open import Data.Product    using (∃; _,_; _×_; Σ) renaming (proj₁ to fst)
  open import Data.Unit       using (⊤; tt)
  open import Data.Empty      using (⊥)
  open import Function        using (_∘_)

  open import Relation.Nullary 
  open import Relation.Binary.PropositionalEquality using (refl; _≡_; cong)

  -------------------------------------------------------------------------------- 
  --                               Exceptions
  --------------------------------------------------------------------------------

  -- a mask is a bitvector, with true at an exception
  -- when is masked.
  ξ-Mask : Set
  ξ-Mask = ξ → Bool

  ξ-mask : (ξ → Bool) → ξ → (ξ → Bool)
  ξ-mask M e₁ e₂ with ξ-≡-dec e₁ e₂
  ... | yes p = true
  ... | no ¬p = M e₂

  ξ-unmask : (ξ → Bool) → ξ → (ξ → Bool)
  ξ-unmask M e₁ e₂ with ξ-≡-dec e₁ e₂
  ... | yes p = false
  ... | no ¬p = M e₂

  -- a labeled list of labeled exceptions (all flow to l)
  ξ-List : Label → Set
  ξ-List l = Maybe (List (Maybe (ξ × Σ Label (_⊑ l))))

  -- add a new exception to the list
  ξ-L-add : ∀ {l : Label} → ξ-List l → ξ → (l' : Label) → (l'⊑l : l' ⊑ l) → ξ-List l
  ξ-L-add (just exs) e l' l'⊑l = just (exs ++ [ just (e , l' , l'⊑l) ])
  ξ-L-add ● e l' l'⊑l          = ●

  -- get the first unmasked exception
  ξ-L-get-unmasked : ∀ {l : Label} → ξ-List l → ξ-Mask → Maybe ((ξ × Σ Label (_⊑ l)) × ξ-List l)
  ξ-L-get-unmasked ● M      = ●
  ξ-L-get-unmasked (just exs) M 
    with first (maybe′ (not ∘ M ∘ fst) false) exs
  ξ-L-get-unmasked (just exs) M | ●                     = ●
  ξ-L-get-unmasked (just exs) M | just (● , _)          = ●
  ξ-L-get-unmasked (just exs) M | just (just ex , exs') = just (ex , just exs')

  -- get the first exception
  ξ-L-get : ∀ {l : Label} → ξ-List l → Maybe ((ξ × Σ Label (_⊑ l)) × ξ-List l)
  ξ-L-get ●         = ●
  ξ-L-get (just []) = nothing
  ξ-L-get (just (● ∷ xs))      = ●
  ξ-L-get (just (just x ∷ xs)) = just (x , just xs)

  -------------------------------------------------------------------------------- 
  --                        Concurrent  Events
  --------------------------------------------------------------------------------

  -- types concurrent events
  data CEvent (l : Label) : Set where
    ∅       : CEvent l
    fork    : (h : Label) → (l⊑h : l ⊑ h) (t : CTerm (MAC h （）)) → ξ-Mask → CEvent l
    throw   : (e : ξ) (h : Label) → (l⊑h : l ⊑ h) → ℕ → CEvent l
    stuck   : CEvent l

    ●        : CEvent l                                                       -- erased event
    fork●    : (h : Label) → (l⊑h : l ⊑ h) → CEvent l
    throw●   : (e : ξ) (h : Label) → (l⊑h : l ⊑ h) → CEvent l

  -------------------------------------------------------------------------------- 
  --                               Memory
  --------------------------------------------------------------------------------


  -- cells in memory, either empty or contain a term
  data Cell (τ : Ty) : Set where
    ⊗   : Cell τ
    ⟦_⟧ : CTerm τ -> Cell τ

  Mem : Set
  Mem = Maybe (List (∃ Cell))

  -- a store is a label-partitioned memory of cells (possibly erased)
  Store : Set
  Store = (l : Label) → Mem

  newˢ : Store → Label → Ty → Store
  newˢ Σ l τ l' with Label-≡-dec l l'
  newˢ Σ l τ l' | .true because ofʸ p
      with Σ l
  newˢ Σ l τ l' | .true because ofʸ p | ●      = ●
  newˢ Σ l τ l' | .true because ofʸ p | just s = just (s ++ [ τ , ⊗ ])
  newˢ Σ l τ l' | .false because ofⁿ ¬p  = Σ l'

  _[_↦_]ˢ : Store → (l : Label) → Maybe (List (∃ Cell)) → Store
  _[_↦_]ˢ Σ l cell l' with Label-≡-dec l l'
  ... | yes refl = cell
  ... | no ¬p    = Σ l'

  -- Sytactic sugar for Lookupℕ in a Mem
  _↦_∈ˢ_ : ℕ -> ∃ Cell -> Mem -> Set
  n ↦ t ∈ˢ ●      = ⊥
  n ↦ t ∈ˢ just P = Lookupℕ t n P

  -- Syntatic sugar for updating a Mem.
  _≔_[_↦_]ˢ : Mem -> Mem -> ℕ -> ∃ Cell -> Set
  P' ≔ ● [ n ↦ t ]ˢ           = ⊥
  ● ≔ just P [ n ↦ t ]ˢ       = ⊥
  just P' ≔ just P [ n ↦ t ]ˢ = Updateℕ t n P P'

  sizeˢ : Store → (l : Label) → ℕ
  sizeˢ Σ l with Σ l
  sizeˢ Σ l | ●      = 0
  sizeˢ Σ l | just s = length s

  -- source of identifies
  Idˢ : Set
  Idˢ = Label → Maybe ℕ

  -------------------------------------------------------------------------------- 
  --                        Sync. Variable  Events
  --------------------------------------------------------------------------------

  data SEvent (l : Label) : Set where
    stuck : SEvent l
    step  : SEvent l
    ●     : SEvent l


  -------------------------------------------------------------------------------- 
  -- properties

  ↦∈ˢ-inj : ∀ {P : Mem} {n} {c₁ c₂ : ∃ Cell}
            → n ↦ c₁ ∈ˢ P
            → n ↦ c₂ ∈ˢ P
            → c₁ ≡ c₂
  ↦∈ˢ-inj {just P} p₁ p₂ = Lookupℕ-inj p₁ p₂
  
  ≔[]ˢ-inj : ∀ {P P₁ P₂ : Mem} {n} {c : ∃ Cell}
            → P₁ ≔ P [ n ↦ c ]ˢ
            → P₂ ≔ P [ n ↦ c ]ˢ
            → P₁ ≡ P₂
  ≔[]ˢ-inj {just P} {just _} {just _} p₁ p₂ = cong just (Updateℕ-inj p₁ p₂)
