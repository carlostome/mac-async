module Security.Sequential.Erasure where

  open import Lattice
  
  open import Calculus.Base                as C
  open import Sequential.Calculus          as SC

  open import Security.Calculus.Base       as Cˢ
  open import Security.Sequential.Calculus as SCˢ renaming (_[_↦_]ˢ to _[_↦_]ˢˢ)
  open import Security.Calculus.Erasure

  open import Data.Bool               using (Bool; true; false; not)
  open import Data.List               using (List; _∷_; []; map; length)
  open import Data.List.Extra 
  open import Data.List.Properties    using (length-map; map-++-commute)
  open import Data.Maybe              using (Maybe; just; nothing; maybe′)
  open import Data.Maybe              using ()                             renaming (nothing to ●)
  open import Data.Nat                using (ℕ)
  open import Data.Product            using (map₂; _,_; Σ; _×_; ∃)         renaming (proj₁ to fst)
  open import Data.Empty              using (⊥-elim)
  open import Function                using (_∘_)
  open import Function.Extensionality using (fun-ext)

  open import Relation.Nullary 
  open import Relation.Binary.PropositionalEquality using (refl; _≡_; inspect; [_]; trans; sym; cong)

  -------------------------------------------------------------------------------- 
  --                        Concurrent  Events
  --------------------------------------------------------------------------------

  εₜ-CEvent : (A : Label) → ∀ {l} → SC.CEvent l → SCˢ.CEvent l
  εₜ-CEvent A {l} ∅              = ∅
  εₜ-CEvent A {l} stuck          = stuck
  εₜ-CEvent A {l} (fork h l⊑h t M)
    with Label-⊑-dec h A
  ... | .true because ofʸ p   = fork h l⊑h (ε A t) M
  ... | .false because ofⁿ ¬p = fork● h l⊑h
  εₜ-CEvent A {l} (throw e h l⊑h m)
    with Label-⊑-dec h A
  ... | .true because ofʸ p   = throw e h l⊑h m
  ... | .false because ofⁿ ¬p = throw● e h l⊑h

  εᵗ-CEvent : (A : Label) → ∀ {l'} → SC.CEvent l' → SCˢ.CEvent l'
  εᵗ-CEvent A {l} ev with Label-⊑-dec l A
  εᵗ-CEvent A {l} ev | .true because ofʸ p   = εₜ-CEvent A ev
  εᵗ-CEvent A {l} ev | .false because ofⁿ ¬p = ●

  -------------------------------------------------------------------------------- 
  --                               Memory
  --------------------------------------------------------------------------------

  -- erase a cell
  εᶜ : (l : Label) → ∀ {τ} → SC.Cell τ → SCˢ.Cell τ
  εᶜ l ⊗     = ⊗
  εᶜ l ⟦ t ⟧ = ⟦ ε l t ⟧

  εₘ : (A : Label) → SC.Mem → SCˢ.Mem
  εₘ A m = just (map (map₂ (εᶜ A)) m)

  -- erase a store
  εₛ : (A : Label) → SC.Store → SCˢ.Store
  εₛ A Σ l
    with Label-⊑-dec l A
  ... | .true because ofʸ p   = εₘ A (Σ l)
  ... | .false because ofⁿ ¬p = ●

  ε-Idˢ : (A : Label) → SC.Idˢ → SCˢ.Idˢ
  ε-Idˢ A ϕ l with Label-⊑-dec l A
  ... | .true because ofʸ p   = just (ϕ l)
  ... | .false because ofⁿ ¬p = ●

  -------------------------------------------------------------------------------- 
  --                        Sync. Variable  Events
  --------------------------------------------------------------------------------

  εₜ-SEvent : (l : Label) → ∀ {l} → SC.SEvent l → SCˢ.SEvent l
  εₜ-SEvent l stuck = stuck
  εₜ-SEvent l step  = step

  εᵗ-SEvent : (l : Label) → ∀ {l} → SC.SEvent l → SCˢ.SEvent l
  εᵗ-SEvent l {l'} ev with Label-⊑-dec l' l
  εᵗ-SEvent l {l'} ev | .true because ofʸ p   = εₜ-SEvent l ev
  εᵗ-SEvent l {l'} ev | .false because ofⁿ ¬p = ●

  -------------------------------------------------------------------------------- 
  --                               Exceptions
  --------------------------------------------------------------------------------

  ε-ξ : (l : Label) → {l' : Label} →(ξ × Σ Label (_⊑ l')) → Maybe (ξ × Σ Label (_⊑ l'))
  ε-ξ l (ex , l'' , l''⊑l') with Label-⊑-dec l'' l
  ε-ξ l (ex , l'' , l''⊑l') | .true because ofʸ p   = just (ex , l'' , l''⊑l')
  ε-ξ l (ex , l'' , l''⊑l') | .false because ofⁿ ¬p = ●

  εₜ-ξ-List : (A : Label) (l : Label) → SC.ξ-List l → SCˢ.ξ-List l 
  εₜ-ξ-List A l exs = just (map (ε-ξ A) exs)

  εᵗ-ξ-List : (l : Label) → {l' : Label} → SC.ξ-List l' → SCˢ.ξ-List l'
  εᵗ-ξ-List l {l'} exs with Label-⊑-dec l' l
  εᵗ-ξ-List l {l'} exs | .true because ofʸ p   = εₜ-ξ-List l l' exs
  εᵗ-ξ-List l {l'} exs | .false because ofⁿ ¬p = ●

  -------------------------------------------------------------------------------- 
  --                               Lemmas
  --------------------------------------------------------------------------------

  private
    newˢ-com-aux : ∀ {Σ l τ} → ∀ A → ∀ l' → εₛ A (SC.newˢ Σ l τ) l' ≡ SCˢ.newˢ (εₛ A Σ) l τ l'
    newˢ-com-aux A l'
      with Label-⊑-dec l' A
    newˢ-com-aux {l = l} A l' | .true because ofʸ p
      with Label-≡-dec l l'
    newˢ-com-aux {Σ = Σ} {l = l} A l' | .true because ofʸ p | .true because ofʸ p'
      with Label-⊑-dec l A
    ... | .true because ofʸ p'' = cong just (map-++-commute _ (Σ l) _)
    newˢ-com-aux {Σ} {.l'} A l' | .true because ofʸ p | .true because ofʸ refl | .false because ofⁿ ¬p = ⊥-elim (¬p p)
    newˢ-com-aux {l = l} A l' | .true because ofʸ p | .false because ofⁿ ¬p
      with Label-⊑-dec l' A
    ... | .true because ofʸ p'   = refl
    ... | .false because ofⁿ ¬p' = ⊥-elim (¬p' p)
    newˢ-com-aux {l = l} A l' | .false because ofⁿ ¬p
      with Label-≡-dec l l'
    newˢ-com-aux {l = l} A l' | .false because ofⁿ ¬p | .true because ofʸ p
      with Label-⊑-dec l A
    newˢ-com-aux {l = .l'} A l' | .false because ofⁿ ¬p | .true because ofʸ refl | .true because ofʸ p' = ⊥-elim (¬p p')
    ... | .false because ofⁿ ¬p' = refl
    newˢ-com-aux {l = l} A l' | .false because ofⁿ ¬p | .false because ofⁿ ¬p'
      with Label-⊑-dec l' A
    ... | .true because ofʸ p     = ⊥-elim (¬p p)
    ... | .false because ofⁿ ¬p'' = refl

  newˢ-com : ∀ (A : Label) (l : Label) →
               (Σ : SC.Store) {τ : Ty}
             → εₛ A (SC.newˢ Σ l τ) ≡ SCˢ.newˢ (εₛ A Σ) l τ
  newˢ-com A l Σ = fun-ext (newˢ-com-aux A)

  sizeˢ-comᴸ : ∀ (A : Label) (l : Label) → (l⊑A : l ⊑ A)
                 (Σ : SC.Store)
               → SC.sizeˢ Σ l ≡ SCˢ.sizeˢ (εₛ A Σ) l
  sizeˢ-comᴸ A  l l⊑A Σ
    with Label-⊑-dec l A
  ... | .true because ofʸ p   = sym (length-map _ (Σ l))
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)

  private
    sizeˢ-comᴴ-aux : ∀ {Σ l τ} → ∀ A → (l⋤A : ¬ l ⊑ A) → ∀ l' → εₛ A (SC.newˢ Σ l τ) l' ≡ εₛ A Σ l'
    sizeˢ-comᴴ-aux {l = l} A l⋤=A l' with Label-⊑-dec l' A
    sizeˢ-comᴴ-aux {l = l} A l⋤=A l' | .true because ofʸ p
      with Label-≡-dec l l'
    sizeˢ-comᴴ-aux {l = .l'} A l⋤=A l' | .true because ofʸ p | .true because ofʸ refl = ⊥-elim (l⋤=A p)
    sizeˢ-comᴴ-aux {l = l} A l⋤=A l' | .true because ofʸ p | .false because ofⁿ ¬p    = refl
    sizeˢ-comᴴ-aux {l = l} A l⋤=A l' | .false because ofⁿ ¬p = refl

  sizeˢ-comᴴ : ∀ (A : Label) (l : Label) → (l⋢A : ¬ l ⊑ A)
                 (Σ : SC.Store) {τ : Ty}
               → εₛ A (SC.newˢ Σ l τ) ≡ εₛ A Σ
  sizeˢ-comᴴ A l l⋤A Σ = fun-ext (sizeˢ-comᴴ-aux A l⋤A)

  private
    ξ-mask-com-aux : ∀ {M : ξ → Bool} → ∀ e → ∀ e' → SC.ξ-mask M e e' ≡ SCˢ.ξ-mask M e e'
    ξ-mask-com-aux e e' with ξ-≡-dec e e'
    ξ-mask-com-aux e e' | .true because ofʸ p   = refl
    ξ-mask-com-aux e e' | .false because ofⁿ ¬p = refl

  ξ-mask-com : ∀ {M : ξ → Bool} → ∀ e → SC.ξ-mask M e ≡ SCˢ.ξ-mask M e
  ξ-mask-com e = fun-ext (ξ-mask-com-aux e)

  private
    ξ-unmask-com-aux : ∀ {M : ξ → Bool} → ∀ e → ∀ e' → SC.ξ-unmask M e e' ≡ SCˢ.ξ-unmask M e e'
    ξ-unmask-com-aux e e' with ξ-≡-dec e e'
    ξ-unmask-com-aux e e' | .true because ofʸ p   = refl
    ξ-unmask-com-aux e e' | .false because ofⁿ ¬p = refl

  ξ-unmask-com : ∀ {M : ξ → Bool} → ∀ e → SC.ξ-unmask M e ≡ SCˢ.ξ-unmask M e
  ξ-unmask-com e = fun-ext (ξ-unmask-com-aux e)
  
  first-≡-nothing : ∀ (A : Label) (l : Label)
                    → (e : ξ) {τ : Ty} (hs : List (ξ × C.CTerm (MAC l τ)))
                    → first (does ∘ (ξ-≡-dec e) ∘ fst) hs ≡ nothing
                    → first (does ∘ (ξ-≡-dec e) ∘ fst) (ε-list A hs) ≡ nothing
  first-≡-nothing A l e [] refl = refl
  first-≡-nothing A l e ((e' , t) ∷ hs) p
    with Label-⊑-dec l A
  first-≡-nothing A l e ((e' , t) ∷ hs) p | .true because ofʸ p'
    with ξ-≡-dec e e'
  ... | .false because ofⁿ ¬p
    with first (does ∘ (ξ-≡-dec e) ∘ fst) hs | inspect (first (does ∘ (ξ-≡-dec e) ∘ fst)) hs
       | first (does ∘ (ξ-≡-dec e) ∘ fst) (ε-list A hs) | inspect (first (does ∘ (ξ-≡-dec e) ∘ fst)) (ε-list A hs)
  ... | ● | [ eq ] | ● | [ eq' ]  = refl
  ... | ● | [ eq ] | just x | [ eq' ]
    with trans (sym eq') (first-≡-nothing A l e hs eq)
  ... | ()
  first-≡-nothing A l e ((e' , t) ∷ hs) p | .false because ofⁿ ¬p
    with ξ-≡-dec e e'
  ... | .false because ofⁿ ¬p'
    with first (does ∘ (ξ-≡-dec e) ∘ fst) hs | inspect (first (does ∘ (ξ-≡-dec e) ∘ fst)) hs
       | first (does ∘ (ξ-≡-dec e) ∘ fst) (ε-list A hs) | inspect (first (does ∘ (ξ-≡-dec e) ∘ fst)) (ε-list A hs)
  ... | ● | [ eq ] | ● | [ eq' ] = refl
  ... | ● | [ eq ] | just _ | [ eq' ]
    with trans (sym eq') (first-≡-nothing A l e hs eq)
  ... | ()

  first-≡-just : ∀ (A : Label) (l : Label)
                   (e : ξ) {τ} (hs : List (ξ × C.CTerm (MAC l τ)))
                   {e' : ξ} {t : C.CTerm (MAC l τ)} {hs' : List (ξ × C.CTerm (MAC l τ))}
                 → first (does ∘ (ξ-≡-dec e) ∘ fst) hs ≡ just ((e' , t) , hs')
                 → first (does ∘ (ξ-≡-dec e) ∘ fst) (ε-list A hs) ≡ just ((e' , ε A t) , ε-list A hs')
  first-≡-just A l e [] ()
  first-≡-just A l e ((e' , t) ∷ hs) p
    with Label-⊑-dec l A
  first-≡-just A l e ((e' , t) ∷ hs) p | .true because ofʸ p'
    with ξ-≡-dec e e'   
  first-≡-just A l e ((e' , t) ∷ hs) refl | .true because ofʸ p' | .true because ofʸ p₁ = refl
  ... | .false because ofⁿ ¬p
    with first (does ∘ (ξ-≡-dec e) ∘ fst) hs | inspect (first (does ∘ (ξ-≡-dec e) ∘ fst)) hs
       | first (does ∘ (ξ-≡-dec e) ∘ fst) (ε-list A hs) | inspect (first (does ∘ (ξ-≡-dec e) ∘ fst)) (ε-list A hs)
  ... | just x | [ eq ] | ● | [ eq' ]
    with trans (sym eq') (first-≡-just A l e hs eq)
  ... | ()
  first-≡-just A l e ((e' , t) ∷ hs) refl | .true because ofʸ p' | .false because ofⁿ ¬p | just _ | [ eq ] | just (x , hs') | [ eq' ]
    with trans (sym eq') (first-≡-just A l e hs eq)
  ... | refl with Label-⊑-dec l A
  ... | .true because ofʸ p   = refl
  ... | .false because ofⁿ ¬p₁ = ⊥-elim (¬p₁ p')
  first-≡-just A l e ((e' , t) ∷ hs) p | .false because ofⁿ ¬p
    with ξ-≡-dec e e'
  first-≡-just A l e ((e' , t) ∷ hs) refl | .false because ofⁿ ¬p | .true because ofʸ p₁ = refl
  ... | .false because ofⁿ ¬p₁
    with first (does ∘ (ξ-≡-dec e) ∘ fst) hs | inspect (first (does ∘ (ξ-≡-dec e) ∘ fst)) hs
       | first (does ∘ (ξ-≡-dec e) ∘ fst) (ε-list A hs) | inspect (first (does ∘ (ξ-≡-dec e) ∘ fst)) (ε-list A hs)
  ... | just (.(_ , _) , snd) | [ eq ] | ● | [ eq' ]
    with trans (sym eq') (first-≡-just A l e hs eq)
  ... | ()
  first-≡-just A l e ((e' , t) ∷ hs) refl | .false because ofⁿ ¬p | .false because ofⁿ ¬p₁ | just (.(_ , _) , snd) | [ eq ] | just x₁ | [ eq' ]
    with trans (sym eq') (first-≡-just A l e hs eq)
  ... | refl with Label-⊑-dec l A
  ... | .true because ofʸ p = ⊥-elim (¬p p)
  ... | .false because ofⁿ ¬p₂ = refl

  ε-first-M-nothingᴸ : ∀ (A l : Label) → (l⊑A : l ⊑ A)
                       → (exs : SC.ξ-List l) (M : SC.ξ-Mask)
                       → first (not ∘ M ∘ fst) exs ≡ nothing
                       → first (maybe′ (not ∘ M ∘ fst) false) (map (ε-ξ A) exs) ≡ nothing
  ε-first-M-nothingᴸ A l l⊑A [] M p = refl
  ε-first-M-nothingᴸ A l l⊑A ((ex , (l' , l'⊑l)) ∷ exs) M p
    with Label-⊑-dec l' A
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p (⊑-trans l'⊑l l⊑A))
  ... | .true  because ofʸ p' with not (M ex)
  ε-first-M-nothingᴸ A l l⊑A ((ex , l' , l'⊑l) ∷ exs) M () | .true because ofʸ p' | true
  ... | false with first (not ∘ M ∘ fst) exs
                 | inspect (first (not ∘ M ∘ fst)) exs
                 | first (maybe′ (not ∘ M ∘ fst) false) (map (ε-ξ A) exs)
                 | inspect (first (maybe′ (not ∘ M ∘ fst) false)) (map (ε-ξ A) exs)
  ε-first-M-nothingᴸ A l l⊑A ((ex , l' , l'⊑l) ∷ exs) M p | .true because ofʸ p' | false | ● | [ eq ] | ● | [ eq' ] = refl
  ε-first-M-nothingᴸ A l l⊑A ((ex , l' , l'⊑l) ∷ exs) M p | .true because ofʸ p' | false | ● | [ eq ] | just x | [ eq' ]
    with trans (sym eq') (ε-first-M-nothingᴸ A l l⊑A exs M eq)
  ... | ()

  ε-first-M-justᴸ : ∀ (A l : Label) → (l⊑A : l ⊑ A)
                    → (exs : SC.ξ-List l) (M : SC.ξ-Mask) {ex : ξ} {l'×l'⊑l : ∃ (_⊑ l)} {exs' : SC.ξ-List l}
                    → first (not ∘ M ∘ fst) exs ≡ just ((ex , l'×l'⊑l) , exs')
                    → first (maybe′ (not ∘ M ∘ fst) false) (map (ε-ξ A) exs) ≡ just (just (ex , l'×l'⊑l) , map (ε-ξ A) exs')
  ε-first-M-justᴸ A l l⊑A [] M ()
  ε-first-M-justᴸ A l l⊑A ((ex , (l' , l'⊑l)) ∷ exs) M p
    with Label-⊑-dec l' A
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p (⊑-trans l'⊑l l⊑A))
  ... | .true  because ofʸ p' with not (M ex)
  ε-first-M-justᴸ A l l⊑A ((ex , l' , l'⊑l) ∷ exs) M refl | .true because ofʸ p' | true = refl
  ... | false with first (not ∘ M ∘ fst) exs
                 | inspect (first (not ∘ M ∘ fst)) exs
                 | first (maybe′ (not ∘ M ∘ fst) false) (map (ε-ξ A) exs)
                 | inspect (first (maybe′ (not ∘ M ∘ fst) false)) (map (ε-ξ A) exs)
  ... | just x | [ eq ] | ● | [ eq' ]
    with trans (sym eq') (ε-first-M-justᴸ A l l⊑A exs M eq)
  ... | ()
  ε-first-M-justᴸ A l l⊑A ((ex , l' , l'⊑l) ∷ exs) M refl | .true because ofʸ p' | false | just (.(_ , _) , snd) | [ eq ] | just (_ , _) | [ eq' ]
    with trans (sym eq') (ε-first-M-justᴸ A l l⊑A exs M eq)
  ... | refl with Label-⊑-dec l' A
  ... | .false because ofⁿ ¬p  = ⊥-elim (¬p (⊑-trans l'⊑l l⊑A))
  ... | .true  because ofʸ p'' = refl

  private
    εˢ-low-update-aux : (A l : Label) → (l⊑A : l ⊑ A)
                    → (Σ : SC.Store) (m : SC.Mem)
                    → ∀ l'
                    → εₛ A (Σ [ l ↦ m ]ˢ) l' ≡ ((εₛ A Σ) SCˢ.[ l ↦ εₘ A m ]ˢ) l'
    εˢ-low-update-aux A l l⊑A Σ m l' with Label-≡-dec l l'
    εˢ-low-update-aux A l l⊑A Σ m .l | .true because ofʸ refl
        with Label-⊑-dec l A
    ... | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
    ... | .true because ofʸ p
        with Label-≡-dec l l
    ... | .false because ofⁿ ¬p' = ⊥-elim (¬p' refl)
    ... | .true because ofʸ refl = refl
    εˢ-low-update-aux A l l⊑A Σ m l' | .false because ofⁿ ¬p
        with Label-⊑-dec l A
    ... | .false because ofⁿ ¬p' = ⊥-elim (¬p' l⊑A)
    ... | .true because ofʸ p
        with Label-⊑-dec l' A
    ... | .false because ofⁿ ¬p' = refl
    ... | .true because ofʸ p'   
        with Label-≡-dec l l
    ... | .false because ofⁿ ¬p' = ⊥-elim (¬p' refl)
    ... | .true because ofʸ refl
        with Label-≡-dec l l'
    ... | .false because ofⁿ ¬p' = refl
    ... | .true because ofʸ refl = ⊥-elim (¬p refl)

  εˢ-updateᴸ : ∀ (A l : Label) → (l⊑A : l ⊑ A)
                 (Σ : SC.Store) (m : SC.Mem)
               → εₛ A (Σ [ l ↦ m ]ˢ)  ≡ (εₛ A Σ) SCˢ.[ l ↦ εₘ A m ]ˢ
  εˢ-updateᴸ A l l⊑A Σ m = fun-ext (εˢ-low-update-aux A l l⊑A Σ m)
  ξ-L-get-unmasked-justᴸ : ∀ (A : Label) (l : Label) → (l⊑A : l ⊑ A)
                           → (exs : SC.ξ-List l) (M : SC.ξ-Mask) {ex : ξ} {l'×l'⊑l : ∃ (_⊑ l)} {exs' : SC.ξ-List l}
                           → SC.ξ-L-get-unmasked exs  M                      ≡ just ((ex , l'×l'⊑l) , exs')
                           → SCˢ.ξ-L-get-unmasked (just (map (ε-ξ A) exs)) M ≡ just ((ex , l'×l'⊑l) , just (map (ε-ξ A) exs'))
  ξ-L-get-unmasked-justᴸ A l l⊑A ((ex , (l' , l'⊑l)) ∷ exs) M p
    with Label-⊑-dec l' A
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p (⊑-trans l'⊑l l⊑A))
  ξ-L-get-unmasked-justᴸ A l l⊑A ((ex , l' , l'⊑l) ∷ exs) M p | .true because ofʸ p'
    with not (M ex)
  ξ-L-get-unmasked-justᴸ A l l⊑A ((ex , l' , l'⊑l) ∷ exs) M refl | .true because ofʸ p' | true = refl
  ... | false with first (not ∘ M ∘ fst) exs | inspect (first (not ∘ M ∘ fst)) exs |
                   first (maybe′ (not ∘ M ∘ fst) false) (map (ε-ξ A) exs) | inspect (first (maybe′ (not ∘ M ∘ fst) false)) (map (ε-ξ A) exs)
  ξ-L-get-unmasked-justᴸ A l l⊑A ((ex , l' , l'⊑l) ∷ exs) M p | .true because ofʸ p' | false | just x | [ eq ] | ● | [ eq' ]
    with trans (sym eq') (ε-first-M-justᴸ A l l⊑A exs M eq)
  ... | ()
  ξ-L-get-unmasked-justᴸ A l l⊑A ((ex , l' , l'⊑l) ∷ exs) M refl | .true because ofʸ p' | false | just (.(_ , _) , _) | [ eq ] | just (● , _) | [ eq' ]
    with trans (sym eq') (ε-first-M-justᴸ A l l⊑A exs M eq)
  ... | ()
  ξ-L-get-unmasked-justᴸ A l l⊑A ((ex , l' , l'⊑l) ∷ exs) M refl | .true because ofʸ p' | false | just (.(_ , _) , _) | [ eq ] | just (just x , _) | [ eq' ]
    with trans (sym eq') (ε-first-M-justᴸ A l l⊑A exs M eq)
  ... | refl with Label-⊑-dec l' A
  ... | .false because ofⁿ ¬p  = ⊥-elim (¬p (⊑-trans l'⊑l l⊑A))
  ... | .true  because ofʸ p'' = refl

  ξ-L-get-unmasked-nothingᴸ : ∀ (A : Label) (l : Label) → (l⊑A : l ⊑ A)
                              → (exs : SC.ξ-List l) (M : SC.ξ-Mask)
                              → SC.ξ-L-get-unmasked exs  M                      ≡ nothing
                              → SCˢ.ξ-L-get-unmasked (just (map (ε-ξ A) exs)) M ≡ nothing
  ξ-L-get-unmasked-nothingᴸ A l l⊑A [] M p = refl
  ξ-L-get-unmasked-nothingᴸ A l l⊑A ((ex , (l' , l'⊑l)) ∷ exs) M p
    with Label-⊑-dec l' A
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p (⊑-trans l'⊑l l⊑A))
  ... | .true  because ofʸ p'
    with not (M ex)
  ξ-L-get-unmasked-nothingᴸ A l l⊑A ((ex , l' , l'⊑l) ∷ exs) M () | .true because ofʸ p' | true
  ... | false with first (not ∘ M ∘ fst) exs | inspect (first (not ∘ M ∘ fst)) exs |
                   first (maybe′ (not ∘ M ∘ fst) false) (map (ε-ξ A) exs) | inspect (first (maybe′ (not ∘ M ∘ fst) false)) (map (ε-ξ A) exs)
  ... | a | [ eq ] | ● | [ eq' ] = refl
  ξ-L-get-unmasked-nothingᴸ A l l⊑A ((ex , l' , l'⊑l) ∷ exs) M refl | .true because ofʸ p' | false | ● | [ eq ] | just (_ , _) | [ eq' ]
    with trans (sym eq') (ε-first-M-nothingᴸ A l l⊑A exs M eq)
  ... | ()

  ξ-L-get-justᴸ : ∀ (A : Label) (l : Label) → (l⊑A : l ⊑ A)
                  → (exs : SC.ξ-List l) {ex : ξ} {l'×l'⊑l : ∃ (_⊑ l)} {exs' : SC.ξ-List l}
                  → SC.ξ-L-get exs                       ≡ just ((ex , l'×l'⊑l) , exs')
                  → SCˢ.ξ-L-get (just (map (ε-ξ A) exs)) ≡ just ((ex , l'×l'⊑l) , just (map (ε-ξ A) exs'))
  ξ-L-get-justᴸ A l l⊑A ((ex , l' , l'⊑l) ∷ exs) refl
    with Label-⊑-dec l' A
  ... | .true because ofʸ p   = refl
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p (⊑-trans l'⊑l l⊑A))

  private
    εˢ-new-com-high-aux : (A : Label) (l : Label) (l⋤A : ¬ (l ⊑ A))
                        → (Σ : SC.Store) {τ : Ty}
                        → ∀ l'
                        → εₛ A Σ l' ≡ εₛ A (SC.newˢ Σ l τ) l'
    εˢ-new-com-high-aux A l l⋤A Σ l'
        with Label-⊑-dec l' A
    εˢ-new-com-high-aux A l l⋤A Σ l' | .true because ofʸ p
        with Label-≡-dec l l'
    ... | .true because ofʸ refl = ⊥-elim (l⋤A p)
    ... | .false because ofⁿ ¬p = refl
    εˢ-new-com-high-aux A l l⋤A Σ l' | .false because ofⁿ ¬p = refl

  εˢ-new-comᴴ : (A : Label) (l : Label) (l⋤A : ¬ (l ⊑ A))
                  → (Σ : SC.Store){τ : Ty}
                  → εₛ A Σ ≡ εₛ A (SC.newˢ Σ l τ)
  εˢ-new-comᴴ A l l⋤A Σ = fun-ext (εˢ-new-com-high-aux A l l⋤A Σ)

  private
    εˢ-[↦]-com-high-aux : (A : Label) (l : Label) (l⋤A : ¬ (l ⊑ A))
                        → (Σ : SC.Store) (mem : List (∃ SC.Cell))
                        → ∀ l'
                        → εₛ A Σ l' ≡ εₛ A (Σ [ l ↦ mem ]ˢ) l'
    εˢ-[↦]-com-high-aux A l l⋤A Σ mem l'
      with Label-⊑-dec l' A
    εˢ-[↦]-com-high-aux A l l⋤A Σ mem l' | .true because ofʸ p
      with Label-≡-dec l l'
    ... | .true because ofʸ refl = ⊥-elim (l⋤A p)
    ... | .false because ofⁿ ¬p  = refl
    εˢ-[↦]-com-high-aux A l l⋤A Σ mem l' | .false because ofⁿ ¬p = refl

  εˢ-[↦]-comᴸ : ∀ (A : Label) (l : Label) (l⋤A : ¬ (l ⊑ A))
                → (Σ : SC.Store) (mem : List (∃ SC.Cell))
                → εₛ A Σ ≡ εₛ A (Σ [ l ↦ mem ]ˢ)
  εˢ-[↦]-comᴸ A l l⋤A Σ mem = fun-ext (εˢ-[↦]-com-high-aux A l l⋤A Σ mem)

  ↦∈ˢ-com : ∀ (A : Label) (l : Label) → (l⊑A : l ⊑ A)
              (Σ : SC.Store) (n : ℕ) {τ : Ty} (cell : SC.Cell τ)
            → n SC.↦  (τ , cell) ∈ˢ (Σ l)
            → n SCˢ.↦ (τ , εᶜ A cell) ∈ˢ (εₛ A Σ l)
  ↦∈ˢ-com A l l⊑A Φ n c P with Label-⊑-dec l A
  ↦∈ˢ-com A l l⊑A Φ n c P | .true because ofʸ p = aux n c (Φ l) P
    where
        aux : ∀ (n : ℕ) {τ : Ty} (cell : SC.Cell τ) (m : SC.Mem)
              → n SC.↦  (τ , cell) ∈ˢ m
              → n SCˢ.↦ (τ , εᶜ A cell) ∈ˢ εₘ A m
        aux _ cell .((_ , cell) ∷ _) here = here
        aux .(_) cell .(_ ∷ _) (there x) = there (Lookupℕ-map x)
  ↦∈ˢ-com A l l⊑A Φ n c P | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
              
  ≔[↦]ˢ-com : ∀ (A : Label) (l : Label) → (l⊑A : l ⊑ A)
                (Σ : SC.Store) (n : ℕ) {τ : Ty} (cell : SC.Cell τ) (m : SC.Mem)
            →  m SC.≔ (Σ l) [ n ↦ (τ , cell) ]ˢ
            →  (εₘ A m) SCˢ.≔ (εₛ A Σ l) [ n ↦ (τ , εᶜ A cell) ]ˢ
  ≔[↦]ˢ-com A l l⊑A Σ n th m p
    with Label-⊑-dec l A
  ≔[↦]ˢ-com A l l⊑A Σ n c m p | .true because ofʸ p'  = aux n c (Σ l) m p
    where  aux : ∀ (n : ℕ) {τ : Ty} (cell : SC.Cell τ) (m : SC.Mem) (m' : SC.Mem)
                 → m' SC.≔ m [ n ↦ (τ , cell) ]ˢ
                 → (εₘ A m') SCˢ.≔ (εₘ A m) [ n ↦ (τ , εᶜ A cell) ]ˢ
           aux .0 c .(_ ∷ _) .(_ ∷ _) here = here
           aux .(_) c .(_ ∷ _) .(_ ∷ _) (there x) = there (Updateℕ-map x)
  ≔[↦]ˢ-com A l l⊑A Σ n c P p | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
