module Security.Sequential.Lemmas where

  open import Security.Calculus.Base
  open import Security.Sequential.Calculus
  open import Security.Sequential.Semantics

  open import Data.Product using (∃; _×_; _,_)
  open import Data.Product.Extra

  open import Relation.Nullary using (¬_)

  return¬⟼e : ∀ {l τ} {ϕ} {Σ} {M} {E} {t : CTerm τ} {de} {ev} {Σ'} {t'} 
               → ¬ ([ ϕ ] Σ , return l t [ M , E ]⟼e[ de , ev ] Σ' , t')
  return¬⟼e (LiftM↯ is↯ ())
  return¬⟼e (LiftM◍ ¬is↯ x ())
  return¬⟼e (LiftM○↯ ¬is↯ x ())
  return¬⟼e (LiftC◍ x ())
  return¬⟼e (LiftC○↯ x ())
  return¬⟼e (LiftP○ x ())
  return¬⟼e (LiftP◍ x ())
  return¬⟼e (LiftS◍ x ())
  return¬⟼e (LiftS○ x ())
  return¬⟼e (LiftS◍ₛ x x₁ ())
  return¬⟼e (LiftSₛ ())
  
  χ¬⟼e : ∀ {l τ} {ϕ} {Σ} {e} {M} {E} {de} {ev} {Σ'} {t' : CTerm (MAC l τ)}
          → ¬ ([ ϕ ] Σ , χ l e [ M , E ]⟼e[ de , ev ] Σ' , t')
  χ¬⟼e (LiftM↯ is↯ ())
  χ¬⟼e (LiftM◍ ¬is↯ x ())
  χ¬⟼e (LiftM○↯ ¬is↯ x ())
  χ¬⟼e (LiftC◍ x ())
  χ¬⟼e (LiftC○↯ x ())
  χ¬⟼e (LiftP○ x ())
  χ¬⟼e (LiftP◍ x ())
  χ¬⟼e (LiftS◍ x ())
  χ¬⟼e (LiftS○ x ())
  χ¬⟼e (LiftS◍ₛ x x₁ ())
  χ¬⟼e (LiftSₛ ())

  ⟼-Values-do-not-step : ∀ {l τ} {ϕ} {Σ} {t : CTerm (MAC l τ)} {M} {exs}
                         → ¬ (⟼-Value t × ∃₄ λ d ev Σ' t' → [ ϕ ] Σ , t [ M , exs ]⟼e[ d , ev ] Σ' , t')
  ⟼-Values-do-not-step (χ l e , _ , _ , _ , _ , s) = χ¬⟼e s
  ⟼-Values-do-not-step (return l t , _ , _ , _ , _ , s) = return¬⟼e s

  ⟼c∨⟼m-excl : ∀ {l} {τ} {t : CTerm (MAC l τ)}
                → ¬ ((∃ λ t' → t ⟼m t') × (∃₄ λ ϕ M ev t' → [ ϕ ] t [ M ]⟼c[ ev ] t'))
  ⟼c∨⟼m-excl ((_ , ()) , _ , _ , _ , _ , Fork l⊑h)
  ⟼c∨⟼m-excl ((_ , ()) , _ , _ , _ , _ , ThrowTo₁ e l⊑h x)
  ⟼c∨⟼m-excl ((_ , ()) , _ , _ , _ , _ , ThrowTo₂ e l⊑h n)

  ⟼m∨↦-excl : ∀ {l} {τ} {t : CTerm (MAC l τ)}
               → ¬ ((∃ λ t' → t ⟼m t') × (∃ λ t' →  t ↦ t'))
  ⟼m∨↦-excl ((_ , Mask) , _ , ())
  ⟼m∨↦-excl ((_ , Maskχ) , _ , ())
  ⟼m∨↦-excl ((_ , Bind) , _ , ())
  ⟼m∨↦-excl ((_ , Bindχ) , _ , ())
  ⟼m∨↦-excl ((_ , Label' l⊑h) , _ , ())
  ⟼m∨↦-excl ((_ , UnMask) , _ , ())
  ⟼m∨↦-excl ((_ , UnMaskχ) , _ , ())
  ⟼m∨↦-excl ((_ , Unlabel₁ l⊑h x) , _ , ())
  ⟼m∨↦-excl ((_ , Unlabel₂ l⊑h) , _ , ())
  ⟼m∨↦-excl ((_ , Catch) , _ , ())

  ⟼c∨↦-excl : ∀ {l} {τ} {t : CTerm (MAC l τ)}
               → ¬ ((∃₄ λ ϕ M ev t' → [ ϕ ] t [ M ]⟼c[ ev ] t') × (∃ λ t' →  t ↦  t'))
  ⟼c∨↦-excl ((_ , _ , _ , _ , Fork l⊑h) , _ , ())
  ⟼c∨↦-excl ((_ , _ , _ , _ , ThrowTo₁ e l⊑h x) , _ , ())
  ⟼c∨↦-excl ((_ , _ , _ , _ , ThrowTo₂ e l⊑h n) , _ , ())

  ⟼s∨↦-excl : ∀ {l} {τ} {Σ : Store} {t : CTerm (MAC l τ)}
               → ¬ ((∃₃ λ sev Σ' t' → Σ , t ⟼s[ sev ] Σ' , t') × (∃ λ t' →  t ↦  t'))
  ⟼s∨↦-excl ((_ , _ , _ , NewMVar l⊑h) , _ , ())
  ⟼s∨↦-excl ((_ , _ , _ , TakeMVar₁ d) , _ , ())
  ⟼s∨↦-excl ((_ , _ , _ , TakeMVar₂ _ _) , _ , ())
  ⟼s∨↦-excl ((_ , _ , _ , TakeMVarₛ _) , _ , ())
  ⟼s∨↦-excl ((_ , _ , _ , PutMVar₁ d) , _ , ())
  ⟼s∨↦-excl ((_ , _ , _ , PutMVar₂ _ _) , _ , ())
  ⟼s∨↦-excl ((_ , _ , _ , PutMVarₛ _) , _ , ())

  ⟼s∨⟼m-excl : ∀ {l} {τ} {Σ : Store} {t : CTerm (MAC l τ)}
                → ¬ ((∃ λ t' → t ⟼m t') × (∃₃ λ sev Σ' t' →  Σ , t ⟼s[ sev ] Σ' , t'))
  ⟼s∨⟼m-excl ((_ , Mask) , _ , _ , ())
  ⟼s∨⟼m-excl ((_ , Maskχ) , _ , _ , ())
  ⟼s∨⟼m-excl ((_ , Bind) , _ , _ , ())
  ⟼s∨⟼m-excl ((_ , Bindχ) , _ , _ , ())
  ⟼s∨⟼m-excl ((_ , Label' l⊑h) , _ , _ , ())
  ⟼s∨⟼m-excl ((_ , UnMask) , _ , _ , ())
  ⟼s∨⟼m-excl ((_ , UnMaskχ) , _ , _ , ())
  ⟼s∨⟼m-excl ((_ , Unlabel₁ l⊑h x) , _ , _ , ())
  ⟼s∨⟼m-excl ((_ , Unlabel₂ l⊑h) , _ , _ , ())
  ⟼s∨⟼m-excl ((_ , Catch) , _ , _ , ())

  ⟼c∨⟼s-excl : ∀ {l} {τ} {Σ : Store} {t : CTerm (MAC l τ)}
                → ¬ ((∃₃ λ sev t' Σ' → Σ , t ⟼s[ sev ] Σ' , t') × (∃₄ λ ϕ M ev t' → [ ϕ ] t [ M ]⟼c[ ev ] t'))
  ⟼c∨⟼s-excl ((_ , _ , _ , NewMVar l⊑h) , _ , _ , ())
  ⟼c∨⟼s-excl ((_ , _ , _ , TakeMVar₁ d) , _ , _ , ())
  ⟼c∨⟼s-excl ((_ , _ , _ , TakeMVar₂ _ _) , _ , _ , ())
  ⟼c∨⟼s-excl ((_ , _ , _ , TakeMVarₛ _) , _ , _ , ())
  ⟼c∨⟼s-excl ((_ , _ , _ , PutMVar₁ d) , _ , _ , ())
  ⟼c∨⟼s-excl ((_ , _ , _ , PutMVar₂ _ _) , _ , _ , ())
  ⟼c∨⟼s-excl ((_ , _ , _ , PutMVarₛ _) , _ , _ , ())
