open import Lattice using (Label)
module Security.Sequential.LowEq (A : Label) where

  open import Security.Calculus.Base
  open import Security.Concurrent.Erasure
  open import Security.Sequential.Erasure

  open import Sequential.Calculus as SC
  open import Security.Sequential.Calculus as SCˢ

  open import Relation.Binary.PropositionalEquality

  _≈ₛ_ : (Σ₁ Σ₂ : SC.Store) → Set
  Σ₁ ≈ₛ Σ₂ = εₛ A Σ₁ ≡  εₛ A Σ₂
