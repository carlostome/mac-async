open import Lattice using (Label)
module Security.Simulation.Concurrent (A : Label) where

  open import Calculus.Base                   as C
  open import Security.Calculus.Base          as Cˢ
  open import Security.Calculus.Erasure       

  open import Data.Bool using (Bool; true; false)
  open import Data.List using (List; _∷_; []; map; length; _++_)
  open import Data.List.Properties using (map-++-commute; length-map)
  open import Data.List.Extra
  open import Data.Maybe using (Maybe; just; maybe′) renaming (nothing to ●; map to mapM)
  open import Data.Nat  using (ℕ; suc)
  open import Data.Product using (_×_; Σ; _,_)
  open import Data.Unit using (⊤; tt)
  open import Function using (_∘_)
  open import Function.Extensionality

  open import Relation.Binary.PropositionalEquality using (_≡_; refl; sym; trans; cong; inspect; [_])
  open import Relation.Nullary

  open import Concurrent.Semantics as CS  
  open import Concurrent.Calculus as CC
  open import Security.Concurrent.Calculus as CCˢ
  open import Security.Concurrent.Erasure
  open import Security.Simulation.Sequential
  open import Security.Sequential.Calculus as SCˢ 
  open import Sequential.Calculus as SC
  open import Security.Sequential.Erasure

  open import Security.Concurrent.Semantics as CSˢ renaming (_,_⊢[_]_↪_ to _,_⊢[_]_↪●_
                                                            ;[_]_,_⟼t[_]_,_ to [_]_,_⟼t●[_]_,_)
  open import Scheduler.Base
  open import Scheduler.Security

  open import Data.Empty using (⊥-elim)

  ε-⟼t-simᴸ : ∀ (l : Label) → (l⊑A : l ⊑ A)
                 {ϕ : SC.Idˢ}
                 {Σ : SC.Store}
                 {th : CC.Thread l}
                 {cev : SC.CEvent l}
                 {Σ' : SC.Store}
                 {th' : CC.Thread l}
               → [ ϕ ] Σ , th ⟼t[ cev ] Σ' , th'
               → [ ε-Idˢ A ϕ  ] εₛ A Σ , εₜ-Thread A l th ⟼t●[ εₜ-CEvent A cev ] εₛ A Σ' , εₜ-Thread A l th'
  ε-⟼t-simᴸ l l⊑A (Lift s)
    with Label-⊑-dec l A
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ... | .true because ofʸ p
    with ε-⟼e-simᴸ A l l⊑A s
  ... | s'
    with Label-⊑-dec l A
  ... | .true because ofʸ _   = Lift s'
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)

  ε-⟼t-simᴴ : ∀ (l : Label)
               → (l⋤A : ¬ (l ⊑ A))
               → {ϕ : SC.Idˢ}
               → {Σ : SC.Store}
               → {th  : CC.Thread l}
               → {cev : SC.CEvent l}
               → {Σ' : SC.Store}
               → {th' : CC.Thread l}
               → [ ϕ ] Σ , th ⟼t[ cev ] Σ' , th'
               → εₛ A Σ ≡ εₛ A Σ'
  ε-⟼t-simᴴ l l⋤A (Lift s) = ε-⟼e-simᴴ A l l⋤A s

  private
    lemma₁ : ∀ (A : Label) (l : Label) → (l⊑A : l ⊑ A)
                (Φ : CC.TPool) (h : Label) (l⊑h : l ⊑ h) (h⊑A : h ⊑ A)
                (nᴴ : ℕ) (t : C.CTerm (MAC h （）)) (M : SC.ξ-Mask) (exs : SC.ξ-List h) (e : ξ) (Pᴸ : CC.Pool l) (Pᴴ : CC.Pool h)
            →  Pᴴ CC.≔ ((Φ CC.[ l ↦ Pᴸ ]ᵀ) h) [ nᴴ ↦ ⟨ t , M , (exs ++ (e , l , l⊑h) ∷ []) ⟩ ]ᴾ
            →  (ε-Pool A h Pᴴ) CCˢ.≔ (((ε-TPool A Φ) CCˢ.[ l ↦ ε-Pool A l Pᴸ ]ᵀ) h) [ nᴴ ↦ just ⟨ ε A t , M , just (map (ε-ξ A) exs ++ (just (e , l , l⊑h) ∷ [])) ⟩ ]ᴾ
    lemma₁ A l l⊑A Φ h l⊑h h⊑A nᴴ t M exs e Pᴸ Pᴴ x
        with Label-≡-dec l h
    ... | .false because ofⁿ ¬p
        with Label-⊑-dec h A
    ... | .false because ofⁿ ¬p' = ⊥-elim (¬p' h⊑A)
    ... | .true because ofʸ p'
        with Updateℕ-map {f = εₜ-Thread A h} x
    ... | r
        rewrite map-++-commute (ε-ξ A) exs ((e , l , l⊑h) ∷ [])
        with Label-⊑-dec l A
    ... | .true because ofʸ _   = r
    ... | .false because ofⁿ ¬p'' = ⊥-elim (¬p'' l⊑A)
    lemma₁ A l l⊑A Φ h l⊑h h⊑A nᴴ t M exs e Pᴸ Pᴴ x | .true because ofʸ refl
        with Updateℕ-map {f = εₜ-Thread A h} x
    ... | r rewrite map-++-commute (ε-ξ A) exs ((e , l , l⊑h) ∷ [])
        with Label-⊑-dec l A
    ... | .true because ofʸ _   = r
    ... | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)


    lemma₂ : ∀ (A : Label) (l : Label) → (l⊑A : l ⊑ A) 
                (h : Label) (h⊑A : h ⊑ A)
                (Φ : CC.TPool) (P : CC.Pool l)
            → ε-Pool A h ((Φ CC.[ l ↦ P ]ᵀ) h) ≡ ((ε-TPool A Φ) CCˢ.[ l ↦ ε-Pool A l P ]ᵀ) h
    lemma₂ A l l⊑A h h⊑A Φ P with Label-≡-dec l h
    ... | .true because ofʸ refl = refl
    ... | .false because ofⁿ ¬eq
        with Label-⊑-dec h A
    ... | .true because ofʸ p   = refl
    ... | .false because ofⁿ ¬p = ⊥-elim (¬p h⊑A)

    lemma₃ : ∀ (Φ : CC.TPool) → ε-Idˢ A (CC.mkIdˢ Φ) ≡ mapM length ∘ (ε-TPool A Φ) 
    lemma₃ Φ = fun-ext (aux A Φ)
        where aux : (A : Label) → (Φ : CC.TPool) → ∀ l → ε-Idˢ A (CC.mkIdˢ Φ) l ≡ mapM length (ε-TPool A Φ l)
              aux A Φ l with Label-⊑-dec l A
              ... | .true because ofʸ p   = cong just (sym (length-map (εₜ-Thread A l) (Φ l)))
              ... | .false because ofⁿ ¬p = refl

    lemma₄ : ∀ (l : Label) (l⊑A : l ⊑ A) → (h : Label) (l⊑h : l ⊑ h) (h⊑A : h ⊑ A)
             → (Φ : CC.TPool) → (P : CC.Pool l) → {n : ℕ} {th : CC.Thread l} → P CC.≔ Φ l [ n ↦ th ]ᴾ
             → CC.sizeᵀ Φ h ≡  CCˢ.sizeᵀ (ε-TPool A Φ CCˢ.[ l ↦ ε-Pool A l P ]ᵀ) h
    lemma₄ l l⊑A h l⊑h h⊑A Φ P p
        with Label-≡-dec l h
    ... | .true because ofʸ refl = trans (Updateℕ-length p) (sym (length-map (εₜ-Thread A l) P))
    ... | .false because ofⁿ ¬p
        with Label-⊑-dec h A
    ... | .false because ofⁿ ¬p' = ⊥-elim (¬p' h⊑A)
    ... | .true because ofʸ p' = sym (length-map (εₜ-Thread A h) (Φ h))

  ⟼t-Value⇒⟼t-Value : ∀ (l : Label) → (th : CC.Thread l)
                        → CS.⟼t-Value th → CSˢ.⟼t-Value (εₜ-Thread A l th)
  ⟼t-Value⇒⟼t-Value l .(⟨ _ , M , exs ⟩) ⟨ ⟼-isV , M , exs ⟩ = ⟨ ⟼-Value⇒⟼-Value A l _ ⟼-isV , M , (just (map (ε-ξ A) exs)) ⟩

  -- unwinding lemmata for the concurrent calculus
  module _ {Sch : Scheduler} {Sch-NIˢ : NIˢ A Sch} where

    open NIˢ Sch-NIˢ

    ε-↪-simᴸ : ∀ (l : Label) → (l⊑A : l ⊑ A)
               → {n : ℕ} {c₁ c₂ : CC.Global Sch}
               → l , n ⊢[ Sch ] c₁            ↪  c₂
               → l , n ⊢[ Sch ] εᴳ A {Sch} {Sch-NIˢ} c₁ ↪● εᴳ A {Sch} {Sch-NIˢ} c₂
    ε-↪-simᴸ l l⊑A (Step {Φ = Φ} {Pᴸ = Pᴸ} {th₁ = th₁}  p₁ p₂ d sch-step)
      with ε-⟼t-simᴸ l l⊑A d
    ... | d'
      rewrite ε-Thread-[↦]-com-low A l l⊑A Φ Pᴸ
            | lemma₃ Φ
      = Step (↦∈ᴾ-com A l l⊑A Φ _ th₁ p₁)
             (≔[↦]ᴾ-com A l l⊑A Φ _ _ _ p₂)
             d'
             (εˢ-simᴸ l⊑A sch-step)
    ε-↪-simᴸ l l⊑A (Fork {Φ = Φ} {Pᴸ = Pᴸ} {h = h}  {l⊑h = l⊑h} {th₁ = th₁} {th₂ = th₂} {t = t} p₁ p₂ d sch-step)
      with ε-⟼t-simᴸ l l⊑A d | εˢ-simᴸ l⊑A sch-step
    ... | d' | sch-step'
      with Label-⊑-dec h A
    ε-↪-simᴸ l l⊑A {n} (Fork {Φ} {Pᴸ} {Σ₁} {ω} {ω'} {h} {l⊑h} {th₁} {th₂} {t} {M} p₁ p₂ d sch-step) | d' | sch-step' | .false because ofⁿ ¬p
      rewrite sym (ε-Thread-[↦]-com-high A h ¬p ((Φ CC.[ l ↦ Pᴸ ]ᵀ))  ((Φ CC.[ l ↦ Pᴸ ]ᵀ) h ++ CS.fork-child t M ∷ []))
            | ε-Thread-[↦]-com-low A l l⊑A Φ Pᴸ 
            | lemma₃ Φ
      with Label-⊑-dec l A
    ... | .false because ofⁿ ¬p' = ⊥-elim (¬p' l⊑A)
    ... | .true because ofʸ p  
      = Fork● (↦∈ᴾ-com A l l⊑A Φ _ _ p₁)
              (≔[↦]ᴾ-com A l l⊑A Φ _ _ _ p₂)
              d'
              sch-step'  
    ε-↪-simᴸ l l⊑A {n} (Fork {Φ} {Pᴸ} {Σ₁} {ω} {ω'} {h} {l⊑h} {th₁} {th₂} {t} {M} p₁ p₂ d sch-step) | d' | sch-step' | .true because ofʸ p
      rewrite ε-Thread-[↦]-com-low A h p (Φ CC.[ l ↦ Pᴸ ]ᵀ) ((Φ CC.[ l ↦ Pᴸ ]ᵀ) h  ++ CS.fork-child t M ∷ [])
            | ε-Thread-[↦]-com-low A l l⊑A Φ Pᴸ 
            | map-++-commute (εₜ-Thread A h) ((Φ CC.[ l ↦  Pᴸ ]ᵀ) h) (CS.fork-child t M ∷ [])
            | lemma₃ Φ
            | lemma₄ l l⊑A h l⊑h p Φ Pᴸ p₂
      = Fork (↦∈ᴾ-com A l l⊑A Φ _ th₁ p₁)
             (≔[↦]ᴾ-com A l l⊑A Φ _ _ _ p₂)
             (lemma₂ A l l⊑A h p Φ Pᴸ)
             d'
             sch-step' 
    ε-↪-simᴸ l l⊑A (Throw {Φ = Φ} {Pᴸ = Pᴸ} {h = h} {l⊑h = l⊑h} {th₁ = th₁} {e = e}  {Pᴴ = Pᴴ} {thξ = thξ } p₁ p₂ p₃ p₄ d sch-step)
      with ε-⟼t-simᴸ l l⊑A d
    ... | d'
      with Label-⊑-dec h A
    ... | .false because ofⁿ ¬p
      rewrite sym (ε-Thread-[↦]-com-high A h ¬p ((Φ CC.[ l ↦ Pᴸ ]ᵀ))  Pᴴ)
            | ε-Thread-[↦]-com-low A l l⊑A Φ Pᴸ 
            | lemma₃ Φ
      = Throw● (↦∈ᴾ-com A l l⊑A Φ _ _ p₁)
               (≔[↦]ᴾ-com A l l⊑A Φ _ _ Pᴸ p₃)
               d'
               (εˢ-simᴸ l⊑A sch-step)
    ... | .true because ofʸ p 
      rewrite ε-Thread-[↦]-com-low A h p (Φ CC.[ l ↦ Pᴸ ]ᵀ) Pᴴ
            | ε-Thread-[↦]-com-low A l l⊑A Φ Pᴸ
            | map-++-commute (ε-ξ A) (Thread.exceptions thξ) ((e , l , l⊑h) ∷ [] )
            | lemma₃ Φ
      = Throw (↦∈ᴾ-com A l l⊑A Φ _ _ p₁)
              (↦∈ᴾ-com₂ A l l⊑A Φ _ h p _ Pᴸ p₂)
              (≔[↦]ᴾ-com A l l⊑A Φ _ _ Pᴸ p₃)
              (lemma₁ A l l⊑A Φ h l⊑h p _ _ _ _ e Pᴸ Pᴴ p₄ )
              d'
              (εˢ-simᴸ l⊑A sch-step)
    ε-↪-simᴸ l l⊑A (Stuck {Φ} p d sch-step)
      with ε-⟼t-simᴸ l l⊑A d
    ... | d'
      rewrite lemma₃ Φ
      = Stuck (↦∈ᴾ-com A l l⊑A _ _ _ p) d' (εˢ-simᴸ l⊑A sch-step)
    ε-↪-simᴸ l l⊑A (Done isV-th₁ p sch-step)
      = Done (⟼t-Value⇒⟼t-Value l _ isV-th₁) (↦∈ᴾ-com A l l⊑A _ _ _ p) (εˢ-simᴸ l⊑A sch-step)

    ε-↪-simᴴ : ∀ (l : Label) → (l⋤A : ¬ (l ⊑ A))
                 {n : ℕ} {c₁ c₂ : CC.Global Sch}
               → l , n ⊢[ Sch ] c₁ ↪ c₂
               → εᴳ A {Sch} {Sch-NIˢ} c₁ ≡ εᴳ A {Sch} {Sch-NIˢ} c₂
    ε-↪-simᴴ l l⋤A (Step {Φ = Φ} {Pᴸ = Pᴸ} p₁ p₂ d sch-step)
      rewrite ε-⟼t-simᴴ l l⋤A d
            | ε-Thread-[↦]-com-high A l l⋤A Φ Pᴸ
            | ⌞ εˢ-simᴴ l⋤A sch-step ⌟ˢ
            = refl
    ε-↪-simᴴ l l⋤A (Fork {Φ = Φ} {Pᴸ = Pᴸ} {h = h} {l⊑h = l⊑h} {th₁ = th₁}  {t = t} {M} p₁ p₂ d sch-step)
      rewrite ε-⟼t-simᴴ l l⋤A d
            | ε-Thread-[↦]-com-high A l l⋤A Φ Pᴸ 
            | ε-Thread-[↦]-com-high A h (⋤-trans l⊑h l⋤A) (Φ CC.[ l ↦ Pᴸ ]ᵀ) ((Φ CC.[ l ↦ Pᴸ ]ᵀ) h  ++ CS.fork-child t M ∷ [])
            | ⌞ εˢ-simᴴ l⋤A sch-step ⌟ˢ
            = refl 
    ε-↪-simᴴ l l⋤A (Throw {Φ = Φ} {Pᴸ = Pᴸ} {h = h} {l⊑h = l⊑h} {th₁ = th₁} {Pᴴ = Pᴴ} {thξ = thξ} p₁ p₂ p₃ p₄ d sch-step)
      rewrite ε-⟼t-simᴴ l l⋤A d
            | ε-Thread-[↦]-com-high A l l⋤A Φ Pᴸ
            | ε-Thread-[↦]-com-high A h (⋤-trans l⊑h l⋤A) (Φ CC.[ l ↦ Pᴸ ]ᵀ) Pᴴ
            | ⌞ εˢ-simᴴ l⋤A sch-step ⌟ˢ
            = refl
    ε-↪-simᴴ l l⋤A (Stuck p d sch-step)
      rewrite ⌞ εˢ-simᴴ l⋤A sch-step ⌟ˢ = refl
    ε-↪-simᴴ l l⋤A (Done isV-th₁ x sch-step)
      rewrite ⌞ εˢ-simᴴ l⋤A sch-step ⌟ˢ = refl
