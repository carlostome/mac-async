module Security.Simulation.Sequential where

  open import Lattice
  open import Exception
  open import Types 

  open import Calculus.Base          as C
  open import Sequential.Calculus    as SC
  open import Security.Calculus.Base as Cˢ

  open import Sequential.Semantics          as S
  open import Security.Sequential.Semantics as Sˢ renaming ([_]_,_[_,_]⟼e[_,_]_,_ to [_]_,_[_,_]⟼e●[_,_]_,_
                                                           ;_↦_ to _↦●_
                                                           ;_⟼m_ to _⟼m●_
                                                           ;_,_⟼s[_]_,_ to _,_⟼s●[_]_,_
                                                           ;[_]_[_]⟼c[_]_ to [_]_[_]⟼c●[_]_)
  open import Security.Sequential.Erasure
  open import Security.Calculus.Erasure

  open import Data.Bool        using (true; false)
  open import Data.Empty       using (⊥-elim)
  open import Data.List        using (List; _∷_; []; map; length)
  open import Data.List.Extra
  open import Data.Maybe       using (just; maybe′)
  open import Data.Product     using (∃; _×_; _,_)

  open import Relation.Nullary
  open import Relation.Binary.PropositionalEquality using (_≡_; refl; sym)

  step-sim-ε-↦ : (A : Label)
                → {τ : Ty}
                → {t t' : C.CTerm τ}
                → t      ↦  t'
                → ε A t ↦● ε A t'
  step-sim-ε-↦ A (App s) = App (step-sim-ε-↦ A s)
  step-sim-ε-↦ A (Beta {t₁ = t₁} {t₂ = t₂})
    rewrite sym (subst-hom-ε A {Σ = []} t₁ (t₂ ∷ [])) = Beta
  step-sim-ε-↦ A (If₁ s) = If₁ (step-sim-ε-↦ A s)
  step-sim-ε-↦ A If₂ = If₂
  step-sim-ε-↦ A If₃ = If₃

  ε-⟼m-simᴸ : ∀ (A l : Label) → (l⊑A : l ⊑ A)
               → {τ : Ty}
               → {t t' : C.CTerm (MAC l τ)}
               → t     ⟼m  t'
               → ε A t ⟼m● ε A t'
  ε-⟼m-simᴸ A l l⊑A Mask
    with Label-⊑-dec l A
  ... | .true because ofʸ p   = Mask
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ε-⟼m-simᴸ A l l⊑A Maskχ
    with Label-⊑-dec l A
  ... | .true because ofʸ p   = Maskχ
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ε-⟼m-simᴸ A l l⊑A Bind
    with Label-⊑-dec l A
  ... | .true because ofʸ p   = Bind
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ε-⟼m-simᴸ A l l⊑A Bindχ
    with Label-⊑-dec l A
  ... | .true because ofʸ p   = Bindχ
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ε-⟼m-simᴸ A l l⊑A (Label' {l'} l'⊑l)
    with Label-⊑-dec l A
  ε-⟼m-simᴸ A l l⊑A (Label' {l'} {h} l'⊑h) | .true because ofʸ p
    with Label-⊑-dec h A
  ... | .true because ofʸ p'  = Label' l'⊑h
  ... | .false because ofⁿ ¬p = Label● l'⊑h
  ε-⟼m-simᴸ A l l⊑A (Label' {l'} {h} l'⊑h) | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ε-⟼m-simᴸ A l l⊑A UnMask
    with Label-⊑-dec l A
  ... | .true because ofʸ p   = UnMask
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ε-⟼m-simᴸ A l l⊑A UnMaskχ
    with Label-⊑-dec l A
  ... | .true because ofʸ p   = UnMaskχ
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ε-⟼m-simᴸ A l l⊑A (Unlabel₁ l'⊑l x)
    with Label-⊑-dec l A
  ... | .true because ofʸ p   = Unlabel₁ l'⊑l (step-sim-ε-↦ A  x)
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ε-⟼m-simᴸ A l l⊑A (Unlabel₂ l'⊑h) 
    with Label-⊑-dec l A
  ε-⟼m-simᴸ A l l⊑A (Unlabel₂ {l'} l'⊑l) | .true because ofʸ p
    with Label-⊑-dec l' A
  ... | .true because ofʸ p'  = Unlabel₂ l'⊑l
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p (⊑-trans l'⊑l l⊑A))
  ε-⟼m-simᴸ A l l⊑A (Unlabel₂ l'⊑h) | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A) 
  ε-⟼m-simᴸ A l l⊑A Catch
    with Label-⊑-dec l A
  ... | .true because ofʸ p   = Catch
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)

  ε-⟼s-simᴸ : ∀ (A : Label) (l : Label) → (l⊑A : l ⊑ A)
                 {τ : Ty}
                 {Σ   : SC.Store}
                 {t   : C.CTerm (MAC l τ)}
                 {sev : SC.SEvent l}
                 {Σ'  : SC.Store}
                 {t' : C.CTerm (MAC l τ)}
               → Σ      , t        ⟼s[  sev             ] Σ'      ,     t'
               → εₛ A Σ , ε A t   ⟼s●[ εₜ-SEvent A sev ] εₛ A Σ' , ε A t'
  ε-⟼s-simᴸ A l l⊑A {Σ = Σ} (NewMVar  {h = h} {τ = τ} l⊑h)
    with Label-⊑-dec l A
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ... | .true because ofʸ p
    with Label-⊑-dec h A
  ... | .true because ofʸ p'
    rewrite newˢ-com A h Σ {τ = τ}
    | sizeˢ-comᴸ A h p' Σ = NewMVar {τ = τ} l⊑h
  ... | .false because ofⁿ ¬p 
    rewrite sizeˢ-comᴴ A h ¬p Σ {τ = τ} 
    = NewMVar● l⊑h
  ε-⟼s-simᴸ A l l⊑A (TakeMVar₁ d)
    with Label-⊑-dec l A
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ... | .true because ofʸ p'  = TakeMVar₁ (step-sim-ε-↦ A d)
  ε-⟼s-simᴸ A l l⊑A {Σ = Σ} (TakeMVar₂ {m' = m'} p₁ p₂)
    with Label-⊑-dec l A
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ... | .true because ofʸ p'  
      rewrite εˢ-updateᴸ A l l⊑A Σ m'
      = TakeMVar₂ (↦∈ˢ-com A l l⊑A Σ _ _ p₁) (≔[↦]ˢ-com A l p' Σ _ _ m' p₂)
  ε-⟼s-simᴸ A l l⊑A {Σ = Σ} (TakeMVarₛ  p)
    with Label-⊑-dec l A
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ... | .true because ofʸ p'  = TakeMVarₛ (↦∈ˢ-com A l l⊑A Σ _ ⊗ p) 
  ε-⟼s-simᴸ A l l⊑A (PutMVar₁ d)
    with Label-⊑-dec l A
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ... | .true because ofʸ p'  = PutMVar₁ (step-sim-ε-↦ A d)
  ε-⟼s-simᴸ A l l⊑A {Σ = Σ} (PutMVar₂ {m' = m'} p₁ p₂)
    with Label-⊑-dec l A
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ... | .true because ofʸ p'  
      rewrite εˢ-updateᴸ A l l⊑A Σ m'
      = PutMVar₂ (↦∈ˢ-com A l l⊑A Σ _ _ p₁) (≔[↦]ˢ-com A l p' Σ _ _ m' p₂)
  ε-⟼s-simᴸ A l l⊑A (PutMVarₛ p)
    with Label-⊑-dec l A
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ... | .true because ofʸ p'  = PutMVarₛ (↦∈ˢ-com A l l⊑A _ _ _ p) 

  ε-⟼s-simᴴ : ∀ (A : Label) (l : Label) → (l⋤A : ¬ (l ⊑ A))
                 {τ : Ty}
                 {Σ   : SC.Store}
                 {t   : C.CTerm (MAC l τ)}
                 {Σ'  : SC.Store}
                 {t'  : C.CTerm (MAC l τ)}
               → Σ , t ⟼s[ step ] Σ' , t'
               → εₛ A Σ  ≡ εₛ A Σ'
  ε-⟼s-simᴴ A l l⋤A (NewMVar l⊑h) = εˢ-new-comᴴ A _ (⋤-trans l⊑h l⋤A) _
  ε-⟼s-simᴴ A l l⋤A (TakeMVar₁ d) = refl
  ε-⟼s-simᴴ A l l⋤A (TakeMVar₂ n∈Σl lk≡) = εˢ-[↦]-comᴸ A l l⋤A _ _
  ε-⟼s-simᴴ A l l⋤A (PutMVar₁ d) = refl
  ε-⟼s-simᴴ A l l⋤A (PutMVar₂ n∈Σl lk≡) = εˢ-[↦]-comᴸ A l l⋤A _ _

  private
    lemma₁ : ∀ (A : Label) (h : Label) → (h⊑A : h ⊑ A)
             → (ϕ : SC.Idˢ)
             → just (ϕ h) ≡ ε-Idˢ A ϕ h
    lemma₁ A h h⊑A ϕ with Label-⊑-dec h A
    ... | .true because ofʸ p   = refl
    ... | .false because ofⁿ ¬p = ⊥-elim (¬p h⊑A)

  ε-⟼c-simᴸ : ∀ (A : Label) (l : Label) → (l⊑A : l ⊑ A)
                 {ϕ : SC.Idˢ} {τ : Ty}
                 {t : C.CTerm (MAC l τ)}
                 {M : SC.ξ-Mask}
                 {ev : SC.CEvent l}
                 {t' : C.CTerm (MAC l τ)}
               → [ ϕ         ] t      [ M ]⟼c[  ev             ] t'
               → [ ε-Idˢ A ϕ ] ε A t  [ M ]⟼c●[ εₜ-CEvent A ev ] ε A t'
  ε-⟼c-simᴸ A l l⊑A {ϕ = ϕ} (Fork {h = h} l⊑h)
    with Label-⊑-dec h A
  ... | .true because ofʸ p
      rewrite lemma₁ A h p ϕ = Fork l⊑h 
  ... | .false because ofⁿ ¬p = Fork● l⊑h 
  ε-⟼c-simᴸ A l l⊑A (ThrowTo₁ {h = h} e l⊑h s)
    with Label-⊑-dec l A
  ... | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ... | .true because ofʸ p  
    with Label-⊑-dec h A
  ... | .false because ofⁿ ¬p' = ThrowTo₁● e l⊑h (step-sim-ε-↦ A s)
  ... | .true because ofʸ p'   = ThrowTo₁  e l⊑h (step-sim-ε-↦ A s)
  ε-⟼c-simᴸ A l l⊑A (ThrowTo₂ {h = h} e l⊑h n)
    with Label-⊑-dec h A
  ε-⟼c-simᴸ A l l⊑A {ϕ} (ThrowTo₂ {h = h} e l⊑h n) | .false because ofⁿ ¬p
    with Label-⊑-dec h A
  ... | .false because ofⁿ ¬p' = ThrowTo₂● e l⊑h
  ... | .true because ofʸ p'   = ⊥-elim (¬p p')
  ε-⟼c-simᴸ A l l⊑A {ϕ} (ThrowTo₂ {h = h} e l⊑h n) | .true because ofʸ p
    with Label-⊑-dec h A
  ... | .false because ofⁿ ¬p' = ⊥-elim (¬p' p)
  ... | .true because ofʸ p'   = ThrowTo₂ e l⊑h n

  ε-⟼e-simᴸ : ∀ (A : Label) (l : Label) → (l⊑A : l ⊑ A)
                 {ϕ : SC.Idˢ}
                 {τ : Ty}
                 {Σ : SC.Store}
                 {t  : C.CTerm (MAC l τ)}
                 {M : ξ-Mask}
                 {exs  : ξ-List l}
                 {exs' : ξ-List l}
                 {ev : SC.CEvent l}
                 {Σ' : SC.Store}
                 {t' : C.CTerm (MAC l τ)}
               → [ ϕ         ] Σ      , t      [ M , exs                    ]⟼e[  exs' , ev                                ] Σ'     , t'
               → [ ε-Idˢ A ϕ ] εₛ A Σ , ε A t [ M , just (map (ε-ξ A) exs) ]⟼e●[ just (map (ε-ξ A) exs') , εₜ-CEvent A ev ] εₛ A Σ' , ε A t'
  ε-⟼e-simᴸ A l l⊑A (Mask s)
    with Label-⊑-dec l A
  ε-⟼e-simᴸ A l l⊑A {τ} {Σ} {M = M} {exs} {exs'} {ev} {Σ'} (Mask {e = e} {t₁} {t₂ = t₂} s) | .true because ofʸ p
    with ε-⟼e-simᴸ A l l⊑A s
  ... | s' rewrite ξ-mask-com {M = M} e = Mask s'
  ε-⟼e-simᴸ A l l⊑A (Mask s) | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ε-⟼e-simᴸ A l l⊑A (UnMask s)
    with Label-⊑-dec l A
  ε-⟼e-simᴸ A l l⊑A {τ} {M = M} {exs} {exs'} {ev} {Σ'} (UnMask {e = e} {t₁} {t₂ = t₂} s) | .true because ofʸ p
    with ε-⟼e-simᴸ A l l⊑A s
  ... | s' rewrite ξ-unmask-com {M = M} e = UnMask s' 
  ε-⟼e-simᴸ A l l⊑A (UnMask s) | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ε-⟼e-simᴸ A l l⊑A (Bind s)  with Label-⊑-dec l A
  ε-⟼e-simᴸ A l l⊑A {τ₂} {Σ} {M = M} {exs} {exs'} {ev} {Σ'} (Bind {τ₁ = τ₁} {t₁ = t₁} {u} {t₂ = t₂} s) | .true because ofʸ p
    with ε-⟼e-simᴸ A l l⊑A s
  ... | s' = Bind s'
  ε-⟼e-simᴸ A l l⊑A (Bind s) | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ε-⟼e-simᴸ A l l⊑A (Catch s)
    with Label-⊑-dec l A
  ε-⟼e-simᴸ A l l⊑A {τ} {Σ} {M = M} {exs} {exs'} {ev} (Catch {hs = hs} {t₁} {t₂ = t₂} s) | .true because ofʸ p
    with ε-⟼e-simᴸ A l l⊑A s
  ... | s' = Catch s'
  ε-⟼e-simᴸ A l l⊑A (Catch s) | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ε-⟼e-simᴸ A l l⊑A (Catchχ₁ x)
    with Label-⊑-dec l A
  ε-⟼e-simᴸ A l l⊑A (Catchχ₁ {hs = hs} x) | .true because ofʸ p = Catchχ₁ (first-≡-nothing A l _ hs x)
  ε-⟼e-simᴸ A l l⊑A (Catchχ₁ x) | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ε-⟼e-simᴸ A l l⊑A (Catchχ₂ x)
    with Label-⊑-dec l A
  ε-⟼e-simᴸ A l l⊑A (Catchχ₂ {hs = hs} x) | .true because ofʸ p
    with first-≡-just A l _ hs x
  ... | r with Label-⊑-dec l A
  ε-⟼e-simᴸ A l l⊑A (Catchχ₂ {hs = hs} x) | .true because ofʸ p | r | .true because ofʸ p₁  = Catchχ₂ r
  ε-⟼e-simᴸ A l l⊑A (Catchχ₂ {hs = hs} x) | .true because ofʸ p | r | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ε-⟼e-simᴸ A l l⊑A (Catchχ₂ x) | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ε-⟼e-simᴸ A l l⊑A (LiftM↯ is↯ s)
    with Label-⊑-dec l A
  ε-⟼e-simᴸ A l l⊑A (LiftM↯ is↯ s) | .true because ofʸ p
    with ε-⟼m-simᴸ A l l⊑A s
  ... | s' = LiftM↯ (ε-Is-↯-⇒ A l p _ is↯) s'
  ε-⟼e-simᴸ A l l⊑A (LiftM↯ is↯ s) | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ε-⟼e-simᴸ A l l⊑A (LiftM◍ ¬is↯ x s)
    with Label-⊑-dec l A
  ε-⟼e-simᴸ A l l⊑A (LiftM◍ {M = M} {exs = exs} ¬is↯ x s) | .true because ofʸ p
    with ε-⟼m-simᴸ A l l⊑A s
  ... | s' = LiftM◍ (ε-¬-Is-↯-⇒ A l p _ ¬is↯) (ξ-L-get-unmasked-nothingᴸ A l p exs M x) s'
  ε-⟼e-simᴸ A l l⊑A (LiftM◍ ¬is↯ x s) | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ε-⟼e-simᴸ A l l⊑A (LiftM○↯ ¬is↯ x s)
    with Label-⊑-dec l A
  ε-⟼e-simᴸ A l l⊑A (LiftM○↯ {M = M} {exs = exs} ¬is↯ x s) | .true because ofʸ p
    with ε-⟼m-simᴸ A l l⊑A s
  ... | s' = LiftM○↯ (ε-¬-Is-↯-⇒ A l p _ ¬is↯) (ξ-L-get-unmasked-justᴸ A l p exs M x) s'
  ε-⟼e-simᴸ A l l⊑A (LiftM○↯ ¬is↯ x s) | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ε-⟼e-simᴸ A l l⊑A (LiftC◍ x s)
    with Label-⊑-dec l A
  ε-⟼e-simᴸ A l l⊑A (LiftC◍ {M = M} {exs = exs} x s) | .true because ofʸ p
    with ε-⟼c-simᴸ A l l⊑A s
  ... | s' = LiftC◍ (ξ-L-get-unmasked-nothingᴸ A l p exs M x) s'
  ε-⟼e-simᴸ A l l⊑A (LiftC◍ x s) | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ε-⟼e-simᴸ A l l⊑A (LiftC○↯ x s)
    with Label-⊑-dec l A
  ε-⟼e-simᴸ A l l⊑A (LiftC○↯ {M = M} {exs = exs} x s) | .true because ofʸ p
    with ε-⟼c-simᴸ A l l⊑A s
  ... | s' = LiftC○↯ (ξ-L-get-unmasked-justᴸ A l p exs M x) s'
  ε-⟼e-simᴸ A l l⊑A (LiftC○↯ x s) | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ε-⟼e-simᴸ A l l⊑A (LiftP○ x s)
    with Label-⊑-dec l A
  ε-⟼e-simᴸ A l l⊑A (LiftP○ {M = M} {exs = exs} x s) | .true because ofʸ p
    with step-sim-ε-↦ A s
  ... | s' = LiftP○ (ξ-L-get-unmasked-justᴸ A l p exs M x) s'
  ε-⟼e-simᴸ A l l⊑A (LiftP○ x s) | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ε-⟼e-simᴸ A l l⊑A (LiftP◍ x s)
    with Label-⊑-dec l A
  ε-⟼e-simᴸ A l l⊑A (LiftP◍ {M = M} {exs = exs} x s) | .true because ofʸ p
    with step-sim-ε-↦ A s
  ... | s' = LiftP◍ (ξ-L-get-unmasked-nothingᴸ A l p exs M x) s' 
  ε-⟼e-simᴸ A l l⊑A (LiftP◍ x s) | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ε-⟼e-simᴸ A l l⊑A (LiftS◍ x s)
    with Label-⊑-dec l A
  ε-⟼e-simᴸ A l l⊑A (LiftS◍ {M = M} {exs = exs} x s) | .true because ofʸ p
    with ε-⟼s-simᴸ A l l⊑A s
  ... | s' with Label-⊑-dec l A
  ε-⟼e-simᴸ A l l⊑A (LiftS◍ {M = M} {exs = exs} x s) | .true because ofʸ p | s' | .true because ofʸ p₁
    = LiftS◍ (ξ-L-get-unmasked-nothingᴸ A l p exs M x) s'
  ε-⟼e-simᴸ A l l⊑A (LiftS◍ x s) | .true because ofʸ p | s' | .false because ofⁿ ¬p = ⊥-elim (¬p p)
  ε-⟼e-simᴸ A l l⊑A (LiftS◍ x s) | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ε-⟼e-simᴸ A l l⊑A (LiftS○ x s)
    with Label-⊑-dec l A
  ε-⟼e-simᴸ A l l⊑A (LiftS○ {M = M} {exs = exs} x s) | .true because ofʸ p
    with ε-⟼s-simᴸ A l l⊑A s
  ... | s' = LiftS○ (ξ-L-get-unmasked-justᴸ A l p exs M x) s'
  ε-⟼e-simᴸ A l l⊑A (LiftS○ x s) | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ε-⟼e-simᴸ A l l⊑A (LiftS◍ₛ p₁ p₂ s)
    with Label-⊑-dec l A
  ε-⟼e-simᴸ A l l⊑A (LiftS◍ₛ {M = M} {exs = exs} p₁ p₂ s) | .true because ofʸ p
    with ε-⟼s-simᴸ A l l⊑A s
  ... | s' = LiftS◍ₛ (ξ-L-get-unmasked-nothingᴸ A l p exs M p₁) (ξ-L-get-justᴸ A l p exs p₂) s'
  ε-⟼e-simᴸ A l l⊑A (LiftS◍ₛ p₁ p₂ s) | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)
  ε-⟼e-simᴸ A l l⊑A (LiftSₛ s)
    with Label-⊑-dec l A
  ε-⟼e-simᴸ A l l⊑A (LiftSₛ s) | .true because ofʸ p
    with ε-⟼s-simᴸ A l l⊑A s
  ... | s' = LiftSₛ s'
  ε-⟼e-simᴸ A l l⊑A (LiftSₛ s) | .false because ofⁿ ¬p = ⊥-elim (¬p l⊑A)

  ε-⟼e-simᴴ : ∀ (A : Label) (l : Label) → (l⋤A : ¬ (l ⊑ A))
                 {ϕ : SC.Idˢ} {τ : Ty}
                 {Σ : SC.Store}
                 {t  : C.CTerm (MAC l τ)}
                 {M : ξ-Mask}
                 {exs : ξ-List l}
                 {exs' : ξ-List l}
                 {ev : SC.CEvent l}
                 {Σ' : SC.Store}
                 {t' : C.CTerm (MAC l τ)}
               → [ ϕ ] Σ , t [ M , exs ]⟼e[ exs' , ev ] Σ' , t'
               → εₛ A Σ ≡ εₛ A Σ'
  ε-⟼e-simᴴ A l l⋤A (Mask s) = ε-⟼e-simᴴ A l l⋤A s
  ε-⟼e-simᴴ A l l⋤A (UnMask s) = ε-⟼e-simᴴ A l l⋤A s
  ε-⟼e-simᴴ A l l⋤A (Bind s) = ε-⟼e-simᴴ A l l⋤A s
  ε-⟼e-simᴴ A l l⋤A (Catch s) = ε-⟼e-simᴴ A l l⋤A s
  ε-⟼e-simᴴ A l l⋤A (Catchχ₁ e∉hs) = refl
  ε-⟼e-simᴴ A l l⋤A (Catchχ₂ e,u∈hs) = refl
  ε-⟼e-simᴴ A l l⋤A (LiftM↯ is↯ s) = refl
  ε-⟼e-simᴴ A l l⋤A (LiftM◍ ¬is↯ x s) = refl
  ε-⟼e-simᴴ A l l⋤A (LiftM○↯ ¬is↯ x s) = refl
  ε-⟼e-simᴴ A l l⋤A (LiftC◍ x s) = refl
  ε-⟼e-simᴴ A l l⋤A (LiftC○↯ x s) = refl
  ε-⟼e-simᴴ A l l⋤A (LiftP○ x s) = refl
  ε-⟼e-simᴴ A l l⋤A (LiftP◍ x s) = refl
  ε-⟼e-simᴴ A l l⋤A (LiftS◍ x s) = ε-⟼s-simᴴ A l l⋤A s
  ε-⟼e-simᴴ A l l⋤A (LiftS○ x s) = refl
  ε-⟼e-simᴴ A l l⋤A (LiftS◍ₛ x x₁ s) = refl
  ε-⟼e-simᴴ A l l⋤A (LiftSₛ s) = refl
    
  ⟼-Value⇒⟼-Value : ∀ (A : Label) (l : Label) (t : C.CTerm (MAC l （）))
                      → S.⟼-Value t → Sˢ.⟼-Value (ε A t)
  ⟼-Value⇒⟼-Value A l .(χ l e) (χ .l e) = χ l e
  ⟼-Value⇒⟼-Value A l .(return l t) (return .l t) = return l (ε A t)
