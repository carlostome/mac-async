module Sequential.Calculus where

  open import Calculus.Base

  open import Exception
  open import Data.Bool       using (Bool; true; false; not)
  open import Data.List       using (List; _∷_; []; _++_; [_]; length)
  open import Data.List.Extra
  open import Data.Maybe      using (Maybe; just; nothing)
  open import Data.Nat        using (ℕ)
  open import Data.Product    using (∃; _,_; _×_; Σ) renaming (proj₁ to fst)
  open import Data.Unit       using (⊤; tt)
  open import Data.Empty      using (⊥)
  open import Function        using (_∘_)

  open import Relation.Nullary 
  open import Relation.Binary.PropositionalEquality using (refl; _≡_)

  -------------------------------------------------------------------------------- 
  --                               Exceptions
  --------------------------------------------------------------------------------

  -- a mask is a bitvector, with true at an exception
  -- when is masked
  ξ-Mask : Set
  ξ-Mask = ξ → Bool

  -- mask an exception
  ξ-mask : ξ-Mask → ξ → ξ-Mask
  ξ-mask M e₁ e₂ with ξ-≡-dec e₁ e₂
  ... | yes p = true
  ... | no ¬p = M e₂

  -- unmask an exception
  ξ-unmask : ξ-Mask → ξ → ξ-Mask
  ξ-unmask M e₁ e₂ with ξ-≡-dec e₁ e₂
  ... | yes p = false
  ... | no ¬p = M e₂

  -- a labeled list of labeled exceptions (all flow to l)
  ξ-List : Label → Set
  ξ-List l = List (ξ × Σ Label (_⊑ l))

  -- add a new exception to the list
  ξ-L-add   : ∀ {l : Label} → ξ-List l → ξ → (l' : Label) → (l'⊑l : l' ⊑ l) → ξ-List l
  ξ-L-add   exs e l' l'⊑l = exs ++ [ e , l' , l'⊑l ] 

  -- get the first unmasked exception
  ξ-L-get-unmasked : ∀ {l : Label} → ξ-List l → ξ-Mask → Maybe ((ξ × Σ Label (_⊑ l)) × ξ-List l)
  ξ-L-get-unmasked exs m = first (not ∘ m ∘ fst) exs

  -- get the first exception
  ξ-L-get : ∀ {l : Label} → ξ-List l → Maybe ((ξ × Σ Label (_⊑ l)) × ξ-List l)
  ξ-L-get []       = nothing
  ξ-L-get (x ∷ xs) = just (x , xs)

  -------------------------------------------------------------------------------- 
  --                        Concurrent  Events
  --------------------------------------------------------------------------------

  -- types concurrent events
  data CEvent (l : Label) : Set where
    ∅       : CEvent l
    fork    : ∀ (h : Label) → (l⊑h : l ⊑ h) (t : CTerm (MAC h （）)) → ξ-Mask → CEvent l
    throw   : ∀ (e : ξ) (h : Label) → (l⊑h : l ⊑ h) → ℕ → CEvent l
    stuck   : CEvent l

  -------------------------------------------------------------------------------- 
  --                               Memory
  --------------------------------------------------------------------------------

  -- cells in memory, either empty or contain a term
  data Cell (τ : Ty) : Set where
    ⊗   : Cell τ
    ⟦_⟧ : CTerm τ -> Cell τ

  Mem : Set
  Mem = List (∃ Cell)

  -- a store is a label-partitioned memory of cells
  Store : Set
  Store = (l : Label) → Mem

  newˢ : Store → Label → Ty → Store
  newˢ Σ l τ l' with Label-≡-dec l l'
  newˢ Σ l τ l' | .true because ofʸ p    = Σ l ++ [ τ , ⊗ ]
  newˢ Σ l τ l' | .false because ofⁿ ¬p  = Σ l'

  _[_↦_]ˢ : Store → (l : Label) → Mem → Store
  _[_↦_]ˢ Σ l mem l' with Label-≡-dec l l'
  ... | yes refl = mem
  ... | no ¬p    = Σ l'
  
  -- Sytactic sugar for Lookupℕ in a Pool
  _↦_∈ˢ_ : ℕ -> ∃ Cell -> Mem -> Set
  n ↦ c ∈ˢ m = Lookupℕ c n m

  -- -- Syntatic sugar for updating a Pool.
  _≔_[_↦_]ˢ : Mem → Mem → ℕ → ∃ Cell → Set
  m₂ ≔ m₁ [ n ↦ c ]ˢ = Updateℕ c n m₁ m₂

  -- size of a partition of the store
  sizeˢ : Store → (l : Label) → ℕ
  sizeˢ Σ l = length (Σ l)

  -- source of identifies
  Idˢ : Set
  Idˢ = Label → ℕ

  -------------------------------------------------------------------------------- 
  --                        Sync. Variable  Events
  --------------------------------------------------------------------------------

  -- sync variable events
  data SEvent (l : Label) : Set where
    stuck : SEvent l
    step  : SEvent l

  
  --------------------------------------------------------------------------------  
  -- lemmas

  ξ-L-get⇏nothing : ∀ {l : Label} {exs : ξ-List l} → ξ-L-get exs ≡ nothing → exs ≡ []
  ξ-L-get⇏nothing {l} {[]} _ = refl
