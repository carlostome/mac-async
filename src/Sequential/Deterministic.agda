module Sequential.Deterministic where

  open import Calculus.Base
  open import Sequential.Calculus
  open import Sequential.Semantics
  open import Sequential.Lemmas

  open import Data.Bool           using (Bool; true; false)
  open import Data.Empty          using (⊥-elim)
  open import Data.List           using (List; _∷_; [])
  open import Data.List.Extra     
  open import Data.Product        using (Σ; _,_; -,_; ∃; ∃₂; _×_)
  open import Data.Product.Extra
  
  open import Relation.Binary.PropositionalEquality using (_≡_; refl; cong; cong₂; trans; sym) renaming (subst to ≡-subst)
  open import Relation.Nullary                      using (¬_)

  -- Small-step pure semantics is deterministic
  ↦-det : ∀ {τ} {t : CTerm τ} {t₁ t₂ : CTerm τ}
        → (d₁ : t ↦ t₁) → (d₂ : t ↦ t₂) → t₁ ≡ t₂
  ↦-det (App d₁) (App d₂) = cong₂ _∙_ (↦-det d₁ d₂) refl
  ↦-det Beta Beta = refl
  ↦-det (If₁ d₁) (If₁ d₂) = cong (λ x → if x then _ else _) (↦-det d₁ d₂)
  ↦-det If₂ If₂ = refl
  ↦-det If₃ If₃ = refl

  -- Small-step monadic semantics is deterministic
  ⟼m-det : ∀ {l} {τ}
          → {t : CTerm (MAC l τ)} {t₁ t₂ : CTerm (MAC l τ)}
          → (d₁ : t ⟼m t₁)
          → (d₂ : t ⟼m t₂)
          → t₁ ≡ t₂
  ⟼m-det Mask Mask   = refl 
  ⟼m-det Maskχ Maskχ = refl
  ⟼m-det Bind Bind   = refl
  ⟼m-det Bindχ Bindχ = refl
  ⟼m-det (Label' l⊑h) (Label' .l⊑h) = refl
  ⟼m-det UnMask UnMask   = refl
  ⟼m-det UnMaskχ UnMaskχ = refl
  ⟼m-det (Unlabel₁ l⊑h d₁) (Unlabel₁ .l⊑h d₂) = cong (unlabel l⊑h) (↦-det d₁ d₂)
  ⟼m-det (Unlabel₂ l⊑h) (Unlabel₂ .l⊑h)       = refl
  ⟼m-det Catch Catch   = refl

  -- Small-step concurrent semantics is deterministic
  ⟼c-det : ∀ {l} {τ} {ϕ}
          → {t : CTerm (MAC l τ)}
            {M : ξ-Mask}
            {ev₁ ev₂ : CEvent l}
            {t₁ : CTerm (MAC l τ)}
            {t₂ : CTerm (MAC l τ)}
          → (d₁ : [ ϕ ] t [ M ]⟼c[ ev₁ ] t₁)
          → (d₂ : [ ϕ ] t [ M ]⟼c[ ev₂ ] t₂)
          → ev₁ ≡ ev₂ × t₁ ≡ t₂
  ⟼c-det (Fork l⊑h) (Fork .l⊑h)                    = refl , refl
  ⟼c-det (ThrowTo₁ e l⊑h s₁) (ThrowTo₁ .e .l⊑h s₂) = refl , (cong (throwTo e l⊑h) (↦-det s₁ s₂))
  ⟼c-det (ThrowTo₂ e l⊑h n) (ThrowTo₂ .e .l⊑h .n)  = refl , refl

  -- Small-step syncronization event is deterministic
  ⟼s-det : ∀ {l} {τ}
              {Σ : Store}
              {t : CTerm (MAC l τ)}
              {sev₁ : SEvent l} {t₁ : CTerm (MAC l τ)} {Σ₁ : Store}
              {sev₂ : SEvent l} {t₂ : CTerm (MAC l τ)} {Σ₂ : Store}
            → (d₁ : Σ , t ⟼s[ sev₁ ] Σ₁ , t₁)
            → (d₂ : Σ , t ⟼s[ sev₂ ] Σ₂ , t₂)
            → sev₁ ≡ sev₂ × Σ₁ ≡ Σ₂ × t₁ ≡ t₂
  ⟼s-det (NewMVar l⊑h) (NewMVar .l⊑h) = refl , refl , refl
  ⟼s-det (TakeMVar₁ d) (TakeMVar₁ d₁) = refl , refl , cong takeMVar (↦-det d d₁)
  ⟼s-det (TakeMVar₂ p₁ p₂) (TakeMVar₂ p₃ p₄)
    with Lookupℕ-inj p₁ p₃ | Updateℕ-inj p₂ p₄
  ... | refl | refl = refl , refl , refl
  ⟼s-det (TakeMVar₂ p₁ p₂) (TakeMVarₛ p)
    with Lookupℕ-inj p p₁
  ... | ()
  ⟼s-det (TakeMVarₛ p) (TakeMVar₂ p₁ p₂)
    with Lookupℕ-inj p p₁
  ... | ()
  ⟼s-det (TakeMVarₛ p) (TakeMVarₛ p₁) = refl , refl , refl
  ⟼s-det (PutMVar₁ d) (PutMVar₁ d₁)   = refl , refl , cong (λ t → putMVar t _) (↦-det d d₁)
  ⟼s-det (PutMVar₂ p₁ p₂) (PutMVar₂ p₃ p₄)
    with Lookupℕ-inj p₁ p₃ | Updateℕ-inj p₂ p₄
  ... | refl | refl = refl , refl , refl
  ⟼s-det (PutMVar₂ p₁ p₂) (PutMVarₛ p)
    with Lookupℕ-inj p p₁
  ... | ()
  ⟼s-det (PutMVarₛ p) (PutMVar₂ p₁ p₂)
    with Lookupℕ-inj p p₁
  ... | ()
  ⟼s-det (PutMVarₛ p) (PutMVarₛ p₁) = refl , refl , refl

  -- Small-step exceptional semantics is deterministic
  ⟼e-det : ∀ {l} {τ} {ϕ}
              {Σ : Store} {t : CTerm (MAC l τ)} {M : ξ-Mask} {exs : ξ-List l}
              {exs₁ : ξ-List l} {ev₁ : CEvent l} {t₁ : CTerm (MAC l τ)} {Σ₁ : Store}
              {exs₂ : ξ-List l} {ev₂ : CEvent l} {t₂ : CTerm (MAC l τ)} {Σ₂ : Store}
            → (d₁ : [ ϕ ] Σ , t [ M , exs ]⟼e[ exs₁ , ev₁ ] Σ₁ , t₁)
            → (d₂ : [ ϕ ] Σ , t [ M , exs ]⟼e[ exs₂ , ev₂ ] Σ₂ , t₂)
            → exs₁ ≡ exs₂ × ev₁ ≡ ev₂ × Σ₁ ≡ Σ₂ × t₁ ≡ t₂
  ⟼e-det (Mask d₁) (Mask d₂) with ⟼e-det d₁ d₂
  ... | (refl , refl , refl , refl) = refl , refl , refl , refl
  ⟼e-det (Mask d₁) (LiftM↯ is↯ Maskχ)        = ⊥-elim (χ¬⟼e d₁)
  ⟼e-det (Mask d₁) (LiftM◍ ¬is↯ x Mask)      = ⊥-elim (return¬⟼e d₁)
  ⟼e-det (Mask d₁) (LiftM◍ ¬is↯ x Maskχ)     = ⊥-elim (χ¬⟼e d₁)
  ⟼e-det (Mask d₁) (LiftM○↯ ¬is↯ x Mask)     = ⊥-elim (return¬⟼e d₁)
  ⟼e-det (Mask d₁) (LiftM○↯ ¬is↯ x Maskχ)    = ⊥-elim (χ¬⟼e d₁)
  ⟼e-det (UnMask d₁) (UnMask d₂) with ⟼e-det d₁ d₂
  ... | (refl , refl , refl , refl) = refl , refl , refl , refl
  ⟼e-det (UnMask d₁) (LiftM↯ is↯ UnMaskχ)     = ⊥-elim (χ¬⟼e d₁)
  ⟼e-det (UnMask d₁) (LiftM◍ ¬is↯ x UnMask)   = ⊥-elim (return¬⟼e d₁)
  ⟼e-det (UnMask d₁) (LiftM◍ ¬is↯ x UnMaskχ)  = ⊥-elim (χ¬⟼e d₁)
  ⟼e-det (UnMask d₁) (LiftM○↯ ¬is↯ x UnMask)  = ⊥-elim (return¬⟼e d₁)
  ⟼e-det (UnMask d₁) (LiftM○↯ ¬is↯ x UnMaskχ) = ⊥-elim (χ¬⟼e d₁)
  ⟼e-det (Bind d₁) (Bind d₂) with ⟼e-det d₁ d₂
  ... | (refl , refl , refl , refl) = refl , refl , refl , refl
  ⟼e-det (Bind d₁) (LiftM↯ is↯ Bindχ)     = ⊥-elim (χ¬⟼e d₁)
  ⟼e-det (Bind d₁) (LiftM◍ ¬is↯ x Bind)   = ⊥-elim (return¬⟼e d₁)
  ⟼e-det (Bind d₁) (LiftM◍ ¬is↯ x Bindχ)  = ⊥-elim (χ¬⟼e d₁)     
  ⟼e-det (Bind d₁) (LiftM○↯ ¬is↯ x Bind)  = ⊥-elim (return¬⟼e d₁)
  ⟼e-det (Bind d₁) (LiftM○↯ ¬is↯ x Bindχ) = ⊥-elim (χ¬⟼e d₁)     
  ⟼e-det (Catch d₁) (Catch d₂) with ⟼e-det d₁ d₂
  ... | (refl , refl , refl , refl) = refl , refl , refl , refl
  ⟼e-det (Catch d₁) (Catchχ₁ x₁) = ⊥-elim (χ¬⟼e d₁)
  ⟼e-det (Catch d₁) (Catchχ₂ _) = ⊥-elim (χ¬⟼e d₁)
  ⟼e-det (Catch d₁) (LiftM↯ is↯ Catch) = ⊥-elim (return¬⟼e d₁)
  ⟼e-det (Catch d₁) (LiftM◍ ¬is↯ x₁ Catch) = ⊥-elim (return¬⟼e d₁)
  ⟼e-det (Catch d₁) (LiftM○↯ ¬is↯ x₁ Catch) = ⊥-elim (return¬⟼e d₁)
  ⟼e-det (Catchχ₁ x₁) (Catch d₂) = ⊥-elim (χ¬⟼e d₂)
  ⟼e-det (Catchχ₁ x) (Catchχ₁ x₁) = refl , refl , refl , refl
  ⟼e-det (Catchχ₁ x) (Catchχ₂ x₁) with trans (sym x₁) x
  ... | ()
  ⟼e-det (Catchχ₁ x) (LiftM↯ is↯ (()))
  ⟼e-det (Catchχ₁ x) (LiftM◍ ¬is↯ x₁ (()))
  ⟼e-det (Catchχ₁ x) (LiftM○↯ ¬is↯ x₁ (()))
  ⟼e-det (Catchχ₂ x₁) (Catch d₂) = ⊥-elim (χ¬⟼e d₂)
  ⟼e-det (Catchχ₂ x₁) (Catchχ₁ x) with trans (sym x₁) x
  ... | ()
  ⟼e-det (Catchχ₂ x₁) (Catchχ₂ x₁') with trans (sym x₁) x₁'
  ... | refl = refl , refl , refl , refl
  ⟼e-det (Catchχ₂ x₁) (LiftM↯ is↯ (()))
  ⟼e-det (Catchχ₂ x₁) (LiftM◍ ¬is↯ x (()))
  ⟼e-det (Catchχ₂ x₁) (LiftM○↯ ¬is↯ x (()))
  ⟼e-det (LiftM↯ is↯ s) (LiftM↯ is↯₁ s₁) with ⟼m-det s s₁
  ... | refl = refl , refl , refl , refl
  ⟼e-det (LiftM↯ is↯ s) (LiftM◍ ¬is↯ x s₁) with ⟼m-det s s₁
  ... | refl = ⊥-elim (¬is↯ is↯)
  ⟼e-det (LiftM↯ is↯ s) (LiftM○↯ ¬is↯ x s₁) with ⟼m-det s s₁
  ... | refl = ⊥-elim (¬is↯ is↯)
  ⟼e-det (LiftM↯ is↯ Catch) (Catch d₂)    = ⊥-elim (return¬⟼e d₂)
  ⟼e-det (LiftM↯ is↯ s) (LiftC◍ x s₁) = ⊥-elim (⟼c∨⟼m-excl ((-, s) , (-,₄ s₁)))
  ⟼e-det (LiftM↯ is↯ s) (LiftC○↯ x s₁) = ⊥-elim (⟼c∨⟼m-excl ((-, s) , (-,₄ s₁)))
  ⟼e-det (LiftM↯ is↯ Maskχ) (Mask d₂)     = ⊥-elim (χ¬⟼e d₂)
  ⟼e-det (LiftM↯ is↯ UnMaskχ) (UnMask d₂) = ⊥-elim (χ¬⟼e d₂)
  ⟼e-det (LiftM↯ is↯ Bindχ) (Bind d₂)     = ⊥-elim (χ¬⟼e d₂)
  ⟼e-det (LiftM↯ is↯ (())) (Catchχ₁ x)
  ⟼e-det (LiftM↯ is↯ (())) (Catchχ₂ _)
  ⟼e-det (LiftM↯ is↯ s) (LiftP○ x s₁) = ⊥-elim (⟼m∨↦-excl ((-, s) , (-, s₁)))
  ⟼e-det (LiftM↯ is↯ s) (LiftP◍ x s₁) = ⊥-elim (⟼m∨↦-excl ((-, s) , (-, s₁)))
  ⟼e-det (LiftM↯ is↯ s) (LiftS◍ x s₁) = ⊥-elim (⟼s∨⟼m-excl ((-, s) , (-, (-, -, s₁))))
  ⟼e-det (LiftM↯ is↯ s) (LiftS○ x s₁) = ⊥-elim (⟼s∨⟼m-excl ((-, s) , (-, (-, -, s₁))))
  ⟼e-det (LiftM↯ is↯ s) (LiftS◍ₛ x x₁ s₁) = ⊥-elim (⟼s∨⟼m-excl ((-, s) , (-, (-, -, s₁))))
  ⟼e-det (LiftM↯ is↯ s) (LiftSₛ s₁) = ⊥-elim (⟼s∨⟼m-excl ((-, s) , (-, (-, -, s₁))))
  ⟼e-det (LiftM◍ ¬is↯ x s) (LiftM↯ is↯ s₁) with ⟼m-det s s₁
  ... | refl = ⊥-elim (¬is↯ is↯)
  ⟼e-det (LiftM◍ ¬is↯ x s) (LiftM◍ ¬is↯₁ x₁ s₁) with ⟼m-det s s₁ | trans (sym x) x₁
  ... | refl | refl = refl , refl , refl , refl
  ⟼e-det (LiftM◍ ¬is↯ x s) (LiftM○↯ ¬is↯₁ x₁ s₁) with trans (sym x) x₁
  ... | ()
  ⟼e-det (LiftM◍ ¬is↯ x Mask) (Mask d₂)      = ⊥-elim (return¬⟼e d₂)
  ⟼e-det (LiftM◍ ¬is↯ x Maskχ) (Mask d₂)     = ⊥-elim (χ¬⟼e d₂)    
  ⟼e-det (LiftM◍ ¬is↯ x UnMask) (UnMask d₂)  = ⊥-elim (return¬⟼e d₂)
  ⟼e-det (LiftM◍ ¬is↯ x UnMaskχ) (UnMask d₂) = ⊥-elim (χ¬⟼e d₂)    
  ⟼e-det (LiftM◍ ¬is↯ x Bind) (Bind d₂)      = ⊥-elim (return¬⟼e d₂)
  ⟼e-det (LiftM◍ ¬is↯ x Bindχ) (Bind d₂)     = ⊥-elim (χ¬⟼e d₂)
  ⟼e-det (LiftM◍ ¬is↯ x₁ Catch) (Catch d₂) = ⊥-elim (return¬⟼e d₂)
  ⟼e-det (LiftM◍ ¬is↯ x (())) (Catchχ₁ x₁)
  ⟼e-det (LiftM◍ ¬is↯ x (())) (Catchχ₂  _)
  ⟼e-det (LiftM◍ ¬is↯ x s) (LiftC◍ x₁ s₁) = ⊥-elim (⟼c∨⟼m-excl ((-, s) , (-,₄ s₁)))
  ⟼e-det (LiftM◍ ¬is↯ x s) (LiftC○↯ x₁ s₁) = ⊥-elim (⟼c∨⟼m-excl ((-, s) , (-,₄ s₁)))
  ⟼e-det (LiftM◍ ¬is↯ x s) (LiftP○ x₁ s₁) = ⊥-elim (⟼m∨↦-excl ((-, s) , (-, s₁)))
  ⟼e-det (LiftM◍ ¬is↯ x s) (LiftP◍ x₁ s₁) = ⊥-elim (⟼m∨↦-excl ((-, s) , (-, s₁)))
  ⟼e-det (LiftM◍ ¬is↯ x s) (LiftS◍ x₁ s₁) = ⊥-elim (⟼s∨⟼m-excl ((-, s) , (-, (-, -, s₁))))
  ⟼e-det (LiftM◍ ¬is↯ x s) (LiftS○ x₁ s₁) = ⊥-elim (⟼s∨⟼m-excl ((-, s) , (-, (-, -, s₁))))
  ⟼e-det (LiftM◍ ¬is↯ x s) (LiftS◍ₛ x₁ x₂ s₁) = ⊥-elim (⟼s∨⟼m-excl ((-, s) , (-, (-, -, s₁))))
  ⟼e-det (LiftM◍ ¬is↯ x s) (LiftSₛ s₁) = ⊥-elim (⟼s∨⟼m-excl ((-, s) , (-, (-, -, s₁))))
  ⟼e-det (LiftM○↯ ¬is↯ x s) (LiftM↯ is↯ s₁) with ⟼m-det s s₁
  ⟼e-det (LiftM○↯ ¬is↯ x s) (LiftM↯ is↯ s₁) | refl = ⊥-elim (¬is↯ is↯)
  ⟼e-det (LiftM○↯ ¬is↯ x s) (LiftM◍ ¬is↯₁ x₁ s₁) with trans (sym x) x₁
  ... | ()
  ⟼e-det (LiftM○↯ ¬is↯ x s) (LiftM○↯ ¬is↯₁ x₁ s₁) with ⟼m-det s s₁ | trans (sym x) x₁
  ... |  refl | refl = refl , refl , refl , refl
  ⟼e-det (LiftM○↯ ¬is↯ x s) (LiftC◍ x₁ s₁) = ⊥-elim (⟼c∨⟼m-excl ((-, s) , (-,₄ s₁)))
  ⟼e-det (LiftM○↯ ¬is↯ x s) (LiftC○↯ x₁ s₁) = ⊥-elim (⟼c∨⟼m-excl ((-, s) , (-,₄ s₁)))
  ⟼e-det (LiftM○↯ ¬is↯ x Mask) (Mask d₂)      = ⊥-elim (return¬⟼e d₂)
  ⟼e-det (LiftM○↯ ¬is↯ x Maskχ) (Mask d₂)     = ⊥-elim (χ¬⟼e d₂)
  ⟼e-det (LiftM○↯ ¬is↯ x UnMask) (UnMask d₂)  = ⊥-elim (return¬⟼e d₂)
  ⟼e-det (LiftM○↯ ¬is↯ x UnMaskχ) (UnMask d₂) = ⊥-elim (χ¬⟼e d₂)
  ⟼e-det (LiftM○↯ ¬is↯ x Bind) (Bind d₂) = ⊥-elim (return¬⟼e d₂)
  ⟼e-det (LiftM○↯ ¬is↯ x Bindχ) (Bind d₂) = ⊥-elim (χ¬⟼e d₂)
  ⟼e-det (LiftM○↯ ¬is↯ x₁ Catch) (Catch d₂) = ⊥-elim (return¬⟼e d₂)
  ⟼e-det (LiftM○↯ ¬is↯ x (())) (Catchχ₁ x₁)
  ⟼e-det (LiftM○↯ ¬is↯ x (())) (Catchχ₂ _)
  ⟼e-det (LiftM○↯ ¬is↯ x s) (LiftP○ x₁ s₁) = ⊥-elim (⟼m∨↦-excl ((-, s) , (-, s₁)))
  ⟼e-det (LiftM○↯ ¬is↯ x s) (LiftP◍ x₁ s₁) = ⊥-elim (⟼m∨↦-excl ((-, s) , (-, s₁)))
  ⟼e-det (LiftM○↯ ¬is↯ x s) (LiftS◍ x₁ s₁) with trans (sym x) x₁
  ... | ()
  ⟼e-det (LiftM○↯ ¬is↯ x s) (LiftS○ x₁ s₁) = ⊥-elim (⟼s∨⟼m-excl ((-, s) , (-, (-, -, s₁))))
  ⟼e-det (LiftM○↯ ¬is↯ x s) (LiftS◍ₛ x₁ x₂ s₁) = ⊥-elim (⟼s∨⟼m-excl ((-, s) , (-, (-, -, s₁))))
  ⟼e-det (LiftC◍ x s) (LiftM↯ is↯ s₁) = ⊥-elim (⟼c∨⟼m-excl ((-, s₁) , (-,₄ s)))
  ⟼e-det (LiftC◍ x s) (LiftM◍ ¬is↯ x₁ s₁) = ⊥-elim (⟼c∨⟼m-excl ((-, s₁) , (-,₄ s)))
  ⟼e-det (LiftC◍ x s) (LiftM○↯ ¬is↯ x₁ s₁) = ⊥-elim (⟼c∨⟼m-excl ((-, s₁) , (-,₄ s)))
  ⟼e-det (LiftC◍ x s) (LiftC◍ x₁ s₁) with ⟼c-det s s₁ | trans (sym x) x₁
  ... | refl , refl | refl = refl , refl , refl , refl
  ⟼e-det (LiftC◍ x s) (LiftC○↯ x₁ s₁) with trans (sym x) x₁
  ... | ()
  ⟼e-det (LiftC◍ x s) (LiftP○ x₁ s₁) = ⊥-elim (⟼c∨↦-excl ((-,₄ s) , (-, s₁)))
  ⟼e-det (LiftC◍ x s) (LiftP◍ x₁ s₁) = ⊥-elim (⟼c∨↦-excl ((-,₄ s) , (-, s₁)))
  ⟼e-det (LiftC◍ x s) (LiftS◍ x₁ s₁) = ⊥-elim (⟼c∨⟼s-excl ((-,₃ s₁) , (-,₄ s)))
  ⟼e-det (LiftC◍ x s) (LiftS○ x₁ s₁) = ⊥-elim (⟼c∨⟼s-excl ((-,₃ s₁) , (-,₄ s)))
  ⟼e-det (LiftC◍ x s) (LiftS◍ₛ x₁ x₂ s₁) = ⊥-elim (⟼c∨⟼s-excl ((-,₃ s₁) , (-,₄ s)))
  ⟼e-det (LiftC◍ x s) (LiftSₛ s₁) = ⊥-elim (⟼c∨⟼s-excl ((-,₃ s₁) , (-,₄ s)))
  ⟼e-det (LiftC○↯ x s) (LiftM↯ is↯ s₁) = ⊥-elim (⟼c∨⟼m-excl ((-, s₁) , (-,₄ s)))
  ⟼e-det (LiftC○↯ x s) (LiftM◍ ¬is↯ x₁ s₁) = ⊥-elim (⟼c∨⟼m-excl ((-, s₁) , (-,₄ s)))
  ⟼e-det (LiftC○↯ x s) (LiftM○↯ ¬is↯ x₁ s₁) = ⊥-elim (⟼c∨⟼m-excl ((-, s₁) , (-,₄ s)))
  ⟼e-det (LiftC○↯ x s) (LiftC◍ x₁ s₁) with trans (sym x) x₁
  ... | ()
  ⟼e-det (LiftC○↯ x s) (LiftC○↯ x₁ s₁) with ⟼c-det s s₁ | trans (sym x) x₁
  ... | refl , refl | refl = refl , refl , refl , refl
  ⟼e-det (LiftC○↯ x s) (LiftP○ x₁ s₁) = ⊥-elim (⟼c∨↦-excl ((-,₄ s) , (-, s₁)))
  ⟼e-det (LiftC○↯ x s) (LiftP◍ x₁ s₁) = ⊥-elim (⟼c∨↦-excl ((-,₄ s) , (-, s₁)))
  ⟼e-det (LiftC○↯ x s) (LiftS◍ x₁ s₁) = ⊥-elim (⟼c∨⟼s-excl ((-,₃ s₁) , (-,₄ s)))
  ⟼e-det (LiftC○↯ x s) (LiftS○ x₁ s₁) = ⊥-elim (⟼c∨⟼s-excl ((-,₃ s₁) , (-,₄ s)))
  ⟼e-det (LiftC○↯ x s) (LiftS◍ₛ x₁ x₂ s₁) = ⊥-elim (⟼c∨⟼s-excl ((-,₃ s₁) , (-,₄ s)))
  ⟼e-det (LiftP○ x s) (LiftM↯ is↯ s₁) = ⊥-elim (⟼m∨↦-excl ((-, s₁) , (-, s)))
  ⟼e-det (LiftP○ x s) (LiftM◍ ¬is↯ x₁ s₁) = ⊥-elim (⟼m∨↦-excl ((-, s₁) , (-, s)))
  ⟼e-det (LiftP○ x s) (LiftM○↯ ¬is↯ x₁ s₁) = ⊥-elim (⟼m∨↦-excl ((-, s₁) , (-, s)))
  ⟼e-det (LiftP○ x s) (LiftC◍ x₁ s₁) = ⊥-elim (⟼c∨↦-excl ((-,₄ s₁) , (-, s)))
  ⟼e-det (LiftP○ x s) (LiftC○↯ x₁ s₁) = ⊥-elim (⟼c∨↦-excl ((-,₄ s₁) , (-, s)))
  ⟼e-det (LiftP○ x s) (LiftP○ x₁ s₁) with trans (sym x) x₁
  ... | refl = refl , refl , refl , refl
  ⟼e-det (LiftP○ x s) (LiftP◍ x₁ s₁) with trans (sym x) x₁
  ... | ()
  ⟼e-det (LiftP○ x s) (LiftS◍ x₁ s₁) = ⊥-elim (⟼s∨↦-excl ((-, -, -, s₁) , (-, s)))
  ⟼e-det (LiftP○ x s) (LiftS○ x₁ s₁) = ⊥-elim (⟼s∨↦-excl ((-, -, -, s₁) , (-, s)))
  ⟼e-det (LiftP○ x s) (LiftS◍ₛ x₁ x₂ s₁) = ⊥-elim (⟼s∨↦-excl ((-, -, -, s₁) , (-, s)))
  ⟼e-det (LiftP◍ x s) (LiftM↯ is↯ s₁) = ⊥-elim (⟼m∨↦-excl ((-, s₁) , (-, s)))
  ⟼e-det (LiftP◍ x s) (LiftM◍ ¬is↯ x₁ s₁) = ⊥-elim (⟼m∨↦-excl ((-, s₁) , (-, s)))
  ⟼e-det (LiftP◍ x s) (LiftM○↯ ¬is↯ x₁ s₁) = ⊥-elim (⟼m∨↦-excl ((-, s₁) , (-, s)))
  ⟼e-det (LiftP◍ x s) (LiftC◍ x₁ s₁) = ⊥-elim (⟼c∨↦-excl ((-,₄ s₁) , (-, s)))
  ⟼e-det (LiftP◍ x s) (LiftC○↯ x₁ s₁) = ⊥-elim (⟼c∨↦-excl ((-,₄ s₁) , (-, s)))
  ⟼e-det (LiftP◍ x s) (LiftP○ x₁ s₁) with trans (sym x) x₁
  ... | ()
  ⟼e-det (LiftP◍ x s) (LiftP◍ x₁ s₁) with ↦-det s s₁ | trans (sym x) x₁
  ... | refl | refl = refl , refl , refl , refl
  ⟼e-det (LiftP◍ x s) (LiftS◍ x₁ s₁) = ⊥-elim (⟼s∨↦-excl ((-, -, -,  s₁) , (-, s)))
  ⟼e-det (LiftP◍ x s) (LiftS○ x₁ s₁) = ⊥-elim (⟼s∨↦-excl ((-, -, -,  s₁) , (-, s)))
  ⟼e-det (LiftP◍ x s) (LiftS◍ₛ x₁ x₂ s₁) = ⊥-elim (⟼s∨↦-excl ((-, -, -,  s₁) , (-, s)))
  ⟼e-det (LiftP◍ x s) (LiftSₛ s₁) = ⊥-elim (⟼s∨↦-excl ((-, -, -,  s₁) , (-, s)))
  ⟼e-det (LiftS◍ x s) (LiftM↯ is↯ s₁) = ⊥-elim (⟼s∨⟼m-excl ((-, s₁) , (-, (-, -, s))))
  ⟼e-det (LiftS◍ x s) (LiftM◍ ¬is↯ x₁ s₁) = ⊥-elim (⟼s∨⟼m-excl ((-, s₁) , (-, (-, -, s))))
  ⟼e-det (LiftS◍ x s) (LiftM○↯ ¬is↯ x₁ s₁) = ⊥-elim (⟼s∨⟼m-excl ((-, s₁) , (-, (-, -, s))))
  ⟼e-det (LiftS◍ x s) (LiftC◍ x₁ s₁) = ⊥-elim (⟼c∨⟼s-excl ((-, -, -, s) , (-,₄ s₁)))
  ⟼e-det (LiftS◍ x s) (LiftC○↯ x₁ s₁) = ⊥-elim (⟼c∨⟼s-excl ((-, -, -, s) , (-,₄ s₁)))
  ⟼e-det (LiftS◍ x s) (LiftP○ x₁ s₁) = ⊥-elim (⟼s∨↦-excl ((-, -, -,  s) , (-, s₁)))
  ⟼e-det (LiftS◍ x s) (LiftP◍ x₁ s₁) = ⊥-elim (⟼s∨↦-excl ((-, -, -,  s) , (-, s₁)))
  ⟼e-det (LiftS◍ x s) (LiftS◍ x₁ s₁) with ⟼s-det s s₁ | trans (sym x) x₁
  ... | refl , refl , refl | refl = refl , refl , refl , refl
  ⟼e-det (LiftS◍ x s) (LiftS○ x₁ s₁) with trans (sym x) x₁
  ... | ()
  ⟼e-det (LiftS◍ x s) (LiftS◍ₛ x₁ x₂ s₁)
    with ⟼s-det s s₁
  ... | () 
  ⟼e-det (LiftS◍ x s) (LiftSₛ s₁)
    with ⟼s-det s s₁
  ... | () 
  ⟼e-det (LiftS○ x s) (LiftM↯ is↯ s₁) = ⊥-elim (⟼s∨⟼m-excl ((-, s₁) , (-, (-, -, s))))
  ⟼e-det (LiftS○ x s) (LiftM◍ ¬is↯ x₁ s₁) = ⊥-elim (⟼s∨⟼m-excl ((-, s₁) , (-, (-, -, s))))
  ⟼e-det (LiftS○ x s) (LiftM○↯ ¬is↯ x₁ s₁) = ⊥-elim (⟼s∨⟼m-excl ((-, s₁) , (-, (-, -, s))))
  ⟼e-det (LiftS○ x s) (LiftC◍ x₁ s₁) = ⊥-elim (⟼c∨⟼s-excl ((-, -, -, s) , (-,₄ s₁)))
  ⟼e-det (LiftS○ x s) (LiftC○↯ x₁ s₁) = ⊥-elim (⟼c∨⟼s-excl ((-, -, -, s) , (-,₄  s₁)))
  ⟼e-det (LiftS○ x s) (LiftP○ x₁ s₁) = ⊥-elim (⟼s∨↦-excl ((-, -, -, s) , (-, s₁)))
  ⟼e-det (LiftS○ x s) (LiftP◍ x₁ s₁) = ⊥-elim (⟼s∨↦-excl ((-, -, -, s) , (-, s₁)))
  ⟼e-det (LiftS○ x s) (LiftS◍ x₁ s₁) with trans (sym x) x₁
  ... | ()
  ⟼e-det (LiftS○ x s) (LiftS○ x₁ s₁) with trans (sym x) x₁
  ... | refl = refl , refl , refl , refl
  ⟼e-det (LiftS○ x s) (LiftS◍ₛ x₁ x₂ s₁) with trans (sym x) x₁
  ... | ()
  ⟼e-det (LiftS◍ₛ x x₁ s) (LiftM↯ is↯ s₁) = ⊥-elim (⟼s∨⟼m-excl ((-, s₁) , (-, (-, -, s))))
  ⟼e-det (LiftS◍ₛ x x₁ s) (LiftM◍ ¬is↯ x₂ s₁) = ⊥-elim (⟼s∨⟼m-excl ((-, s₁) , (-, (-, -, s))))
  ⟼e-det (LiftS◍ₛ x x₁ s) (LiftM○↯ ¬is↯ x₂ s₁) = ⊥-elim (⟼s∨⟼m-excl ((-, s₁) , (-, (-, -, s))))
  ⟼e-det (LiftS◍ₛ x x₁ s) (LiftC◍ x₂ s₁) = ⊥-elim (⟼c∨⟼s-excl ((-, -, -, s) , (-,₄  s₁)))
  ⟼e-det (LiftS◍ₛ x x₁ s) (LiftC○↯ x₂ s₁) = ⊥-elim (⟼c∨⟼s-excl ((-, -, -, s) , (-,₄ s₁)))
  ⟼e-det (LiftS◍ₛ x x₁ s) (LiftP○ x₂ s₁) = ⊥-elim (⟼s∨↦-excl ((-, -, -, s) , (-, s₁)))
  ⟼e-det (LiftS◍ₛ x x₁ s) (LiftP◍ x₂ s₁) = ⊥-elim (⟼s∨↦-excl ((-, -, -, s) , (-, s₁)))
  ⟼e-det (LiftS◍ₛ x x₁ s) (LiftS◍ x₂ s₁)
    with ⟼s-det s s₁
  ... | () 
  ⟼e-det (LiftS◍ₛ x x₁ s) (LiftS○ x₂ s₁) with trans (sym x) x₂
  ... | ()
  ⟼e-det (LiftS◍ₛ x x₁ s) (LiftS◍ₛ x₂ x₃ s₁) with trans (sym x₁) x₃
  ... | refl = refl , refl , refl , refl
  ⟼e-det (LiftSₛ s) (LiftM↯ is↯ s₁) = ⊥-elim (⟼s∨⟼m-excl ((-, s₁) , (-, (-, -, s))))
  ⟼e-det (LiftSₛ s) (LiftM◍ ¬is↯ x s₁) = ⊥-elim (⟼s∨⟼m-excl ((-, s₁) , (-, (-, -, s))))
  ⟼e-det (LiftSₛ s) (LiftC◍ x s₁) = ⊥-elim (⟼c∨⟼s-excl ((-, -, -, s) , (-,₄ s₁)))
  ⟼e-det (LiftSₛ s) (LiftP◍ x s₁) = ⊥-elim (⟼s∨↦-excl ((_ , _ ,  -, s) , (-, s₁)))
  ⟼e-det (LiftSₛ s) (LiftS◍ x s₁)
    with ⟼s-det s s₁
  ... | () 
  ⟼e-det (LiftSₛ s) (LiftSₛ s₁) = refl , refl , refl , refl
  ⟼e-det (LiftM○↯ ¬is↯ x s) (LiftSₛ s₁) = ⊥-elim (⟼s∨⟼m-excl ((-, s) , (-, (-, -, s₁))))
  ⟼e-det (LiftC○↯ x s) (LiftSₛ s₁) = ⊥-elim (⟼c∨⟼s-excl ((-,₃ s₁) , (-,₄ s)))
  ⟼e-det (LiftP○ x s) (LiftSₛ s₁) = ⊥-elim (⟼s∨↦-excl ((_ , _ ,  -, s₁) , (-, s)))
  ⟼e-det (LiftS○ () s) (LiftSₛ s₁)
  ⟼e-det (LiftS◍ₛ x () s) (LiftSₛ s₁)
  ⟼e-det (LiftSₛ s) (LiftM○↯ ¬is↯ x s₁) = ⊥-elim (⟼s∨⟼m-excl ((-, s₁) , (-, (-, -, s))))
  ⟼e-det (LiftSₛ s) (LiftC○↯ x s₁) = ⊥-elim (⟼c∨⟼s-excl ((-, -, -, s) , (-,₄ s₁)))
  ⟼e-det (LiftSₛ s) (LiftP○ x s₁) = ⊥-elim (⟼s∨↦-excl ((_ , _ ,  -, s) , (-, s₁)))
  ⟼e-det (LiftSₛ s) (LiftS○ () s₁)
  ⟼e-det (LiftSₛ s) (LiftS◍ₛ x () s₁)
