module Sequential.Progress where

  open import Calculus.Base
  open import Sequential.Calculus
  open import Sequential.Semantics
  open import Sequential.Valid

  open import Data.List 
  open import Data.List.Extra
  open import Data.Unit using (⊤)
  open import Data.Product renaming (proj₁ to fst)
  open import Data.Product.Extra
  open import Data.Nat
  open import Relation.Binary.PropositionalEquality renaming ([_] to [_]≡)
  open import Data.Maybe using (Maybe; just; nothing)
  open import Function 

  open import Data.Sum
  open import Data.Product
  open import Relation.Nullary
  open import Data.Bool hiding (if_then_else_)
  open import Relation.Binary.PropositionalEquality

  ↦-progress : ∀ {τ} → (t : CTerm τ) → ↦-Status t
  ↦-progress (λ′ t) = inj₁ (λ′ t)
  ↦-progress (t ∙ u) with ↦-progress t
  ↦-progress (.(λ′ t) ∙ u) | inj₁ (λ′ t) = inj₂ ([ t / u ] , Beta)
  ↦-progress (t ∙ u) | inj₂ (_ , s) = inj₂ (_ , (App s))
  ↦-progress （） = inj₁ （）
  ↦-progress True = inj₁ True
  ↦-progress False = inj₁ False
  ↦-progress (if t then t₁ else t₂)
    with ↦-progress t
  ↦-progress (if .True then t₁ else t₂) | inj₁ True   = inj₂ (t₁ , If₂)
  ↦-progress (if .False then t₁ else t₂) | inj₁ False = inj₂ (t₂ , If₃)
  ↦-progress (if t then t₁ else t₂) | inj₂ (t' , s) = inj₂ ((if t' then t₁ else t₂) , If₁ s)
  ↦-progress (return l t) = inj₁ (return l t)
  ↦-progress (t >>= t₁) = inj₁ (t >>= t₁)
  ↦-progress (Labeled l t) = inj₁ (Labeled l t)
  ↦-progress (label l⊑h t) = inj₁ (label l⊑h t)
  ↦-progress (unlabel l⊑h t) = inj₁ (unlabel l⊑h t)
  ↦-progress (fork l⊑h t) = inj₁ (fork l⊑h t)
  ↦-progress (χ l x) = inj₁ (χ l x)
  ↦-progress (throwTo e l⊑h t) = inj₁ (throwTo e l⊑h t)
  ↦-progress (catch hs t) = inj₁ (catch hs t)
  ↦-progress (mask e t) = inj₁ (mask e t)
  ↦-progress (unmask e t) = inj₁ (unmask e t)
  ↦-progress (#Thread l n) = inj₁ (#Thread n)
  ↦-progress (newMVar l⊑h) = inj₁ (newMVar l⊑h)
  ↦-progress (takeMVar t) = inj₁ (takeMVar t)
  ↦-progress (putMVar t t₁) = inj₁ (putMVar t t₁)
  ↦-progress (#MVar l n) = inj₁ (#MVar l n)

  private
    lemma₁ : ∀ {A : Set} (e e' : ξ) (hs hs' : List (ξ × A)) h
           → first (does ∘ ξ-≡-dec e ∘ fst) hs ≡ just ((e' , h) , hs')
           → e ≡ e'
    lemma₁ e e' ((e'' , _) ∷ hs) hs' h x with ξ-≡-dec e e''
    lemma₁ e .e'' ((e'' , _) ∷ hs) .hs _ refl | .true because ofʸ p = p
    lemma₁ e e' ((e'' , _) ∷ hs) hs' h x | .false because ofⁿ ¬p
      with first (does ∘ ξ-≡-dec e ∘ fst) hs | inspect (first (does ∘ ξ-≡-dec e ∘ fst)) hs
    lemma₁ e e' ((e'' , _) ∷ hs) .((e'' , _) ∷ snd) h refl | .false because ofⁿ ¬p | just (.(e' , h) , snd) | [ eq ]≡
      = lemma₁ e e' hs snd h eq

  ⟼-progress : ∀ {τ} {l : Label} (ϕ : Idˢ) (Σ : Store) (t : CTerm (MAC l τ)) (v : well-scoped Σ t) (M : ξ-Mask) (exs : ξ-List l)  → ⟼-Status ϕ Σ t M exs
  ⟼-progress ϕ Σ₁ (t ∙ u) v M exs with ξ-L-get-unmasked exs M | inspect (ξ-L-get-unmasked exs) M
  ⟼-progress ϕ Σ₁ (t ∙ u) v M exs | nothing | [ eq ]≡
    with ↦-progress t
  ⟼-progress ϕ Σ₁ (.(λ′ t) ∙ u) v M exs | nothing | [ eq ]≡ | inj₁ (λ′ t) = inj₂ (inj₁ (-,₄ ((LiftP◍ eq Beta) , λ ())))
  ⟼-progress ϕ Σ₁ (t ∙ u) v M exs | nothing | [ eq ]≡ | inj₂ (t' , s)     = inj₂ (inj₁ (-,₄ ((LiftP◍ eq (App s)) , λ ())))
  ⟼-progress ϕ Σ₁ (t ∙ u) v M exs | just _  | [ eq ]≡
    with ↦-progress t
  ⟼-progress ϕ Σ₁ (.(λ′ t) ∙ u) v M exs | just _ | [ eq ]≡ | inj₁ (λ′ t) = inj₂ (inj₁ (-,₄ ((LiftP○ eq Beta) , λ ())))
  ⟼-progress ϕ Σ₁ (t ∙ u) v M exs | just _ | [ eq ]≡ | inj₂ (t' , s) = inj₂ (inj₁ (-,₄ ((LiftP○ eq (App s)) , λ ())))
  ⟼-progress ϕ Σ₁ (if t then t₁ else t₂) v M exs with ξ-L-get-unmasked exs M | inspect (ξ-L-get-unmasked exs) M
  ⟼-progress ϕ Σ₁ (if t then t₁ else t₂) v M exs | nothing | [ eq ]≡
    with ↦-progress t
  ⟼-progress ϕ Σ₁ (if .True then t₁ else t₂) v M exs | nothing | [ eq ]≡ | inj₁ True = inj₂ (inj₁ (exs , ∅ , Σ₁ , t₁ , LiftP◍ eq If₂ , (λ ())))
  ⟼-progress ϕ Σ₁ (if .False then t₁ else t₂) v M exs | nothing | [ eq ]≡ | inj₁ False = inj₂ (inj₁ (exs , ∅ , Σ₁ , t₂ , LiftP◍ eq If₃ , (λ ())))
  ⟼-progress ϕ Σ₁ (if t then t₁ else t₂) v M exs | nothing | [ eq ]≡ | inj₂ (t' , s) = inj₂ (inj₁ (exs , ∅ , Σ₁ , (if t' then t₁ else t₂) , LiftP◍ eq (If₁ s) , (λ ())))
  ⟼-progress ϕ Σ₁ (if t then t₁ else t₂) v M exs | just _ | [ eq ]≡
    with ↦-progress t
  ⟼-progress ϕ Σ₁ (if .True then t₁ else t₂) v M exs | just _ | [ eq ]≡ | inj₁ True = inj₂ (inj₁ (-,₄ (LiftP○ eq If₂ , λ ())))
  ⟼-progress ϕ Σ₁ (if .False then t₁ else t₂) v M exs | just _ | [ eq ]≡ | inj₁ False = inj₂ (inj₁ (-,₄ (LiftP○ eq If₃ , λ ())))
  ⟼-progress ϕ Σ₁ (if t then t₁ else t₂) v M exs | just _ | [ eq ]≡ | inj₂ (t' , s)  = inj₂ (inj₁ (-,₄ (LiftP○ eq (If₁ s) , λ ())))
  ⟼-progress ϕ Σ₁ (return l t) v M exs = inj₁ (return l t)
  ⟼-progress ϕ Σ₁ (t >>= t₁) (v₁ , v₂) M exs with ⟼-progress ϕ Σ₁ t v₁ M exs
  ⟼-progress ϕ Σ₁ (.(χ l e) >>= t₁) (v₁ , v₂) M exs | inj₁ (χ l e) = inj₂ (inj₁ (-,₄ (LiftM↯ (is-↯ l e) Bindχ , (λ ()))))
  ⟼-progress ϕ Σ₁ (.(return l t) >>= t₁) (v₁ , v₂) M exs | inj₁ (return l t) with ξ-L-get-unmasked exs M | inspect (ξ-L-get-unmasked exs) M
  ⟼-progress ϕ Σ₁ (.(return l t) >>= t₁) (v₁ , v₂) M exs | inj₁ (return l t) | nothing | [ eq ]≡ = inj₂ (inj₁ (-,₄ (LiftM◍ (λ ()) eq Bind , (λ ()))))
  ⟼-progress ϕ Σ₁ (.(return l t) >>= t₁) (v₁ , v₂) M exs | inj₁ (return l t) | just x | [ eq ]≡ = inj₂ (inj₁ (-,₄ ((LiftM○↯ (λ ()) eq Bind) , λ ())))
  ⟼-progress ϕ Σ₁ (t >>= t₁) (v₁ , v₂) M exs | inj₂ (inj₁ (_ , _ , _ , _ , (s , p))) = inj₂ (inj₁ (-,₄ (Bind s , p)))
  ⟼-progress ϕ Σ₁ (t >>= t₁) (v₁ , v₂) M exs | inj₂ (inj₂ y) = inj₂ (inj₂ (Bind y))
  ⟼-progress ϕ Σ₁ (label l⊑h t) v M exs  with ξ-L-get-unmasked exs M | inspect (ξ-L-get-unmasked exs) M
  ⟼-progress ϕ Σ₁ (label l⊑h t) v M exs | nothing | [ eq ]≡ = inj₂ (inj₁ (-,₄ ((LiftM◍ (λ ()) eq (Label' l⊑h)) , λ ())))
  ⟼-progress ϕ Σ₁ (label l⊑h t) v M exs | just x | [ eq ]≡ = inj₂ (inj₁ (-,₄ ((LiftM○↯ (λ ()) eq (Label' l⊑h)) , λ ())))
  ⟼-progress ϕ Σ₁ (unlabel l⊑h t) v M exs with ξ-L-get-unmasked exs M | inspect (ξ-L-get-unmasked exs) M
  ⟼-progress ϕ Σ₁ (unlabel l⊑h t) v M exs | nothing | [ eq ]≡
    with ↦-progress t
  ⟼-progress ϕ Σ₁ (unlabel l⊑h .(Labeled l t)) v M exs | nothing | [ eq ]≡ | inj₁ (Labeled l t) = inj₂ (inj₁ (-,₄ (LiftM◍ (λ ()) eq (Unlabel₂ l⊑h) , (λ ()))))
  ⟼-progress ϕ Σ₁ (unlabel l⊑h t) v M exs | nothing | [ eq ]≡ | inj₂ (_ , s) = inj₂ (inj₁ (-,₄ (LiftM◍ (λ ()) eq (Unlabel₁ l⊑h s) , (λ ()))))
  ⟼-progress ϕ Σ₁ (unlabel l⊑h t) v M exs | just x | [ eq ]≡
    with ↦-progress t
  ⟼-progress ϕ Σ₁ (unlabel l⊑h .(Labeled l t)) v M exs | just x | [ eq ]≡ | inj₁ (Labeled l t) = inj₂ (inj₁ (-,₄ ((LiftM○↯ (λ ()) eq (Unlabel₂ l⊑h)) , λ ()))) --- 
  ⟼-progress ϕ Σ₁ (unlabel l⊑h t) v M exs | just x | [ eq ]≡ | inj₂ (_ , s) = inj₂ (inj₁ (-,₄ ((LiftM○↯ (λ ()) eq (Unlabel₁ l⊑h s) , λ ())))) 
  ⟼-progress ϕ Σ₁ (fork l⊑h t) v M exs with ξ-L-get-unmasked exs M | inspect (ξ-L-get-unmasked exs) M
  ⟼-progress ϕ Σ₁ (fork l⊑h t) v M exs | nothing | [ eq ]≡ = inj₂ (inj₁ (-,₄ ((LiftC◍  eq (Fork l⊑h)) , λ ())))
  ⟼-progress ϕ Σ₁ (fork l⊑h t) v M exs | just x | [ eq ]≡ = inj₂ (inj₁ (-,₄ ((LiftC○↯  eq (Fork l⊑h)) , λ ())))
  ⟼-progress ϕ Σ₁ (χ l x) v M exs = inj₁ (χ l x)
  ⟼-progress ϕ Σ₁ (throwTo e l⊑h t) v M exs with ξ-L-get-unmasked exs M | inspect (ξ-L-get-unmasked exs) M

  ⟼-progress ϕ Σ₁ (throwTo e l⊑h t) v M exs | nothing | [ eq ]≡
    with ↦-progress t
  ⟼-progress ϕ Σ₁ (throwTo e l⊑h .(#Thread _ n)) v M exs | nothing | [ eq ]≡ | inj₁ (#Thread n) = inj₂ (inj₁ (-,₄ (LiftC◍ eq (ThrowTo₂ e l⊑h n) , (λ ()))))
  ⟼-progress ϕ Σ₁ (throwTo e l⊑h t) v M exs | nothing | [ eq ]≡ | inj₂ (_ , s) = inj₂ (inj₁ (-,₄ (LiftC◍ eq (ThrowTo₁ e l⊑h s) , (λ ()))))
  ⟼-progress ϕ Σ₁ (throwTo e l⊑h t) v M exs | just x | [ eq ]≡
    with ↦-progress t
  ⟼-progress ϕ Σ₁ (throwTo e l⊑h .(#Thread _ n)) v M exs | just x | [ eq ]≡ | inj₁ (#Thread n) = inj₂ (inj₁ (-,₄ (LiftC○↯ eq (ThrowTo₂ e l⊑h n) , (λ ()))))
  ⟼-progress ϕ Σ₁ (throwTo e l⊑h t) v M exs | just x | [ eq ]≡ | inj₂ (_ , s) = inj₂ (inj₁ (-,₄ (LiftC○↯ eq (ThrowTo₁ e l⊑h s) , (λ ()))))
  ⟼-progress ϕ Σ₁ (catch hs t) (vₕₛ , v) M exs
    with ⟼-progress ϕ Σ₁ t v M exs
  ⟼-progress ϕ Σ₁ (catch hs .(χ l e)) v M exs | inj₁ (χ l e)
    with first (does ∘ ξ-≡-dec e ∘ fst) hs | inspect (first (does ∘ ξ-≡-dec e ∘ fst)) hs
  ⟼-progress ϕ Σ₁ (catch hs .(χ l e)) v M exs | inj₁ (χ l e) | nothing | [ eq ]≡ = inj₂ (inj₁ (exs , ∅ , Σ₁ , χ l e , Catchχ₁ eq , (λ ())))
  ⟼-progress ϕ Σ₁ (catch hs .(χ l e)) v M exs | inj₁ (χ l e) | just ((_ , _) , _) | [ eq ]≡ with lemma₁ e _ hs _ _ eq
  ... | refl = inj₂ (inj₁ (-,₄ ((Catchχ₂ eq) , λ ())))
  ⟼-progress ϕ Σ₁ (catch hs .(return l t)) v M exs | inj₁ (return l t) with ξ-L-get-unmasked exs M | inspect (ξ-L-get-unmasked exs) M
  ⟼-progress ϕ Σ₁ (catch hs .(return l t)) v M exs | inj₁ (return l t) | nothing | [ eq ]≡ = inj₂ (inj₁ (exs , ∅ , Σ₁ , return l t , LiftM◍ (λ ()) eq Catch , (λ ())))
  ⟼-progress ϕ Σ₁ (catch hs .(return l t)) v M exs | inj₁ (return l t) | just x | [ eq ]≡ = inj₂ (inj₁ (-,₄ ((LiftM○↯ (λ ()) eq Catch) , λ ())))
  ⟼-progress ϕ Σ₁ (catch hs t) v M exs | inj₂ (inj₁ ( _ , _ , _ , _ , s , p )) = inj₂ (inj₁ (-,₄ ((Catch s) , p)))
  ⟼-progress ϕ Σ₁ (catch hs t) v M exs | inj₂ (inj₂ y) = inj₂ (inj₂ (Catch y))
  ⟼-progress ϕ Σ₁ (mask e t) v M exs
    with ⟼-progress ϕ Σ₁ t v (ξ-mask M e) exs
  ⟼-progress ϕ Σ₁ (mask e .(χ l e₁)) v M exs | inj₁ (χ l e₁) = inj₂ (inj₁ (-,₄ (LiftM↯ (is-↯ l e₁) Maskχ , (λ ()))))
  ⟼-progress ϕ Σ₁ (mask e .(return l t)) v M exs | inj₁ (return l t) with ξ-L-get-unmasked exs M | inspect (ξ-L-get-unmasked exs) M
  ⟼-progress ϕ Σ₁ (mask e .(return l t)) v M exs | inj₁ (return l t) | nothing | [ eq ]≡ = inj₂ (inj₁ (-,₄ ((LiftM◍ (λ ()) eq Mask) , λ ())))
  ⟼-progress ϕ Σ₁ (mask e .(return l t)) v M exs | inj₁ (return l t) | just x | [ eq ]≡ = inj₂ (inj₁ (-,₄ ((LiftM○↯ (λ ()) eq Mask) , λ ())))
  ⟼-progress ϕ Σ₁ (mask e t) v M exs | inj₂ (inj₁ (_ , _ , _ , _ , s , p)) = inj₂ (inj₁ (-,₄ (Mask s , p)))
  ⟼-progress ϕ Σ₁ (mask e t) v M exs | inj₂ (inj₂ s) = inj₂ (inj₂ (Mask s))
  ⟼-progress ϕ Σ₁ (unmask e t) v M exs
    with ⟼-progress ϕ Σ₁ t v (ξ-unmask M e) exs
  ⟼-progress ϕ Σ₁ (unmask e .(χ l e₁)) v M exs | inj₁ (χ l e₁) = inj₂ (inj₁ (-,₄ (LiftM↯ (is-↯ l e₁) UnMaskχ , (λ ()))))
  ⟼-progress ϕ Σ₁ (unmask e .(return l t)) v M exs | inj₁ (return l t) with ξ-L-get-unmasked exs M | inspect (ξ-L-get-unmasked exs) M
  ⟼-progress ϕ Σ₁ (unmask e .(return l t)) v M exs | inj₁ (return l t) | nothing | [ eq ]≡ = inj₂ (inj₁ (-,₄ ((LiftM◍ (λ ()) eq UnMask) , λ ())))
  ⟼-progress ϕ Σ₁ (unmask e .(return l t)) v M exs | inj₁ (return l t) | just x | [ eq ]≡ = inj₂ (inj₁ (-,₄ ((LiftM○↯ (λ ()) eq UnMask) , λ ())))
  ⟼-progress ϕ Σ₁ (unmask e t) v M exs | inj₂ (inj₁ (_ , _ , _ , _ , s , p)) = inj₂ (inj₁ (-,₄ (UnMask s , p)))
  ⟼-progress ϕ Σ₁ (unmask e t) v M exs | inj₂ (inj₂ s) = inj₂ (inj₂ (UnMask s))
  ⟼-progress ϕ Σ₁ (newMVar l⊑h) v M exs with ξ-L-get-unmasked exs M | inspect (ξ-L-get-unmasked exs) M
  ⟼-progress ϕ Σ₁ (newMVar l⊑h) v M exs | nothing | [ eq ]≡ = inj₂ (inj₁ (-,₄ ((LiftS◍ eq (NewMVar l⊑h)) , (λ ()))))
  ⟼-progress ϕ Σ₁ (newMVar l⊑h) v M exs | just x | [ eq ]≡ = inj₂ (inj₁ (-,₄ ((LiftS○ eq (NewMVar l⊑h)) , (λ ()))))
  ⟼-progress ϕ Σ₁ (takeMVar t) v M exs with ξ-L-get-unmasked exs M | inspect (ξ-L-get-unmasked exs) M
  ⟼-progress ϕ Σ₁ (takeMVar t) v M exs | nothing | [ eq ]≡
    with ↦-progress t
  ⟼-progress ϕ Σ₁ (takeMVar .(#MVar l n)) (⊗ , p) M exs | nothing | [ eq ]≡ | inj₁ (#MVar l n)
    with ξ-L-get exs | inspect ξ-L-get exs
  ⟼-progress ϕ Σ₁ (takeMVar .(#MVar l n)) (⊗ , p) M exs | nothing | [ eq ]≡ | inj₁ (#MVar l n) | nothing | [ eq' ]≡
    with ξ-L-get⇏nothing eq'
  ... | refl  = inj₂ (inj₂ (LiftSₛ (TakeMVarₛ p)))
  ⟼-progress ϕ Σ₁ (takeMVar .(#MVar l n)) (⊗ , p) M exs | nothing | [ eq ]≡ | inj₁ (#MVar l n) | just x | [ eq' ]≡
    = inj₂ (inj₁ (-,₄ ((LiftS◍ₛ eq eq' (TakeMVarₛ p)) , (λ ()))))
  ⟼-progress ϕ Σ₁ (takeMVar {τ = τ} .(#MVar l n)) (⟦ x ⟧ , p) M exs | nothing | [ eq ]≡ | inj₁ (#MVar l n)
    with Lookupℕ⇒Updateℕ p (τ , ⊗)
  ... | _ , p' = inj₂ (inj₁ (-,₄ ((LiftS◍ eq (TakeMVar₂ p p')) , (λ ()))))
  ⟼-progress ϕ Σ₁ (takeMVar t) v M exs | nothing | [ eq ]≡ | inj₂ (_ , s) = inj₂ (inj₁ (-,₄ (LiftS◍ eq (TakeMVar₁ s) , (λ ()))))
  ⟼-progress ϕ Σ₁ (takeMVar t) v M exs | just x | [ eq ]≡
    with ↦-progress t
  ⟼-progress ϕ Σ₁ (takeMVar .(#MVar l n)) (⊗ , p) M exs | just x | [ eq ]≡ | inj₁ (#MVar l n) = inj₂ (inj₁ (-,₄ ((LiftS○ eq (TakeMVarₛ p)) , λ ())))
  ⟼-progress ϕ Σ₁ (takeMVar {τ = τ} .(#MVar l n)) (⟦ x₁ ⟧ , p) M exs | just x | [ eq ]≡ | inj₁ (#MVar l n)
    with Lookupℕ⇒Updateℕ p (τ , ⊗)
  ... | (_ , p')  = inj₂ (inj₁ (-,₄ ((LiftS○ eq (TakeMVar₂ p p')) , λ ())))
  ⟼-progress ϕ Σ₁ (takeMVar t) v M exs | just x | [ eq ]≡ | inj₂ (_ , s) = inj₂ (inj₁ (-,₄ ((LiftS○ eq (TakeMVar₁ s)) , (λ ()))))
  ⟼-progress ϕ Σ₁ (putMVar t u) v M exs with ξ-L-get-unmasked exs M | inspect (ξ-L-get-unmasked exs) M
  ⟼-progress ϕ Σ₁ (putMVar t u) v M exs | nothing | [ eq ]≡
    with ↦-progress t
  ⟼-progress ϕ Σ₁ (putMVar .(#MVar l n) u) ((⊗ , p) , _) M exs | nothing | [ eq ]≡ | inj₁ (#MVar l n)
    with Lookupℕ⇒Updateℕ p (_ , ⟦ u ⟧)
  ... | (_ , p') 
    = inj₂ (inj₁ (-,₄ ((LiftS◍ eq (PutMVar₂ p p' )) , λ ())))
  ⟼-progress ϕ Σ₁ (putMVar .(#MVar l n) u) ((⟦ x ⟧ , p) , _) M exs | nothing | [ eq ]≡ | inj₁ (#MVar l n)
    with ξ-L-get exs | inspect ξ-L-get exs
  ⟼-progress ϕ Σ₁ (putMVar .(#MVar l n) u) ((⟦ x ⟧ , p) , _) M exs | nothing | [ eq ]≡ | inj₁ (#MVar l n) | nothing | [ eq' ]≡
    with ξ-L-get⇏nothing eq'
  ... | refl  = inj₂ (inj₂ (LiftSₛ (PutMVarₛ p)))
  ⟼-progress ϕ Σ₁ (putMVar .(#MVar l n) u) ((⟦ x ⟧ , p) , _) M exs | nothing | [ eq ]≡ | inj₁ (#MVar l n) | just e | [ eq' ]≡
    = inj₂ (inj₁ (-,₄ ((LiftS◍ₛ eq eq' (PutMVarₛ p)) , λ ())))
  ⟼-progress ϕ Σ₁ (putMVar t u) v M exs | nothing | [ eq ]≡ | inj₂ (_ , s) = inj₂ (inj₁ (-,₄ (LiftS◍ eq (PutMVar₁ s) , (λ ()))))
  ⟼-progress ϕ Σ₁ (putMVar t u) v M exs | just x | [ eq ]≡
    with ↦-progress t
  ⟼-progress ϕ Σ₁ (putMVar .(#MVar l n) u) ((⊗ , p) , _) M exs | just x | [ eq ]≡ | inj₁ (#MVar l n)
    with Lookupℕ⇒Updateℕ p (_ , ⟦ u ⟧)
  ... | (_ , p') = inj₂ (inj₁ (-,₄ ((LiftS○ eq (PutMVar₂ p p' )) , λ ())))
  ⟼-progress ϕ Σ₁ (putMVar .(#MVar l n) u) ((⟦ _ ⟧ , p) , _) M exs | just x | [ eq ]≡ | inj₁ (#MVar l n) = inj₂ (inj₁ (-,₄ ((LiftS○ eq (PutMVarₛ p)) , λ ()) ))
  ⟼-progress ϕ Σ₁ (putMVar t u) v M exs | just x | [ eq ]≡ | inj₂ (_ , s) = inj₂ (inj₁ (-,₄ ((LiftS○ eq (PutMVar₁ s)) , (λ ()))))
