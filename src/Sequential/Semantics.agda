module Sequential.Semantics where

  open import Calculus.Base
  open import Sequential.Calculus
  
  open import Function     using (flip)
  open import Data.List    using (List; []; _∷_; length; lookup; _[_]∷=_; uncons; map)
  open import Data.List.Extra using (first)
  open import Data.List.Relation.Unary.Any  using (index)
  open import Data.List.Membership.Propositional
  open import Data.Nat     using (ℕ; _<_)
  open import Data.Fin     using (fromℕ<; Fin)
  open import Data.Bool    using (Bool; true; false; not)
  open import Data.Product using (_×_; _,_; -,_) renaming (proj₁ to fst; proj₂ to snd; Σ to Σ')
  open import Data.Product.Extra
  open import Data.Maybe   using (Maybe; just; nothing)
  open import Relation.Binary.PropositionalEquality
  open import Function
  open import Data.Unit using (tt)

  open import Relation.Nullary

  data _↦_ : ∀ {τ} -> CTerm τ -> CTerm τ -> Set where

    App : ∀ {τ₁ τ₂} {t₁ t₁' : CTerm (τ₁ ⇒ τ₂)} {t₂ : CTerm τ₁} -> t₁ ↦ t₁' -> t₁ ∙ t₂ ↦ (t₁' ∙ t₂)

    Beta : ∀ {τ₁ τ₂} {t₁ : Term (τ₁ ∷ []) τ₂} {t₂ : CTerm τ₁} → (λ′ t₁) ∙ t₂ ↦ [ t₁ / t₂ ]

    If₁ : ∀ {τ} {t₁ t₁' : CTerm Bool'} {t₂ t₃ : CTerm τ} → t₁ ↦ t₁'
        → (if t₁ then t₂ else t₃) ↦ (if t₁' then t₂ else t₃)

    If₂ : ∀ {τ} {t₁ t₂ : CTerm τ} → (if True then t₁ else t₂) ↦ t₁

    If₃ : ∀ {τ} {t₁ t₂ : CTerm τ} → (if False then t₁ else t₂) ↦ t₂


  data ↦-Value {Γ : Ctx} : ∀ {τ} -> Term Γ τ -> Set where
    （）   : ↦-Value （）
    True  : ↦-Value True
    False : ↦-Value False
    λ′    : ∀ {τ₁ τ₂} (t : Term (τ₁ ∷ Γ) τ₂) → ↦-Value (λ′ t)

    -- Monadic
    return : ∀ {τ} → (l : Label) → (t : Term Γ τ) → ↦-Value (return l t)
    _>>=_  : ∀ {l} {τ₁ τ₂} → (t : Term Γ (MAC l τ₁)) → (u : Term Γ (τ₁ ⇒ MAC l τ₂)) → ↦-Value (t >>= u)

    -- Basic labeled values
    Labeled  : ∀ {τ} → (l : Label) → (t : Term Γ τ) → ↦-Value (Labeled l t)
    label    : ∀ {l h τ} → (l⊑h : l ⊑ h) → (t : Term Γ τ) → ↦-Value (label l⊑h t)
    unlabel  : ∀ {l h τ} → (l⊑h : l ⊑ h) → (t : Term Γ (Labeled l τ)) → ↦-Value (unlabel l⊑h t)

    -- Concurrency
    fork    : ∀ {l h} (l⊑h : l ⊑ h) → (t : Term Γ (MAC h （）)) → ↦-Value (fork l⊑h t)
    throwTo : ∀ {l h} → (e : ξ) → (l⊑h : l ⊑ h) → (t : Term Γ (ThreadId h)) → ↦-Value (throwTo e l⊑h t)
    χ       : ∀ {τ} (l : Label) (e : ξ) → ↦-Value (χ {τ = τ} l e)
    catch   : ∀ {l τ} → (hs : List (ξ × Term Γ (MAC l τ))) → (t : Term Γ (MAC l τ)) → ↦-Value (catch hs t)
    mask    : ∀ {l τ} → (e : ξ) → (t : Term Γ (MAC l τ)) → ↦-Value (mask e t)
    unmask  : ∀ {l τ} → (e : ξ) → (t : Term Γ (MAC l τ)) → ↦-Value (unmask e t)
    #Thread : ∀ {l} (n : ℕ) → ↦-Value (#Thread l n)

    -- Synchronization variables
    newMVar   : ∀ {l h τ} → (l⊑h : l ⊑ h)           → ↦-Value (newMVar {τ = τ} l⊑h)
    takeMVar  : ∀ {l τ}   → (t : Term Γ (MVar l τ)) → ↦-Value (takeMVar t)
    putMVar   : ∀ {l τ}   →  (u : Term Γ (MVar l τ)) → (t : Term Γ τ) → ↦-Value (putMVar u t)
    #MVar     : ∀ {τ} (l : Label) → (n : ℕ)         → ↦-Value (#MVar {τ = τ} l n)

  Dec-↦-Value : ∀ {τ} {Γ} → (t : Term Γ τ) → Dec (↦-Value t)
  Dec-↦-Value (var τ∈Γ) = no (λ ())
  Dec-↦-Value (λ′ t) = yes (λ′ t)
  Dec-↦-Value (t ∙ t₁) = no (λ ())
  Dec-↦-Value （） = yes （）
  Dec-↦-Value True = yes True
  Dec-↦-Value False = yes False
  Dec-↦-Value (if t then t₁ else t₂) = no (λ ())
  Dec-↦-Value (return l t) = yes (return l t)
  Dec-↦-Value (t >>= t₁) = yes (t >>= t₁)
  Dec-↦-Value (Labeled l t) = yes (Labeled l t)
  Dec-↦-Value (label l⊑h t) = yes (label l⊑h t)
  Dec-↦-Value (unlabel l⊑h t) = yes (unlabel l⊑h t)
  Dec-↦-Value (fork l⊑h t) = yes (fork l⊑h t)
  Dec-↦-Value (χ l x) = yes (χ l x)
  Dec-↦-Value (throwTo e l⊑h t) = yes (throwTo e l⊑h t)
  Dec-↦-Value (catch hs t) = yes (catch hs t)
  Dec-↦-Value (mask e t) = yes (mask e t)
  Dec-↦-Value (unmask e t) = yes (unmask e t)
  Dec-↦-Value (#Thread l n) = yes (#Thread n)
  Dec-↦-Value (newMVar l⊑h) = yes (newMVar l⊑h)
  Dec-↦-Value (takeMVar t) = yes (takeMVar t)
  Dec-↦-Value (putMVar t t₁) = yes (putMVar t t₁)
  Dec-↦-Value (#MVar l n) = yes (#MVar l n)

  open import Data.Sum
  open import Data.Product using (∃)

  ↦-Reducible : ∀ {τ} → (t : CTerm τ) → Set
  ↦-Reducible t = ∃ λ t' → t ↦ t'

  ↦-Status : ∀ {τ} → (t : CTerm τ) → Set
  ↦-Status t = ↦-Value t ⊎ ↦-Reducible t

  -- Monadic small step semantics for syncronization variables
  data _,_⟼s[_]_,_ (Σ : Store) : ∀ {τ} {l} → CTerm (MAC l τ) → (sev : SEvent l) → Store → CTerm (MAC l τ) → Set where
  
    NewMVar : ∀ {l h τ}
            → (l⊑h : l ⊑ h)
            → Σ , newMVar {τ = τ} l⊑h ⟼s[ step ] newˢ Σ h τ , return l (#MVar h (length (Σ h)))

    TakeMVar₁ : ∀ {l τ} {t₁ t₂ : CTerm (MVar l τ)}

              → (d : t₁ ↦ t₂)
              ------------------------------------------------
              → Σ , takeMVar t₁ ⟼s[ step ] Σ , takeMVar t₂

    TakeMVar₂ : ∀ {l τ} {n : ℕ} {t : CTerm τ} {m'}

              (p₁ : n ↦ (τ , ⟦ t ⟧) ∈ˢ (Σ l))
              (p₂ : m' ≔ (Σ l) [ n ↦ (τ , ⊗) ]ˢ)
              -----------------------------------------------------------------
              → Σ , takeMVar (#MVar l n) ⟼s[ step ] Σ [ l ↦ m'  ]ˢ , return l t

    TakeMVarₛ : ∀ {l τ} {n : ℕ}

              (p : n ↦ (τ , ⊗) ∈ˢ (Σ l))
              -----------------------------------------------------------------------
              → Σ , takeMVar {τ = τ} (#MVar l n) ⟼s[ stuck ] Σ , takeMVar (#MVar l n)

    PutMVar₁ : ∀ {l τ} {u : CTerm τ} {t₁ t₂ : CTerm (MVar l τ)}

              → (d : t₁ ↦ t₂)
              ------------------------------------------------
              → Σ , putMVar t₁ u ⟼s[ step ] Σ , putMVar t₂ u

    PutMVar₂ : ∀ {l τ} {n : ℕ} {t : CTerm τ} {m'}

             (p₁ : n ↦ (τ , ⊗) ∈ˢ (Σ l))
             (p₂ : m' ≔ (Σ l) [ n ↦ (τ , ⟦ t ⟧) ]ˢ)
             -------------------------------------------------------------------
             → Σ , putMVar (#MVar l n) t ⟼s[ step ] Σ [ l ↦ m' ]ˢ , return l （）

    PutMVarₛ : ∀ {l τ} {n : ℕ} {t : CTerm τ} {u : CTerm τ}

             (p : n ↦ (-, ⟦ t ⟧) ∈ˢ (Σ l))
             -----------------------------------------------------------------
             → Σ , putMVar (#MVar l n) u ⟼s[ stuck ] Σ , putMVar (#MVar l n) u

  -- Standard monadic exception-free concurrent-free semantics
  data _⟼m_ : ∀ {τ} {l} → CTerm (MAC l τ) → CTerm (MAC l τ) → Set where


      Mask  : ∀ {l} {e} {τ} {t : CTerm τ}
             ------------------------------------
             → mask e (return l t) ⟼m return l t

      Maskχ  : ∀ {l} {e₁} {e₂} {τ}
             -------------------------------------
             → mask e₁ (χ {τ = τ} l e₂) ⟼m χ l e₂

      Bind : ∀ {l} {τ₁ τ₂}  {t : CTerm τ₁} {u : CTerm (τ₁ ⇒ MAC l τ₂)}

           --------------------------------
           → (return l t >>= u) ⟼m (u ∙ t)

      Bindχ : ∀ {l} {τ₁ τ₂} {e} {u : CTerm (τ₁ ⇒ MAC l τ₂)}

             -------------------------
             → (χ l e >>= u) ⟼m χ l e

      Label' : ∀ {l h τ} {t : CTerm τ} → (l⊑h : l ⊑ h)

             ----------------------------------------
             → label l⊑h t ⟼m return l (Labeled h t)

      UnMask : ∀ {l} {τ} {e} {t : CTerm τ}

              ----------------------------------------
              → unmask e (return l t) ⟼m (return l t)

      UnMaskχ : ∀ {l} {τ} {e₁} {e₂}

              -----------------------------------------
              → unmask e₁ (χ {τ = τ} l e₂) ⟼m (χ l e₂)

      Unlabel₁ : ∀ {l h τ}  {t₁ t₂ : CTerm (Labeled l τ)} (l⊑h : l ⊑ h)

               → t₁ ↦ t₂
               -------------------------------------
               → unlabel l⊑h t₁ ⟼m unlabel l⊑h t₂

      Unlabel₂ : ∀ {l h τ}  {t : CTerm τ} → (l⊑h : l ⊑ h)

               ----------------------------------------------
               → unlabel l⊑h (Labeled l t) ⟼m return h t

      Catch : ∀ {l τ} {hs : List (ξ × CTerm (MAC l τ))} {t : CTerm τ}

            -------------------------------------- 
            → catch hs (return l t) ⟼m return l t

  -- Monadic concurrent semantics
  data [_]_[_]⟼c[_]_ (ϕ : Idˢ) : ∀ {τ l} → CTerm (MAC l τ) → (M : ξ-Mask) →(ev : CEvent l) → CTerm (MAC l τ) → Set where

      Fork : ∀ {l h} {t : CTerm (MAC h （）)} {M} (l⊑h : l ⊑ h) 

            ----------------------------------------------------------
            → [ ϕ ] fork l⊑h t [ M ]⟼c[ fork h l⊑h t M ] return l (#Thread h (ϕ h))

      ThrowTo₁ : ∀ {l h} {t₁ t₂ : CTerm (ThreadId h)} (e : ξ)  {M} (l⊑h : l ⊑ h)

               → t₁ ↦ t₂ 
               --------------------------------------------
               → [ ϕ ] throwTo e l⊑h t₁ [ M ]⟼c[ ∅ ] throwTo e l⊑h t₂

      ThrowTo₂ : ∀ {l h} (e : ξ) (l⊑h : l ⊑ h)  {M} (n : ℕ) 

               -----------------------------------------------------------------
               → [ ϕ ] throwTo e l⊑h (#Thread h n) [ M ]⟼c[ throw e h l⊑h n ] return l （）

  -- Monadic exceptional semantics
  data [_]_,_[_,_]⟼e[_,_]_,_ (ϕ : Idˢ) (Σ : Store) : ∀ {τ} {l} → CTerm (MAC l τ) → ξ-Mask → ξ-List l → ξ-List l → CEvent l → Store → CTerm (MAC l τ) → Set where

      Mask  : ∀ {Σ'} {l τ} {e : ξ} {t₁ : CTerm (MAC l τ)} {M} {exs} {de} {ev}
                {t₂ : CTerm (MAC l τ)}

             → (d : [ ϕ ] Σ , t₁ [ ξ-mask M e , exs ]⟼e[ de , ev ] Σ' , t₂)
             -----------------------------------------
             → [ ϕ ] Σ , mask e t₁ [ M , exs ]⟼e[ de , ev ] Σ' , mask e t₂

      UnMask  : ∀ {Σ'} {l τ} {e : ξ} {t₁ : CTerm (MAC l τ)}
                  {M : ξ-Mask} {exs} {de} {ev} {t₂ : CTerm (MAC l τ)}

              → (d : [ ϕ ] Σ , t₁ [ ξ-unmask M e , exs ]⟼e[ de , ev ] Σ' , t₂)
              ---------------------------------------------
              → [ ϕ ] Σ , unmask e t₁ [ M , exs ]⟼e[ de , ev ] Σ' , unmask e t₂

      Bind : ∀ {Σ'} {l} {τ₁ τ₂} {t₁ : CTerm (MAC l τ₁)} {u : CTerm (τ₁ ⇒ MAC l τ₂)}
               {M : ξ-Mask} {exs} {de} {ev} {t₂ : CTerm (MAC l τ₁)}

           → (d : [ ϕ ] Σ , t₁ [ M , exs ]⟼e[ de , ev ] Σ' , t₂)
           -------------------------------------------------------------------------
           → [ ϕ ] Σ , (t₁ >>= u) [ M , exs ]⟼e[ de , ev ] Σ' , t₂ >>= u

      Catch : ∀ {l τ} {Σ₁} {hs : List (ξ × CTerm (MAC l τ))} {t₁ : CTerm (MAC l τ)} 
                {M : ξ-Mask} {exs} {de} {ev} {t₂ : CTerm (MAC l τ)}
             
            → (d : [ ϕ ] Σ , t₁  [ M , exs ]⟼e[ de , ev ] Σ₁ , t₂)
            ---------------------------------------------------------------------------
            → [ ϕ ] Σ , catch hs t₁ [ M , exs ]⟼e[ de , ev ] Σ₁ , catch hs t₂

      Catchχ₁ : ∀ {l τ} {e : ξ} {hs :  List (ξ × CTerm (MAC l τ))}
                  {M : ξ-Mask} {exs : ξ-List l}

              → (e∉hs : first (does ∘ ξ-≡-dec e ∘ fst) hs ≡ nothing)                    -- there is no exception handler for e
              ----------------------------------------------------------
              → [ ϕ ] Σ , catch hs (χ l e) [ M , exs ]⟼e[ exs , ∅ ] Σ , χ l e

      Catchχ₂ : ∀ {l τ} {e : ξ} {hs :  List (ξ × CTerm (MAC l τ))} 
                  {M : ξ-Mask} {exs : ξ-List l}
                  {h : CTerm (MAC l τ)} {hs' :  List (ξ × CTerm (MAC l τ))}

              → (e,u∈hs : first (does ∘ ξ-≡-dec e ∘ fst) hs ≡ just ((e , h) , hs'))     -- there is a exception handler for e
              ------------------------------------------------------
              → [ ϕ ] Σ , catch hs (χ l e) [ M , exs ]⟼e[ exs , ∅ ] Σ , h

      LiftM↯  : ∀ {l τ} {t₁ : CTerm (MAC l τ)} {M : ξ-Mask}
                  {exs : ξ-List l} {t₂ : CTerm (MAC l τ)}

              → (is↯ : Is-↯ t₂)     → (s : t₁ ⟼m t₂)
              ---------------------------------------
              → [ ϕ ] Σ , t₁ [ M , exs ]⟼e[ exs , ∅ ] Σ , t₂

      LiftM◍ : ∀ {l τ} {t₁ : CTerm (MAC l τ)}
                 {M : ξ-Mask} {exs : ξ-List l}
                 {t₂ : CTerm (MAC l τ)}

              → (¬is↯ : ¬ (Is-↯ t₂)) → ξ-L-get-unmasked exs M ≡ nothing → (s : t₁ ⟼m t₂)
              ------------------------------------------------------------
              → [ ϕ ] Σ , t₁ [ M , exs ]⟼e[ exs , ∅ ] Σ , t₂

      LiftM○↯ : ∀ {l τ} {t₁ : CTerm (MAC l τ)}
                  {M : ξ-Mask} {exs : ξ-List l}
                  {t₂ : CTerm (MAC l τ)}
                  {ex  : ξ} {l'×l'⊑l : Σ' Label (_⊑ l)}
                  {exs' : ξ-List l}

              → (¬is↯ : ¬ (Is-↯ t₂))
              → ξ-L-get-unmasked exs M ≡ just ((ex , l'×l'⊑l) , exs')
              → (s : t₁ ⟼m t₂)
              ----------------------------------------------------------------------------------
              → [ ϕ ] Σ , t₁ [ M , exs ]⟼e[ exs' , ∅ ] Σ , χ l ex

      LiftC◍ : ∀ {l τ} {t₁ : CTerm (MAC l τ)}
                 {M : ξ-Mask} {exs : ξ-List l}
                 {ev : CEvent l} {t₂ : CTerm (MAC l τ)}

              → ξ-L-get-unmasked exs M ≡ nothing  → (s : [ ϕ ] t₁ [ M ]⟼c[ ev ] t₂)
              ------------------------------------------------------------
              → [ ϕ ] Σ , t₁ [ M , exs ]⟼e[ exs , ev ] Σ , t₂

      LiftC○↯ : ∀ {l τ} {t₁ : CTerm (MAC l τ)}
                  {M : ξ-Mask} {exs : ξ-List l}
                  {ev : CEvent l} {t₂ : CTerm (MAC l τ)}
                  {ex  : ξ} {l'×l'⊑l : Σ' Label (_⊑ l)}
                  {exs' : ξ-List l}

              → ξ-L-get-unmasked exs M ≡ just ((ex , l'×l'⊑l) , exs')  → (s : [ ϕ ] t₁ [ M ]⟼c[ ev ] t₂)
              ------------------------------------------------------------
              → [ ϕ ] Σ , t₁ [ M , exs ]⟼e[ exs' , ∅ ] Σ , χ l ex

      LiftP○ : ∀ {l τ} {t₁ : CTerm (MAC l τ)}
                 {M : ξ-Mask} {exs : ξ-List l}
                 {t₂ : CTerm (MAC l τ)}
                 {ex  : ξ} {l'×l'⊑l : Σ' Label (_⊑ l)}
                 {exs' : ξ-List l}

              → ξ-L-get-unmasked exs M ≡ just ((ex , l'×l'⊑l) , exs')  → (s : t₁ ↦ t₂)
              ---------------------------------------------------
              → [ ϕ ] Σ , t₁ [ M , exs ]⟼e[ exs' , ∅ ] Σ , χ l ex

      LiftP◍ : ∀ {l τ} {t₁ : CTerm (MAC l τ)}
                 {M : ξ-Mask} {exs : ξ-List l}
                 {t₂ : CTerm (MAC l τ)}

              → ξ-L-get-unmasked exs M ≡ nothing  → (s : t₁ ↦ t₂)
              ---------------------------------------------------
              → [ ϕ ] Σ , t₁ [ M , exs ]⟼e[ exs , ∅ ] Σ , t₂

      LiftS◍ : ∀  {l τ} {t₁ : CTerm (MAC l τ)}
                  {M : ξ-Mask} {exs  : ξ-List l}
                  {Σ' : Store} {t₂ : CTerm (MAC l τ)}

              → ξ-L-get-unmasked exs M ≡ nothing -- there are no unmasked exceptions

              → (s : Σ , t₁ ⟼s[ step ] Σ' , t₂)
              ------------------------------------------------------------
              → [ ϕ ] Σ , t₁ [ M , exs ]⟼e[ exs , ∅ ] Σ' , t₂

      LiftS○ : ∀  {l τ} {Σ₂} {t₁ : CTerm (MAC l τ)}
                  {M : ξ-Mask} {exs  : ξ-List l}
                  {sev : SEvent l} {t₂ : CTerm (MAC l τ)}
                  {ex  : ξ} {l'×l'⊑l : Σ' Label (_⊑ l)}
                  {exs' : ξ-List l}

              → ξ-L-get-unmasked exs M ≡ just ((ex , l'×l'⊑l) , exs') -- there is an unmasked exception

              → (s : Σ , t₁ ⟼s[ sev ] Σ₂ , t₂)
              ------------------------------------------------------------
              → [ ϕ ] Σ , t₁ [ M , exs ]⟼e[ exs' , ∅ ] Σ , χ l ex

      LiftS◍ₛ : ∀  {l τ} {t : CTerm (MAC l τ)}
                   {M : ξ-Mask} {exs  : ξ-List l}
                   {ex  : ξ} {l'×l'⊑l : Σ' Label (_⊑ l)}
                   {exs' : ξ-List l} 

              → ξ-L-get-unmasked exs M ≡ nothing                       -- there are no unmasked exceptions
              → ξ-L-get exs            ≡ just ((ex , l'×l'⊑l) , exs')  -- but there is a masked exception
              → (s : Σ , t ⟼s[ stuck ] Σ , t )
              ------------------------------------------------------------
              → [ ϕ ] Σ , t [ M , exs ]⟼e[ exs' , ∅ ] Σ , χ l ex

      LiftSₛ : ∀  {l τ} {t : CTerm (MAC l τ)}
                  {M : ξ-Mask}

              → (s : Σ , t ⟼s[ stuck ] Σ , t )
              -----------------------------------------------------------
              → [ ϕ ] Σ , t [ M , [] ]⟼e[ [] , stuck ] Σ , t

  data ⟼-Value {Γ : Ctx} : ∀ {l τ} -> Term Γ (MAC l τ) -> Set where
    χ       : ∀ {τ} → (l : Label) → (e : ξ) → ⟼-Value (χ {τ = τ} l e)
    return  : ∀ {τ} → (l : Label) → (t : Term Γ τ) -> ⟼-Value (return l t)

  ⟼-Reducible : ∀ {τ} {l : Label} (ϕ : Idˢ) (Σ : Store) (t : CTerm (MAC l τ)) (M : ξ-Mask) (exs : ξ-List l) → Set
  ⟼-Reducible ϕ Σ t M exs = ∃₄ λ exs' e Σ' t' → [ ϕ ] Σ , t [ M , exs ]⟼e[ exs' , e ] Σ' , t' × ¬ (e ≡ stuck)

  ⟼-Stuck : ∀ {τ} {l : Label} (ϕ : Idˢ) (Σ : Store) (t : CTerm (MAC l τ)) (M : ξ-Mask) (exs : ξ-List l) → Set
  ⟼-Stuck ϕ Σ t M exs = [ ϕ ] Σ , t [ M , exs ]⟼e[ exs , stuck ] Σ , t

  ⟼-Status : ∀ {τ} {l : Label} (ϕ : Idˢ) (Σ : Store) (t : CTerm (MAC l τ)) (M : ξ-Mask) (exs : ξ-List l) → Set
  ⟼-Status ϕ Σ t M exs = ⟼-Value t ⊎ ⟼-Reducible ϕ Σ t M exs ⊎ ⟼-Stuck ϕ Σ t M exs
                                     
