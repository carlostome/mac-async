module Sequential.Valid where

  open import Calculus.Base
  open import Sequential.Calculus

  open import Data.List 
  open import Data.List.Extra using (Lookupℕ)
  open import Data.Unit using (⊤; tt)
  open import Data.Product using (_×_; _,_; ∃)

  mutual
    well-scoped : ∀ {Γ τ} (Σ : Store) → Term Γ τ → Set
    well-scoped Σ (var τ∈Γ) = ⊤
    well-scoped Σ (λ′ t) = well-scoped Σ t
    well-scoped Σ (t ∙ t₁) = well-scoped Σ t × well-scoped Σ t₁
    well-scoped Σ （） = ⊤
    well-scoped Σ True = ⊤
    well-scoped Σ False = ⊤
    well-scoped Σ (if t then t₁ else t₂) = well-scoped Σ t × well-scoped Σ t₁ × well-scoped Σ t₂
    well-scoped Σ (return l t) = well-scoped Σ t
    well-scoped Σ (t >>= u) = well-scoped Σ t × well-scoped Σ u
    well-scoped Σ (Labeled l t) = well-scoped Σ t
    well-scoped Σ (label l⊑h t) = well-scoped Σ t
    well-scoped Σ (unlabel l⊑h t) = well-scoped Σ t
    well-scoped Σ (fork l⊑h t) = well-scoped Σ t
    well-scoped Σ (χ l x) = ⊤
    well-scoped Σ (throwTo e l⊑h t) = well-scoped Σ t
    well-scoped Σ (catch hs t) = well-scoped-list Σ hs × well-scoped Σ t
    well-scoped Σ (mask e t) = well-scoped Σ t
    well-scoped Σ (unmask e t) = well-scoped Σ t
    well-scoped Σ (#Thread l n) = ⊤
    well-scoped Σ (newMVar l⊑h) = ⊤
    well-scoped Σ (takeMVar t) = well-scoped Σ t
    well-scoped Σ (putMVar t t₁) = well-scoped Σ t × well-scoped Σ t₁
    well-scoped Σ (#MVar {τ = τ} l n) = ∃ λ c → Lookupℕ (τ , c) n (Σ l)

    well-scoped-list : ∀ {Γ τ} (Σ : Store) → List (ξ × Term Γ τ) → Set
    well-scoped-list Σ [] = ⊤
    well-scoped-list Σ ((_ , t) ∷ xs) = well-scoped Σ t × well-scoped-list Σ xs
