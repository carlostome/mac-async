module Types where

  open import Lattice
  open import Exception

  open import Relation.Nullary
  open import Relation.Binary.PropositionalEquality
  open import Data.Nat
  open import Data.Nat.Properties using (_≟_)

  data Ty : Set where
    （）  : Ty
    Bool' : Ty
    _⇒_   : (τ₁ t₂ : Ty) → Ty
    MAC   : (l : Label) → Ty → Ty      -- Mac computation
    Labeled : (l : Label) → Ty → Ty    -- Labeled value
    Χ       : ξ → Ty
    MVar : (l : Label) → Ty → Ty       -- Labeled synchronization variable
    ThreadId : (l : Label) → Ty        -- Labeled thread identifier

  infixr 3 _⇒_

  Ty-≡-dec : ∀ (x y : Ty) → Dec (x ≡ y)
  Ty-≡-dec （） （） = yes refl
  Ty-≡-dec （） Bool' = no (λ ())
  Ty-≡-dec （） (y ⇒ y₁) = no (λ ())
  Ty-≡-dec （） (MAC l y) = no (λ ())
  Ty-≡-dec （） (Labeled x y) = no (λ ())
  Ty-≡-dec （） (ThreadId l) = no (λ ())
  Ty-≡-dec （） (Χ n) = no (λ ())
  Ty-≡-dec Bool' （） = no (λ ())
  Ty-≡-dec Bool' Bool' = yes refl
  Ty-≡-dec Bool' (y ⇒ y₁) = no (λ ())
  Ty-≡-dec Bool' (MAC l y) = no (λ ())
  Ty-≡-dec Bool' (Labeled x y) = no (λ ())
  Ty-≡-dec Bool' (ThreadId l) = no (λ ())
  Ty-≡-dec Bool' (Χ n) = no (λ ())
  Ty-≡-dec (x ⇒ x₁) （） = no (λ ())
  Ty-≡-dec (x ⇒ x₁) Bool' = no (λ ())
  Ty-≡-dec (x ⇒ x₁) (Χ n) = no (λ ())
  Ty-≡-dec (x ⇒ x₁) (y ⇒ y₁)
    with Ty-≡-dec x y | Ty-≡-dec x₁ y₁
  ... | yes refl | yes refl = yes refl
  ... | yes refl | no ¬p    = no (λ {refl → ¬p refl})
  ... | no ¬p | yes p       = no (λ {refl → ¬p refl})
  ... | no ¬p | no ¬p₁      = no (λ {refl → ¬p refl})
  Ty-≡-dec (x ⇒ x₁) (MAC l y) = no (λ ())
  Ty-≡-dec (x ⇒ x₁) (Labeled x₂ y) = no (λ ())
  Ty-≡-dec (x ⇒ x₁) (ThreadId l) = no (λ ())
  Ty-≡-dec (MAC l x) （） = no (λ ())
  Ty-≡-dec (MAC l x) Bool' = no (λ ())
  Ty-≡-dec (MAC l x) (y ⇒ y₁) = no (λ ())
  Ty-≡-dec (MAC l x) (Χ n) = no (λ ())
  Ty-≡-dec (MAC l x) (MAC l₁ y)
    with Ty-≡-dec x y | Label-≡-dec l l₁
  ... | yes refl | yes refl = yes refl
  ... | yes refl | no ¬p    = no (λ {refl → ¬p refl})
  ... | no ¬p | yes refl    = no (λ {refl → ¬p refl})
  ... | no ¬p | no ¬p₁      = no (λ {refl → ¬p refl})
  Ty-≡-dec (MAC l x) (Labeled x₁ y) = no (λ ())
  Ty-≡-dec (MAC l x) (ThreadId l₁) = no (λ ())
  Ty-≡-dec (Labeled x x₁) （） = no (λ ())
  Ty-≡-dec (Labeled x x₁) Bool' = no (λ ())
  Ty-≡-dec (Labeled x x₁) (y ⇒ y₁) = no (λ ())
  Ty-≡-dec (Labeled x x₁) (MAC l y) = no (λ ())
  Ty-≡-dec (Labeled t x) (Χ n) = no (λ ())
  Ty-≡-dec (Labeled l x) (Labeled l₁ y)
    with Ty-≡-dec x y | Label-≡-dec l l₁
  ... | yes refl | yes refl = yes refl
  ... | yes refl | no ¬p    = no (λ {refl → ¬p refl})
  ... | no ¬p | yes refl    = no (λ {refl → ¬p refl})
  ... | no ¬p | no ¬p₁      = no (λ {refl → ¬p refl})
  Ty-≡-dec (Labeled x x₁) (ThreadId l) = no (λ ())
  Ty-≡-dec (ThreadId l) （） = no (λ ())
  Ty-≡-dec (ThreadId l) Bool' = no (λ ())
  Ty-≡-dec (ThreadId l) (y ⇒ y₁) = no (λ ())
  Ty-≡-dec (ThreadId l) (MAC l₁ y) = no (λ ())
  Ty-≡-dec (ThreadId l) (Labeled x y) = no (λ ())
  Ty-≡-dec (ThreadId l) (Χ n) = no (λ ())
  Ty-≡-dec (ThreadId l) (ThreadId l₁)
    with Label-≡-dec l l₁
  ... | yes refl = yes refl
  ... | no ¬p    = no (λ {refl → ¬p refl})
  Ty-≡-dec (Χ n) （） = no (λ ())
  Ty-≡-dec (Χ n) Bool' = no (λ ())
  Ty-≡-dec (Χ n) (y ⇒ y₁) = no (λ ())
  Ty-≡-dec (Χ n) (MAC l y) = no (λ ())
  Ty-≡-dec (Χ n) (Labeled l y) = no (λ ())
  Ty-≡-dec (Χ n) (Χ m) with ξ-≡-dec n m
  Ty-≡-dec (Χ n) (Χ .n) | yes refl = yes refl
  Ty-≡-dec (Χ n) (Χ m) | no ¬p = no (λ {refl → ¬p refl})
  Ty-≡-dec (Χ n) (ThreadId l) = no (λ ())
  Ty-≡-dec （） (MVar l b) = no (λ ())
  Ty-≡-dec Bool' (MVar l b) = no (λ ())
  Ty-≡-dec (a ⇒ a₁) (MVar l b) = no (λ ())
  Ty-≡-dec (MAC l a) (MVar l₁ b) = no (λ ())
  Ty-≡-dec (Labeled l a) (MVar l₁ b) = no (λ ())
  Ty-≡-dec (Χ x) (MVar l b) = no (λ ())
  Ty-≡-dec (MVar l a) （） = no (λ ())
  Ty-≡-dec (MVar l a) Bool' = no (λ ())
  Ty-≡-dec (MVar l a) (b ⇒ b₁) = no (λ ())
  Ty-≡-dec (MVar l a) (MAC l₁ b) = no (λ ())
  Ty-≡-dec (MVar l a) (Labeled l₁ b) = no (λ ())
  Ty-≡-dec (MVar l a) (Χ x) = no (λ ())
  Ty-≡-dec (MVar l a) (MVar l₁ b) with Ty-≡-dec a b | Label-≡-dec l l₁
  Ty-≡-dec (MVar l a) (MVar l₁ .a) | yes refl | yes refl  = yes refl
  Ty-≡-dec (MVar l a) (MVar l₁ .a) | yes refl | no ¬p     = no (λ {refl → ¬p refl})
  ... | no ¬p    | p = no (λ {refl → ¬p refl})
  Ty-≡-dec (MVar l a) (ThreadId l₁) = no (λ ())
  Ty-≡-dec (ThreadId l) (MVar l₁ b) = no (λ ())

  open import Data.List using (List; _∷_; [])

  Ctx : Set
  Ctx = List Ty

  data _∈_ (τ : Ty) : (Γ : Ctx) → Set where
    here  : ∀ {Γ}      → τ ∈ (τ ∷ Γ)
    there : ∀ {τ'} {Γ} → τ ∈ Γ → τ ∈ (τ' ∷ Γ)

  data _⊆_ : Ctx → Ctx → Set where
    base : [] ⊆ []
    drop : ∀ {τ} {Γ Δ} → Γ ⊆ Δ → Γ ⊆ (τ ∷ Δ)
    keep : ∀ {τ} {Γ Δ} → Γ ⊆ Δ → (τ ∷ Γ) ⊆ (τ ∷ Δ)

  wkenⱽ : ∀ {τ} {Γ₁ Γ₂ : Ctx} -> Γ₁ ⊆ Γ₂ -> τ ∈ Γ₁ -> τ ∈ Γ₂
  wkenⱽ (drop Γ₁⊆Γ₂) t         = there (wkenⱽ Γ₁⊆Γ₂ t)
  wkenⱽ (keep Γ₁⊆Γ₂) here      = here
  wkenⱽ (keep Γ₁⊆Γ₂) (there t) = there (wkenⱽ Γ₁⊆Γ₂ t)

  ⊆-refl : ∀ {Γ} → Γ ⊆ Γ
  ⊆-refl {[]}    = base
  ⊆-refl {x ∷ Γ} = keep ⊆-refl
